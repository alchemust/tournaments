$(document).ready(function() {
		var bg_h = $(document).height(); 
		$('#overlay').click(function (){
			close_post_score();
		});
		function loading_popup() {
			$('#overlay').attr('style', 'display: block;');
			$('#wrap-pop').attr('style', 'display: block; top: '+Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
	                 $(window).scrollTop()) + 'px');
			$('#login').attr('style', 'display: block;');
			$('#overlay').css('height', bg_h+'px');
		}
		
		function close_post_score(){
			$('#overlay').attr('style', 'display: none;');
			 $('#wrap-score').attr('style', 'display: none;');
			 $('#post-score').attr('style', 'display: none;');
			 $('#overlay').css('height', 'auto');
		}
		
		function close_popup(){
			$('#overlay').attr('style', 'display: none;');
			 $('#wrap-pop').attr('style', 'display: none;');
			 $('#login').attr('style', 'display: none;');
			 $('#overlay').css('height', 'auto');
		}
		
		
		function loading_post_score() {
			$('#overlay').attr('style', 'display: block;');
			$('#wrap-score').attr('style', 'display: block; top: '+Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
	                 $(window).scrollTop()) + 'px');
			$('#post-score').attr('style', 'display: block;');
			$('#overlay').css('height', bg_h+'px');
		}
		
		
		
		$("#btn_comments").click(function(){
			loading_popup();
			var match_id = $("#match_id").val();
			var tour_id = $("#tour_id").val();
			var comment = $("#txt_comment").val();
			var team_id = $("#team_id").val();
			if(comment =="") {alert("กรุณากรอกข้อความด้วยค่ะ"); close_popup(); return false;}
			$.ajax({
				url: '/match/post_comment',
				type: "POST",			
				data : {match_id : match_id,tour_id:tour_id, team_id : team_id, comment : comment },
				success: function(response){
					switch (response) {
						case "100" : 
							alert("คุณได้ทำการโพสแจ้งผลการแข่งขันเรียบร้อยแล้วค่ะ ขอบคุณค่ะ"); 
							location.reload(); break;
							
						default: 
							alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ");  
						    location.reload(); break;
							break;
					}
				},
				error: function() {
					alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
					$("#btn_edit_round").prop('disabled', false);
					location.reload();
				 }
			});
		});
		
		$("#btn_post_score").click(function(){
			loading_post_score();
		});
		
		$("#btn_up_pic").click(function(){
			var match_id = $("#match_id").val();
			var score_pic = $("#txt_score_pic").val();
			if(score_pic =="") {alert("กรุณาใส่ลิงค์รูปมาด้วยค่ะ"); return false;}
			$.ajax({
				url: '/match/update_score',
				type: "POST",			
				data : {match_id : match_id, score_pic : score_pic },
				success: function(response){
					switch (response) {
						case "100" : 
							alert("คุณได้ทำการแจ้งผลการแข่งขันเรียบร้อยแล้วค่ะ ขอบคุณค่ะ"); 
							location.reload(); break;
							
						default: 
							alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ");  
						    location.reload(); break;
							break;
					}
				},
				error: function() {
					alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
					$("#btn_edit_round").prop('disabled', false);
					location.reload();
				 }
			});
		});
		
		$("#login_form").validate({
		   messages: {
			login_user: "Username !",
			login_pass: "Password !"
		   },
		   onkeyup: false,
		   submitHandler: function() {
			   loading_popup();

			   var login_user = $("#login_user").val();
			   var login_pass = $("#login_pass").val();
			   $.ajax({
					url: 'login/validation',
					type: "POST",
					data: { login_user: login_user,login_pass:login_pass },
					success: function(data){
						if (data==1){
							window.location = "blackend/main";
						}
						else
						{
							 alert("ขออภัยค่ะ คุณกรอก User และ Password ไม่ถูกต้อง"); 
							 $('#overlay').attr('style', 'display: none;');
							 $('#wrap-pop').attr('style', 'display: none;');
							 $('#login').attr('style', 'display: none;');
							 $('#overlay').css('height', 'auto');
						}
					}
				});
		   }
		});
		
		$("#btn-logout").click(function(){
			$.ajax({
				url: '/register/user_logout',
				type: "POST",
				dataType : "json",
				success: function(response){
					switch (response.code) {
						case "1000": location.reload(); break;
						
						default: 
							alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
							location.reload();
							break;
					}
				},
				error: function() {
					alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
					location.reload();
				 }
			});
		});

		$("#btn-profile").click(function(){
			window.location="/profile_info";
		});

		$("#btn-edit-profile").click(function(){
			window.location="/edit_profile_info";
		});
		
		$("#login_sidebar_form").validate({
			   messages: {
				login_user: "Username !",
				login_pass: "Password !"
			   },
			   onkeyup: false,
			   submitHandler: function() {
					$('#overlay').attr('style', 'display: block;');
					$('#wrap-pop').attr('style', 'display: block; top: 500px;');
					$('#login').attr('style', 'display: block;');
					$('#overlay').css('height', bg_h+'px');

				   var login_user = $("#login_user").val();
				   var login_pass = $("#login_pass").val();
				   
				   $.ajax({
						url: '/register/user_validation',
						type: "POST",
						dataType : "json",			
						data: { login_user: login_user,login_pass:login_pass },
						success: function(response){
							switch (response.code) {
								case "1000": window.location = "/"; break;
								case "1001": window.location = "/"; break;
								case "1002": 
									alert("ขออภัยค่ะ คุณกรอก User และ Password ไม่ถูกต้อง"); 
									$('#overlay').attr('style', 'display: none;');
									$('#wrap-pop').attr('style', 'display: none;');
									$('#login').attr('style', 'display: none;');
									$('#overlay').css('height', 'auto');
									$('#login_user').focus();
									break;
								default: 
									alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
									location.reload();
									break;
							}
						},
						error: function() {
							alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
							location.reload();
						 }
					});
			   }
		});
		
		$("#register_form").validate({
			   messages: {
				login_user: "Username !",
				login_pass: "Password !"
			   },
			   onkeyup: false,
			   submitHandler: function() {
					$('#overlay').attr('style', 'display: block;');
					$('#wrap-pop').attr('style', 'display: block; top: 500px;');
					$('#login').attr('style', 'display: block;');
					$('#overlay').css('height', bg_h+'px');

				   var login_user = $("#login_user").val();
				   var login_pass = $("#login_pass").val();
				   
				   $.ajax({
						url: '/register/user_validation',
						type: "POST",
						dataType : "json",			
						data: { login_user: login_user,login_pass:login_pass },
						success: function(response){
							alert(response.code);
							switch (response.code) {
								case "1000": window.location = "/register/form"; break;
								case "1001": window.location = "/"; break;
								case "1002": 
									alert("ขออภัยค่ะ คุณกรอก User และ Password ไม่ถูกต้อง"); 
									$('#overlay').attr('style', 'display: none;');
									$('#wrap-pop').attr('style', 'display: none;');
									$('#login').attr('style', 'display: none;');
									$('#overlay').css('height', 'auto');
									$('#login_user').focus();
									break;
								default: 
									alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
									location.reload();
									break;
							}
						},
						error: function() {
							alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
							location.reload();
						 }
					});
			   }
		});
		
		
 });
 
 function tournament_join(id) {
	 var obj = $('#btn_join_'+id);
	obj.button('loading');
	if($("#team_id").val() !="") {
		if(confirm("คุณต้องการร่วมเข้าแข่งขันรายการนี้หรือไม่ ?") == true) {
			var team_id = $("#team_id").val();
			var tour_id = id;
		
			$.ajax({
				url: '/register/join_validation',
				type: "POST",
				dataType : "json",			
				data: { team_id: team_id,tour_id:tour_id },
				success: function(response){
					switch (response.code) {
						case 100:
							obj.button('complete');
							obj.attr("class", "btn btn-success btn-plan-select");
							obj.html("<i class='icon-white icon-ok'></i> ร่วมแล้ว</span>"); 
							$('#team_num_'+id).html(response.team_num); 
							alert(response.msg); 
						break;
						case 101: alert(response.msg); location.reload();break;
						case 102: alert(response.msg); break;
						default: 
							alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
							location.reload();
							break;
					}
				},
				error: function() {
					alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
					location.reload();
				 }
			});
			
			return true;
		} else {
			obj.button('complete');
			return false;
		}
	} else {
		obj.button('complete');
		return false;
	}
}