<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class matches extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('team/m_team');
		$this->load->model('tournament/m_tournament');
		$this->load->model('news/m_news');
		$this->load->library('pagination');
		$this->load->model('m_frontend');
		$this->load->model('m_matches');
	}
	
	public function match_detail(){
		$this->load->model('chat/m_chat');
		$match_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data_main = array();
		$data_sidebar = array();
		
		$data_main["match_chat"] = $this->m_chat->match_chat($match_id);
		$data_sidebar = $this->m_frontend->sidebar_info();
		//var_dump($data_sidebar);
		$data_main["match_detail"] = $this->m_matches->match_detail($match_id);
		//var_dump($data_main["match_detail"]);
		$team1_id = intval($data_main["match_detail"][0]->team1_id);
		$team2_id = intval($data_main["match_detail"][0]->team2_id);
		
		$data_main["team1_detail"] = $this->m_team->team_detail_by_team_id($team1_id);
		$data_main["team2_detail"] = $this->m_team->team_detail_by_team_id($team2_id);
		$data_main["login"] = $data_sidebar["login"];
		$data_main["team_name"] = $data_sidebar["team_name"];
		$data_main["team_id"] = $data_sidebar["team_id"];
		$data_main["team_logo"] = $data_sidebar["team_logo"];
		
		
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar',$data_sidebar);
		$this->load->view('match_detail_view', $data_main);
		$this->load->view('frontend/footer');
	}
	
	public function update_score(){
		$this->load->model('matches/m_matches');
		$res = $this->m_matches->update_score();
		if($res == true) {
			echo 100;
		} else{
			echo 200;
		}
	}	
	
	public function match_detail_popup()
	{
		//$this->load->model('m_login');
		
		//$this->m_login->check_login();
		$data_main = array();
		
		$match_id =  $this->uri->segment(3);
		$this->load->model('matches/m_matches');
		$this->load->model('chat/m_chat');
		
		$data_main["match_detail"] = $this->m_matches->match_detail($match_id);
		$data_main["match_chat"] = $this->m_chat->match_chat($match_id);
		
		$data = array(
			'page' => 'matches'
		);
		
		$this->load->view('match_detail_popup', $data_main);
				
	}
	
	public function edit_match(){
		
		$this->load->model('matches/m_matches');
		$res = $this->m_matches->validation("edit");
		$challonge_match_id = intval($this->input->post('challonge_match_id'));
		if($res == true) {
			$data = array(
					'page' => 'matches'
			);
				
			$data_main["match_id"] = $challonge_match_id;
			$this->load->view('match_complete_popup', $data_main);
		} else {
			$this->load->view('match_complete_popup', $data_main);
		}
		
	}
	
	public function post_comment(){
		$this->load->model('matches/m_matches');
		$res = $this->m_matches->post_comment();
		if($res == true) {
			echo 100;
		} else{
			echo 200;
		}
	}
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */