<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Fps Thailand, เกาะติดสถานการณ์และวงการเกม FPS ของเมืองไทย</title>
<meta name="robots" content="nofollow,noindex" /> 
<meta name="description" content="เวบไซต์ที่รวบรวม ข่าวสารวงการเกมfps อุปกรณ์เล่นเกม คลิบการแข่งขัน ผู้เล่นระดับแนวหน้า แคลนดัง เทคนิคการเล่นเกม"> 
<meta name="keywords" content="fps, fps thailand, first person shooting, esport, e-sports, esports, counter strike, sudden attack, special force, point blank, sa, sf, pb, cso, cs, game online, ava , battery , เกมส์, เกมส์ต่อสู้, เกมส์ยิงปืน"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://fpsthailand.com/favicon.ico" > 
    <?=css_asset('bootstrap.css', 'frontend')?>
    <?=css_asset('main.css', 'frontend')?>
    <? //js_asset('jquery.min.js', 'frontend'); ?>
    <?=js_asset('jquery-1.8.2.min.js', 'frontend'); ?>
     <?=js_asset('bootstrap.js', 'frontend'); ?>
      <?=js_asset('jquery.validate.min.js', 'frontend'); ?>
    <?=js_asset('scripts.js', 'frontend'); ?>   
	<?=js_asset('jquery.fancybox.js','frontend'); ?>
	<?=css_asset('jquery.fancybox.css','frontend')?>
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
<style type="text/css">
body {
	margin:0 auto;
	background-color: #000000;
	background-repeat:no-repeat;
	background-position:center;
	background-position:top;
	background-attachment:fixed;
	height:100%; overflow-y:auto;
	color: #999999;
    font-family: "MS Sans Serif",Tahoma,verdana;
    
}

</style>
  </head>
<body>

 <div class="tournaments_wrapper">
               	<?php foreach($match_detail as $detail) {?>
               	<div class="post">
<form id="myForm" name="myForm"  class="uniform"  method="post" action="/matches/edit_match" enctype="multipart/form-data" >
				
					<fieldset>
						<h1>Match Detail : <?php echo $detail->challonge_match_id;?> </h1>
                        
						<dl class="inline">
                        	<dt><label for="name">Tournament Name</label></dt>
							<dd>
								<input type="text" id="tour_title" name="tour_title" class="medium required" size="50" value="<?php echo $detail->tour_title;?>" disabled="disabled" />
								<small>Tournament Title</small>
							</dd>
                            <dt><label for="name">Round</label></dt>
							<dd>
								<input type="text" id="tour_title" name="tour_title" class="medium required" size="50" value="<?php echo $detail->round;?>" disabled="disabled" />
								<small>Round Title</small>
							</dd>
                            <dt><label>Winner Team</label></dt>
                                <dd>
                                    <label><input type="radio" id="winner_id" name="winner_id" value="<?php echo $detail->player1_id;?>" <?php echo ($detail->winner_id == $detail->player1_id) ? "checked=\"checked\"" : "";?>  /><?php echo $detail->player1_name;?></label>
                                    <label><input type="radio" id="winner_id" name="winner_id" value="<?php echo $detail->player2_id;?>" <?php echo ($detail->winner_id == $detail->player2_id) ? "checked=\"checked\"" : "";?> /><?php echo $detail->player2_name;?></label>
                                </dd>
							

					</fieldset>
                    
                    <div class="buttons">
								<button type="submit" class="button">Submit</button>
								<button type="button" class="button white" onclick="javascript:window.parent.close_popup();">Cancel</button>
					</div>    
                    <input type="hidden" id="challonge_match_id" name="challonge_match_id" value="<?php echo $detail->challonge_match_id;?>"  />
                    <input type="hidden" id="tour_id" name="tour_id" value="<?php echo $detail->tour_id;?>"  />
                    <input type="hidden" id="challonge_tour_id" name="challonge_tour_id" value="<?php echo $detail->challonge_tour_id;?>"  />
                    <input type="hidden" id="player1_id" name="player1_id" value="<?php echo $detail->player1_id;?>"  />
                     <input type="hidden" id="player2_id" name="player2_id" value="<?php echo $detail->player2_id;?>"  />
                    
 				</form>
 				</div>
 				<div class="match_wrapper">
	 				<div class="post_comment">
	            	<h3 class="header">Comment : โพสเพื่อนัดแนะการแข่งขัน</h3>
	                    <div class="post_box">
	                    
	                    <?php if($match_chat) {?>
	                    <?php foreach($match_chat as $chat) {?>
	                     <div class="comment">
	                            <div class="img_post">
	                            <?php $team_chat_img = ($chat->team_logo) ? $chat->team_logo : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
	                                
	                                <a href="/team/<?php echo $chat->team_id;?>" title="" target="_blank"><img src="/timthumb.php?src=<?php echo IMGPATH_URL."/".$team_chat_img;?>&zc=1&w=80&h=80" width="80" height="80" class="postthumbnail"></a>
	                            </div>
	                            <div class="postlayout">
	                            <span><a href="/team/<?php echo $chat->team_id;?>" title="" target="_blank"><?php echo $chat->team_name;?></a></span> <span class="postintro">On <?php echo $chat->create_date;?></span>
	                            <div class="clear"></div>
	                             <div class="postteam">
	                             		<?php 
											
											$chat_msg = htmlspecialchars_decode($chat->chat_msg);
											echo $this->utility->bb_parse($chat_msg);
										?>
	                                  
	                             </div>       
	                         </div>
	                    </div>
	                    <?php } ?>
	                	<?php } else {?>
	                	ไม่มีข้อความ
	                	<?php } ?>
	            </div>
				  </div>
			  </div>
  <?php } ?>
  </body>
  </html>         
      