
<div class="content">
        <div class="match_wrapper">
            <h1 class="header">Match Detail</h1>
            
            <?php if($match_detail) { //var_dump($team1_detail); ?>
            	<?php if($team1_detail) { ?>
             <div class="post">
             		<?php $team1_img = ($team1_detail["team_logo"]) ? IMGPATH_URL."/".$team1_detail["team_logo"] : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                    <img src="/timthumb.php?src=<?php echo $team1_img;?>&zc=1&w=120&h=120" width="120" height="120" class="postthumbnail">
                    <div class="teamdetail-main">
                        <div class="teamdetail-list">
                          <ul>
                                <li><span class="head"><?php echo $team1_detail["team_name"]; ?></span> <?php echo team_star($team1_detail["team_point"]); ?></li>     
                                <?php if($team1_detail["member_list"]) {?>
                                <li><span class="leader">รายชื่อสมาชิก : </span></li>  
                                <li>
                                     <?php foreach ($team1_detail["member_list"] as $members => $member_list) { ?>
                                        	<span><?php echo $member_list->member_name; ?></span>
                                            <br />
                                            
                                          <?php  } ?>  
                                    
                                </li>              
                                <?php } ?>
                               <li><span class="leader">Score : </span> <?php echo $team1_detail["team_point"]; ?></li>  
                               <li><span class="leader">Win : </span><?php echo $team1_detail["team_win"]; ?></li>          
                                <li><span class="leader">Lose : </span><?php echo $team1_detail["team_lose"]; ?></li>                               
                          </ul>
                          </div>
                    </div>
            </div>
            <?php } ?>
            <div class="versus"><h1>VS</h1></div>
            <?php if($team2_detail) { ?>
            <div class="post">
                    <?php $team2_img = ($team2_detail["team_logo"]) ? IMGPATH_URL."/".$team2_detail["team_logo"] : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                    <img src="/timthumb.php?src=<?php echo $team2_img;?>&zc=1&w=120&h=120" width="120" height="120" class="postthumbnail">
                     <div class="teamdetail-main">
                        <div class="teamdetail-list">
                          <ul>
                                <li><span class="head"><?php echo $team2_detail["team_name"]; ?></span> <?php echo team_star($team2_detail["team_point"]); ?></li>     
                                <?php if($team2_detail["member_list"]) {?>
                                <li><span class="leader">รายชื่อสมาชิก : </span></li>  
                                <li>
                                     <?php foreach ($team2_detail["member_list"] as $members => $member_list) { ?>
                                        	<span><?php echo $member_list->member_name; ?></span>
                                            <br />
                                            
                                          <?php  } ?>  
                                    
                                </li>              
                                <?php } ?>
                               <li><span class="leader">Score : </span> <?php echo $team2_detail["team_point"]; ?></li>  
                               <li><span class="leader">Win : </span><?php echo $team2_detail["team_win"]; ?></li>          
                                <li><span class="leader">Lose : </span><?php echo $team2_detail["team_lose"]; ?></li>                               
                          </ul>
                          </div>
                    </div>
            </div>
            <?php } ?>
            <div class="clear"></div>
            <?php if($login == 1 && ($team_id == $team1_detail["team_id"] || $user_id == $team2_detail["team_id"])) { ?>
            <div style="margin:20px 20px 20px 0;">
            <span href="#main_tour" class="btn btn-success btn-plan-select" id="btn_post_score" name="btn_post_score"><i class="icon-white icon-ok"></i> แจ้งผลการแข่งขัน</span>
            </div>
            <?php } ?>
            <?php if(!empty($match_detail[0]->winner_id)) { ?>
            <div class="winner">
                <h2 class="header">Winner</h2>
                <h3><?php echo ($match_detail[0]->winner_id == $match_detail[0]->player1_id) ? $match_detail[0]->player1_name : $match_detail[0]->player2_name ;?></h3>
            </div>
 			 <?php } ?>        
 			  
            <div class="post_comment">
            	<h3 class="header">Comment : โพสเพื่อนัดแนะการแข่งขัน</h3>
                    <div class="post_box">
                    <?php  if($login == 1 && ($team_id == $team1_detail["team_id"] || $user_id == $team2_detail["team_id"])) { ?>
                     
                     <div class="comment" style="background-color:#4E4E4E;">
                            <div class="img_post">
                            <?php $team_logo_img = ($team_logo) ? $team_logo : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                                <a href="/team/<?php echo $team_id;?>" title="" target="_blank"><img src="/timthumb.php?src=<?php echo $team_logo_img;?>&zc=1&w=80&h=80" width="80" height="80" class="postthumbnail"></a>
                            </div>
                            <div class="postlayout">
                            <span><a href="/team/<?php echo $team_id;?>"><?php echo $team_name;?></a></span> 
                            <div class="clear"></div>
                             
                             <div class="postteam">
                                    <textarea id="txt_comment" name="txt_comment" rows="4" style="width:460px;"></textarea>
                              <button type="button" class="btn btn-info" id="btn_comments" name="btn_comments">Comment</button>
                              <input type="hidden" id="match_id" name="match_id" value="<?php echo $match_detail[0]->challonge_match_id;?>" />
            				  <input type="hidden" id="team_id" name="team_id" value="<?php echo $team_id;?>" />
            				  <input type="hidden" id="tour_id" name="tour_id" value="<?php echo $match_detail[0]->tour_id;?>" />
                             </div>
                    </div>
                    </div> 
                    <?php } ?>
                    <?php if($match_chat) {?>
                    <?php foreach($match_chat as $chat) {?>
                     <div class="comment">
                            <div class="img_post">
                            <?php $team_chat_img = ($chat->team_logo) ? $chat->team_logo : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                                
                                <a href="/team/<?php echo $chat->team_id;?>" title="" target="_blank"><img src="/timthumb.php?src=<?php echo IMGPATH_URL."/".$team_chat_img;?>&zc=1&w=80&h=80" width="80" height="80" class="postthumbnail"></a>
                            </div>
                            <div class="postlayout">
                            <span><a href="/team/<?php echo $chat->team_id;?>" title="" target="_blank"><?php echo $chat->team_name;?></a></span> <span class="postintro">On <?php echo $chat->create_date;?></span>
                            <div class="clear"></div>
                             <div class="postteam">
                             		<?php 
										
										$chat_msg = htmlspecialchars_decode($chat->chat_msg);
										echo $this->utility->bb_parse($chat_msg);
									?>
                                  
                             </div>       
                         </div>
                    </div>
                    <?php } ?>
                	<?php } ?>
            </div>
           
</div>
<?php } ?>
</div>
<div id="wrap-score" style="display:none;">
    <div id="post-score" style="display: none;">
        <div class="loading-box">
        <p>ทำการใส่ลิงค์รูปภาพ เพื่อแจ้งผลการแข่งขัน</p>
        <p><input type="text" id="txt_score_pic" name="txt_score_pic" /></p>
        <p><button type="button" class="btn btn-info" id="btn_up_pic" name="btn_up_pic">Update Score</button></p>
        </div>
    </div>
</div>
</div>