<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_matches extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database('default');
		$this->process_id = $this->session->userdata('session_id');
	}

	function validation($mode ="add")
	{
		$res = false;
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('winner_id', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('tour_id', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('challonge_match_id', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('challonge_tour_id', '', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			return $res;
		}
		else
		{
			if($mode == "add") {
				$res = $this->create_match();
			} else {
				$res = $this->edit_match();
			}
			return $res;
		}
	}
	
	public function update_score() {
		$res_add = false;
		
		$challonge_match_id = intval($this->input->post('match_id'));
		$score_img = htmlspecialchars(trim($this->input->post('score_pic')));
		$time_process = date("Y-m-d H:i:s", time());
		$score_images = array(
		   'challonge_match_id' => $challonge_match_id,
		   'score_img' =>  $score_img,
		   'create_date' => $time_process,
		   'update_date' => $time_process
		);
		
		$this->db->insert('tour_matches_score' , $score_images);
		$tour_id = $this->db->insert_id();
		$logs = $this->process_id."|1||m_matches.update_score|Update_score_match|2|challonge_match_id:".$challonge_match_id.",score_img:".$score_img."\n";
		$this->utility->writeLog("update_score_match",$logs);
		$res_add = true;
		return $res_add; 
	}  
	
	public function post_comment() {
		$res_add = false;
		
		$challonge_match_id = intval($this->input->post('match_id'));
		$tour_id = intval($this->input->post('tour_id'));
		$team_id = intval($this->input->post('team_id'));
		$chat_msg = htmlspecialchars(trim($this->input->post('comment')));
		$time_process = date("Y-m-d H:i:s", time());
		
		$chat_log = array(
		   'challonge_match_id' => $challonge_match_id,
		   'tour_id' =>  $tour_id,
		   'team_id' =>  $team_id,
		   'chat_msg' =>  $chat_msg,
		   'chat_active' =>  1,
		   'create_date' => $time_process,
		   'update_date' => $time_process
		);
		
		$this->db->insert('tour_chat' , $chat_log);
		$chat_id = $this->db->insert_id();
		$logs = $this->process_id."|1||m_matches.post_comment|Post_Comment|2|challonge_match_id:".$challonge_match_id.",tour_id:".$tour_id.",chat_id:".$chat_id.",chat_msg:".$chat_msg."\n";
		$this->utility->writeLog("post_comment",$logs);
		$res_add = true;
		return $res_add; 
	}  

	public function edit_match() {
		$res_edit = false;
		
		$time_process = date("Y-m-d H:i:s", time());
		$winner_id= intval($this->input->post('winner_id'));
		$player1_id= intval($this->input->post('player1_id'));
		$player2_id= intval($this->input->post('player2_id'));
		$tour_id = intval($this->input->post('tour_id'));
		$challonge_tour_id= intval($this->input->post('challonge_tour_id'));
		$challonge_match_id= intval($this->input->post('challonge_match_id'));
		$scores_csv = ($winner_id == $player1_id) ? "1-0" : "0-1";
		$player1_votes = ($winner_id == $player1_id) ? 1 : 0;
		$player2_votes = ($winner_id == $player2_id) ? 1 : 0;
		
		$this->load->library('challonge/challongeapi');
		ChallongeAPI::$api_key = challongeAPI_KEY;
		$tournament = new ChallongeMatch($challonge_tour_id);
		
		$tournament->setParams(array(
			'match_id' => $challonge_match_id,
			'match' => array(
				'scores_csv' => $scores_csv,
				'winner_id' => $winner_id,
				'player1_votes' => $player1_votes,
				'player2_votes' => $player2_votes
			)
		));
		
		// Call Challonge API for Update Match  
		$xml = $tournament->reqUpdate($challonge_match_id);
		$loser_id =  intval($xml->{'loser-id'});
		$state =  trim($xml->{'state'});
	
		$update_match = array(
			'winner_id' => $winner_id,
			'loser_id' => $loser_id,
			'scores_csv' => $scores_csv,
			'state' => $state,
			'update_date' => $time_process
		);
		
		// Update challonge Match in challonge_matches Table
		$this->db->update('challonge_matches', $update_match, "challonge_match_id = ".$challonge_match_id);
		
		// Write Log about  Update Match for Challonge API
		$logs = $this->process_id."|1||m_matches.edit_match|create_challonge_participants|2|challonge_match_id:".$challonge_match_id.",challonge_tour_id:".$challonge_tour_id.",winner_id:".$winner_id.",loser_id:".$loser_id.",scores_csv:".$tscores_csv.",xml_res:".$xml->asXML()."\n";
		$this->utility->writeLog("challonge_api",$logs);
		$res_edit = TRUE;
		
		return $res_edit;
	}

	public function all_record_count() {

        return $this->db->count_all("challonge_matches");
	}

	function match_list($limit = 0, $start = 0, $admin=0)
	{
		
		$this->db->select('a.tour_match_id, a.challonge_match_id, a.challonge_tour_id,a.round, a.tour_id, b.tour_title, c.participant_name as player1_name, d.participant_name as player2_name');

		//$this->db->where('challonge_matches a ', 1); 
		$this->db->from('challonge_matches a');
		$this->db->join('tour_tournaments b', 'a.tour_id = b.tour_id');
		$this->db->join('challonge_participants c', 'a.player1_id = c.participant_id', 'left');
		$this->db->join('challonge_participants d', 'a.player2_id = d.participant_id','left');
		/*if($admin == 0) { 
			$this->db->where('a.tour_hilight', 0); 
			$this->db->where('a.tour_active', 1); 
		}*/
		$this->db->limit($limit, $start);
		$this->db->order_by("challonge_tour_id", "DESC"); 
		
		$query = $this->db->get();
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	
	}
	
	public function round_list($tourID = 0) {
		$this->db->select('a.challonge_match_id, a.challonge_tour_id,a.round,a.round, a.match_date');

		$this->db->from('challonge_matches a');
		$this->db->join('tour_tournaments b', 'a.tour_id = b.tour_id');
		$this->db->join('challonge_participants c', 'a.player1_id = c.participant_id');
		$this->db->join('challonge_participants d', 'a.player2_id = d.participant_id');
		$this->db->where('a.tour_id', $tourID); 
		$this->db->limit(1);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
    }	
	
	function tour_match_list($tourID = 0) {
		$this->db->select('a.tour_match_id, a.challonge_match_id, a.challonge_tour_id,a.round, a.tour_id, b.tour_title, a.player1_id,a.player2_id,a.winner_id,a.loser_id,c.participant_name as player1_name, d.participant_name as player2_name');

		$this->db->from('challonge_matches a');
		$this->db->join('tour_tournaments b', 'a.tour_id = b.tour_id');
		$this->db->join('challonge_participants c', 'a.player1_id = c.participant_id');
		$this->db->join('challonge_participants d', 'a.player2_id = d.participant_id');
		$this->db->where('a.tour_id', $tourID); 
		//$this->db->limit(1);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	}
	
	function match_list_by_team($teamID = 0) {
		$this->db->select('a.tour_match_id, a.challonge_match_id, a.challonge_tour_id,a.round, a.tour_id, b.tour_title, a.player1_id,a.player2_id,a.winner_id,a.loser_id,c.participant_name as player1_name, d.participant_name as player2_name, a.scores_csv');

		$this->db->from('challonge_matches a');
		$this->db->join('tour_tournaments b', 'a.tour_id = b.tour_id');
		$this->db->join('challonge_participants c', 'a.player1_id = c.participant_id');
		$this->db->join('challonge_participants d', 'a.player2_id = d.participant_id');
		$where = "c.team_id=".$teamID." OR d.team_id=".$teamID;
		//$where = team_id=".$teamID." OR d.team_id=".$teamID;
		//$this->db->where($where);
		$this->db->where('c.team_id', $teamID);
		$this->db->or_where('d.team_id', $teamID);  
		//$this->db->limit(1);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	}
	
	function match_detail($matchID = 0){
		
		$this->db->select('a.tour_match_id, a.challonge_match_id, a.challonge_tour_id,a.round, a.tour_id, b.tour_title, a.player1_id,a.player2_id,a.winner_id,a.loser_id,c.team_id as team1_id, c.participant_name as player1_name,d.team_id as team2_id, d.participant_name as player2_name');

		$this->db->from('challonge_matches a');
		$this->db->join('tour_tournaments b', 'a.tour_id = b.tour_id');
		$this->db->join('challonge_participants c', 'a.player1_id = c.participant_id');
		$this->db->join('challonge_participants d', 'a.player2_id = d.participant_id');
		$this->db->where('a.challonge_match_id', $matchID); 
		$this->db->limit(1);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}

	
}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */