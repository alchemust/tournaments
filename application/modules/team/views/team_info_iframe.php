<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Fps Thailand, เกาะติดสถานการณ์และวงการเกม FPS ของเมืองไทย</title>
<meta name="robots" content="nofollow,noindex" /> 
<meta name="description" content="เวบไซต์ที่รวบรวม ข่าวสารวงการเกมfps อุปกรณ์เล่นเกม คลิบการแข่งขัน ผู้เล่นระดับแนวหน้า แคลนดัง เทคนิคการเล่นเกม"> 
<meta name="keywords" content="fps, fps thailand, first person shooting, esport, e-sports, esports, counter strike, sudden attack, special force, point blank, sa, sf, pb, cso, cs, game online, ava , battery , เกมส์, เกมส์ต่อสู้, เกมส์ยิงปืน"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://fpsthailand.com/favicon.ico" > 
    <?=css_asset('bootstrap.css', 'frontend')?>
    <?=css_asset('main.css', 'frontend')?>
    <? //js_asset('jquery.min.js', 'frontend'); ?>
    <?=js_asset('jquery-1.8.2.min.js', 'frontend'); ?>
     <?=js_asset('bootstrap.js', 'frontend'); ?>
      <?=js_asset('jquery.validate.min.js', 'frontend'); ?>
    <?=js_asset('scripts.js', 'frontend'); ?>   
	<?=js_asset('jquery.fancybox.js','frontend'); ?>
	<?=css_asset('jquery.fancybox.css','frontend')?>
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
<style type="text/css">
body {
	margin:0 auto;
	background-color: #000000;
	background-repeat:no-repeat;
	background-position:center;
	background-position:top;
	background-attachment:fixed;
	height:100%; overflow-y:auto;
	color: #999999;
    font-family: "MS Sans Serif",Tahoma,verdana;
    
}

</style>
  </head>
<body>

               	<div class="tournaments_wrapper">

                    <?php if($team_detail) { ?>
                     <div class="post">
                          <?php $team_img = ($team_detail["team_logo"]) ? IMGPATH_URL."/".$team_detail["team_logo"] : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                             <img src="/timthumb.php?src=<?php echo $team_img;?>&zc=1&w=185&h=185" width="185" height="185" class="postthumbnail">
                            <div class="teamdetail-main">
                                <div class="teamdetail-list">
                                  <ul>
                                        <li><span class="head"><?php echo $team_detail["team_name"]; ?></span> <?php echo team_star($team_detail["team_point"]); ?></li>     
                                        <li style="margin-bottom: 10px;"><span class="leader">รายชื่อสมาชิก : </span></li>  
                                        <?php if($team_detail["member_list"]) {?>
                                        <li>
                                          <?php foreach ($team_detail["member_list"] as $members => $member_list) { ?>
                                          <?php $member_logo = ($member_list->member_logo) ? IMGPATH_URL."/".$member_list->member_logo : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                                        	<div style="height: 30px;"><img src="/timthumb.php?src=<?php echo $member_logo;?>&zc=1&w=40&h=40" /> <?php echo $member_list->member_name; ?></div>
                                            <br/>
                                            
                                            
                                          <?php  } ?>  
                                        </li>    
                                        <?php  } ?>        
                                         <li style="border-top: 1px solid #666666;width:300px;"></li>     
                                        <li><span class="leader" >Score : </span> <?php echo $team_detail["team_point"]; ?></li>  
                                        <li><span class="leader">Win : </span><?php echo $team_detail["team_win"]; ?></li>          
                                        <li><span class="leader">Lose : </span><?php echo $team_detail["team_lose"]; ?></li>                               
                                  </ul>
                                  </div>
            				</div>
            				
                    </div>
                    <?php if($match_list) { ?>
                    <!-- <div class="post">
                    	<span style="color:#FF8A00;">สถิติการแข่งขัน</span>
                        <div id="members-menu">
                        <ul>
                            <li>
                                <div class="team-main-head">ชื่อทีม</div>
                                <div class="team-enemy-head">คู่แข่ง</div>
                                <div class="team-win-head">Win</div>
                                <div class="team-lost-head">Lose</div>
                            </li>
                            <?php foreach($match_list as $match) { 
                                  list($player1_point, $player2_point) = explode("-",$match->scores_csv);
                             ?>
                             <li class="menu1">
                                 <div class="team-main"><a href="/match_detail/<?php echo $match->challonge_match_id;?>" ><?php echo $match->player1_name;?></a></div>
                                <div class="team-enemy"><a href="/match_detail/<?php echo $match->challonge_match_id;?>" ><?php echo $match->player2_name;?></a></div>
                                <div class="team-win"><?php echo $player1_point;?></div>
                                <div class="team-lost"><?php echo $player2_point;?></div>
                           </li>
                           <?php } ?>
                       </ul>
                    </div>
                </div> -->
                  <?php } ?>
               <?php } ?>
  </div>
  
  </body>
  </html>         
      