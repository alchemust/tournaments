<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class team extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		//$this->load->model('m_frontend');
		//$this->load->model('backend/products');
	}
	
	function team_info(){
		$team_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$data_main = array();
		$data_sidebar = array();
		
		$this->load->model('m_frontend');
		$this->load->model('team/m_team');
		$this->load->model('matches/m_matches');
		
		
		$data_sidebar = $this->m_frontend->sidebar_info();
		
		//$user_id = ($this->m_authen->getsession("user_id")) ? $this->m_authen->getsession("user_id") : 0;
		
		//$team_id = ($this->m_authen->getsession("team_id")) ? $this->m_authen->getsession("team_id") : 0;
		$data_main["team_detail"] = $this->m_team->team_detail_by_team_id($team_id);
		//$team_id = intval($data_main["team_detail"]["team_id"]);
		$data_main["match_list"] = $this->m_matches->match_list_by_team($team_id);
		
		
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar',$data_sidebar);
		$this->load->view('team_info', $data_main);
		$this->load->view('frontend/footer');
	}
	
     function team_info_iframe(){
		$team_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$data_main = array();
		$data_sidebar = array();
		
		$this->load->model('m_frontend');
		$this->load->model('team/m_team');
		$this->load->model('matches/m_matches');
		
		
		$data_sidebar = $this->m_frontend->sidebar_info();
		
		//$user_id = ($this->m_authen->getsession("user_id")) ? $this->m_authen->getsession("user_id") : 0;
		
		//$team_id = ($this->m_authen->getsession("team_id")) ? $this->m_authen->getsession("team_id") : 0;
		$data_main["team_detail"] = $this->m_team->team_detail_by_team_id($team_id);
		//$team_id = intval($data_main["team_detail"]["team_id"]);
		$data_main["match_list"] = $this->m_matches->match_list_by_team($team_id);
		
		
		$this->load->view('team_info_iframe', $data_main);
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */