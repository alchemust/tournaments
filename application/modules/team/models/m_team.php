<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_team extends CI_Model {
	var $process_id;
	var $key;
	
	function __construct()
	{
		parent::__construct();

		//LOAD DB DEFAULT
		$this->key = KEY;
		$this->process_id = $this->session->userdata('session_id');
		$this->load->database('default');
	}

	function index()
	{
		$this->validation();
	}
	
	function join_validation(){
		$res = array("code" => 101, "msg" => "ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ");
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('team_id', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('tour_id', '', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			return json_encode($res);
		}
		else
		{
			$res = $this->join_tournament();
			return json_encode($res);
			
		}
	}
	
	public function join_tournament(){
		
		$res = array("code" => 101, "msg" => "ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ");
		$team_id = intval($this->input->post('team_id'));
		$tour_id = intval($this->input->post('tour_id'));
		
		if(!$this->check_duplicate_join($team_id,$tour_id)) {
			$this->load->model('tournament/m_tournament');
			
			$time_process = date("Y-m-d H:i:s", time());
			
			$members = array(
			   'team_id' => $team_id,
			   'tour_id' => $tour_id,
			   'create_date' => $time_process
			);
		
			$this->db->insert('tour_team_regis' , $members);
			$tour_regis_id = $this->db->insert_id();
			$logs = $this->process_id."|1||M_team.join_tournament|Team_Join_Tournament|2|tour_regis_id:".$tour_regis_id.",team_id:".$team_id.",tour_id:".$tour_id."\n";
			$this->utility->writeLog("join_tournament",$logs);
			
			$update_team_num = $this->m_tournament->update_team_num($tour_id);
			
			$res_add = array("code" => 100, "msg" => "คุณได้เข้าร่วมการแข่งขันนี้เรียบร้อยแล้วค่ะ", "team_num" => $update_team_num);
			
		} else {
			$res_add = array("code" => 102, "msg" => "คุณเคยเข้าร่วมการแข่งขันนี้แล้วค่ะ");
		}
		return $res_add;
	}
	
	public function all_record_count() {

        return $this->db->count_all("tour_teams");
	}
	
	function team_validation($mode ="1")
	{
		$res = 0;
		$this->load->library('form_validation');
		
		if($mode == "1") {
			$this->form_validation->set_rules('team_name', '', 'trim|required|xss_clean');
			$this->form_validation->set_rules('team_desc', '', 'trim|required|xss_clean');
		} else {
			$this->form_validation->set_rules('team_desc', '', 'trim|required|xss_clean');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			return $res;
		}
		else
		{
			if($mode == "1") {
				$res = $this->create_team();
			} else if($mode == "2") {
				$res = $this->edit_team();
			}
			 
			return $res; 
			
		}
	}
	
	function team_rankings_list($limit = 10) {
		
		$this->db->select('a.team_id, a.user_id, a.team_name, a.team_desc, a.team_logo, a.member_num, a.team_win, a.team_lose');

		$this->db->where('team_active', 1); 
		$this->db->from('tour_teams a');
		$this->db->limit($limit);
		$this->db->order_by("team_point", "DESC"); 
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	}
	
	function team_list($limit = 0, $start = 10, $admin=0){
		$this->db->select('a.team_id, a.user_id, b.user_name, a.team_name, a.team_desc, a.team_logo, a.member_num, a.team_win, a.team_lose');


		$this->db->from('tour_teams a');
		$this->db->join('tour_users b', 'a.user_id = b.user_id');
		if($admin == 0) { 
			$this->db->where('a.team_active', 1); 
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		//$tournament_list = "select tour_id, game_id, tour_title, tour_excerpt, tour_rule, tour_award, tour_regis_start, tour_regis_end, tour_match_start, tour_match_end, tour_small_img, tour_medium_img, tour_large_img, tour_point, tour_hilight, tour_active from tour_tournaments where tour_hilight = 0 AND tour_active = 1 limit ".$limit.", ".$start."  ORDER BY update_date DESC ";
		
		
		//$product_list = array();
		//$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	
	function team_detail($teamID = 0)
	{
		
		$this->db->select('a.tour_id, a.game_id, a.tour_title, a.tour_excerpt, a.tour_rule, a.tour_award, a.tour_regis_start, a.tour_regis_end, a.tour_match_start, a.tour_match_end, a.tour_small_img, a.tour_medium_img, a.tour_large_img, a.tour_team_num, a.tour_point, a.tour_hilight, a.tour_active');

		$this->db->from('tour_tournaments a');
		
		$this->db->where('a.tour_id', $teamID); 
		$this->db->limit(1);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	public function team_gamepoint($teamID){
		$this->db->select('a.team_id, a.game_id, b.game_name, b.game_logo, a.game_point');

		$this->db->from('tour_team_point a');
		$this->db->join('tour_games b', 'a.game_id = b.game_id');
		$this->db->where('a.team_id', $teamID); 
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	private function create_team() {
		$res_add = FALSE;
		$team_name = trim($this->input->post('team_name'));
		$team_desc = trim($this->input->post('team_desc'));
		$member_num = count(array_filter($_POST["member_name"]));
		$user_id = intval($this->m_authen->getsession("user_id"));
		$user_ip = $this->input->ip_address();
		$time_process = date("Y-m-d H:i:s", time());
		
		if($user_id != 0) {
			if(!$this->check_duplicate_team($team_name)) {
				
					##### Upload Logo Team #########	
					if ( !empty($_FILES['logo_img']['name']) ) { 
						preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $_FILES['logo_img']['name'], $matches);
						// Location of thumbnail on server.
						$source_ext = strtolower($matches[0]);	
						$thumbFileName = time().rand(0,100);
						$datetime = date('Y-m');
					
						$dirImg = IMGPATH."/".$datetime."/";
						
						if(!file_exists($dirImg)) mkdir($dirImg,0777);
						@chmod($dirImg,0777);
						
						$loc = $dirImg . $thumbFileName.$source_ext;
						$team_logo = $datetime."/".$thumbFileName.$source_ext;
						
						// Attempt to move the uploaded thumbnail to the thumbnail directory.
						if ( !empty($_FILES['logo_img']['tmp_name']) && move_uploaded_file($_FILES['logo_img']['tmp_name'], $loc) ) 
							//@unlink(CONTENT_DIR . $new_img);
							$thumbUploaded = true;
									
					} else {
						$thumbUploaded = false;
					}
					
					$teams = array(
					   'user_id' => $user_id,
					   'team_name' => $team_name,
					   'team_desc' => $team_desc,
					   'team_logo' => $team_logo,
					   'member_num' => $member_num,
					   'team_active' => 1,
					   'create_date' => $time_process
					);
					
					
					$this->db->insert('tour_teams' , $teams);
					$team_id = $this->db->insert_id();
					$logs = $this->process_id."|1||M_team.create_team|Register_team|2|team_id:".$team_id.",user_id:".$user_id.",team_name:".$team_name.",team_logo:".$team_logo.",member_num:".$member_num.",user_ip:".$user_ip."\n";
					$this->utility->writeLog("register_team",$logs);
					
					//Insert Member By Team ID
					foreach($_POST['member_name'] as $key => $value) {
						$member_name = trim($value);
						if(!empty($member_name)) {
							if ( !empty($_FILES['img_member']['name'][$key]) ) { 
								preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $_FILES['img_member']['name'][$key], $matches);
								// Location of thumbnail on server.
								$source_ext = strtolower($matches[0]);	
								$thumbFileName = time().rand(100,300);
								$datetime = date('Y-m');
							
								$dirImg = IMGPATH."/".$datetime."/";
								
								if(!file_exists($dirImg)) mkdir($dirImg,0777);
								@chmod($dirImg,0777);
								
								$loc = $dirImg . $thumbFileName.$source_ext;
								$member_logo = $datetime."/".$thumbFileName.$source_ext;
								
								// Attempt to move the uploaded thumbnail to the thumbnail directory.
								if ( !empty($_FILES['img_member']['tmp_name'][$key]) && move_uploaded_file($_FILES['img_member']['tmp_name'][$key], $loc) ) 
									//@unlink(CONTENT_DIR . $new_img);
									$thumbUploaded = true;
									
									
							} else {
								$member_logo = "";
								$thumbUploaded = false;
							}
							
							$this->create_member_team($team_id, $member_name, $member_logo);
						} else {
							$member_logo = "";
							$thumbUploaded = false;
						}
					}
					
					$users = array(
					   'team_active' => 1,
					   'update_date' => $time_process
					);
			
					$this->db->update('tour_users', $users, "user_id = ".$user_id);
					
					
					$this->m_authen->setsession('team_active', 1);
					 
					$res_add = 100;
			} else {
				$res_add = FALSE;
			} 
		} else {
			$logs = $this->process_id."|1||M_team.create_team|Register_team|4|user_id:".$user_id.",team_name:".$team_name.",user_ip:".$user_ip."\n";
			$this->utility->writeLog("register_team",$logs);
			$res_add = FALSE;
		}
		
		return $res_add;
	}
	
	private function edit_team() {
		$res_edit = FALSE;
		
		$team_desc = trim($this->input->post('team_desc'));
		$member_num = count(array_filter($_POST["member_name"]));
		$user_id = intval($this->m_authen->getsession("user_id"));
		$team_id = intval($this->input->post('team_id'));
		$user_ip = $this->input->ip_address();
		$current_logo_img = trim($this->input->post('current_logo_img'));
		$time_process = date("Y-m-d H:i:s", time());
		
		
		if($user_id != 0) {
				
					##### Upload Logo Team #########	
					if ( !empty($_FILES['logo_img']['name']) ) { 
						preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $_FILES['logo_img']['name'], $matches);
						// Location of thumbnail on server.
						if(!empty($current_logo_img)) {
							@unlink(IMGPATH ."/". $current_logo_img);
						}
						$source_ext = strtolower($matches[0]);	
						$thumbFileName = time().rand(0,100);
						$datetime = date('Y-m');
					
						$dirImg = IMGPATH."/".$datetime."/";
						
						if(!file_exists($dirImg)) mkdir($dirImg,0777);
						@chmod($dirImg,0777);
						
						$loc = $dirImg . $thumbFileName.$source_ext;
						$team_logo = $datetime."/".$thumbFileName.$source_ext;
						
						// Attempt to move the uploaded thumbnail to the thumbnail directory.
						if ( !empty($_FILES['logo_img']['tmp_name']) && move_uploaded_file($_FILES['logo_img']['tmp_name'], $loc) ) 
							//@unlink(CONTENT_DIR . $new_img);
							$thumbUploaded = true;
									
					} else {
						$thumbUploaded = false;
						$team_logo = $current_logo_img; 
					}
					
					$teams = array(
					   'team_desc' => $team_desc,
					   'team_logo' => $team_logo,
					   'member_num' => $member_num,
					   'team_active' => 1,
					   'update_date' => $time_process
					);
					
					
					$this->db->update('tour_teams', $teams, "user_id = ".$user_id);
					
					$logs = $this->process_id."|1||M_team.edit_team|Edit_team|2|team_id:".$team_id.",user_id:".$user_id.",team_logo:".$team_logo.",member_num:".$member_num.",user_ip:".$user_ip."\n";
					$this->utility->writeLog("edit_team_profile",$logs);
					
					//Insert Member By Team ID
					foreach($_POST['member_name'] as $key => $value) {
						$member_name = trim($value);
						$img_member_current = $_POST["img_member_current"][$key];
						$member_id = $_POST["member_id"][$key];
						if(!empty($member_name)) {
							if ( !empty($_FILES['img_member']['name'][$key]) ) { 
								preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $_FILES['img_member']['name'][$key], $matches);
								// Location of thumbnail on server.
								
								if(!empty($img_member_current)) {
									@unlink(IMGPATH ."/". $img_member_current);
								}
								$source_ext = strtolower($matches[0]);	
								$thumbFileName = time().rand(100,300);
								$datetime = date('Y-m');
							
								$dirImg = IMGPATH."/".$datetime."/";
								
								if(!file_exists($dirImg)) mkdir($dirImg,0777);
								@chmod($dirImg,0777);
								
								$loc = $dirImg . $thumbFileName.$source_ext;
								$member_logo = $datetime."/".$thumbFileName.$source_ext;
								
								// Attempt to move the uploaded thumbnail to the thumbnail directory.
								if ( !empty($_FILES['img_member']['tmp_name'][$key]) && move_uploaded_file($_FILES['img_member']['tmp_name'][$key], $loc) ) 
									//@unlink(CONTENT_DIR . $new_img);
									$thumbUploaded = true;
									
									
							} else {
								$member_logo = $img_member_current;
								$thumbUploaded = false;
							}
							
							$this->edit_member_team($member_id, $member_name, $member_logo);
						} else {
							$member_logo = $img_member_current;
							$thumbUploaded = false;
						}
					}
					
					$users = array(
					   'team_active' => 1,
					   'update_date' => $time_process
					);
			
					$this->db->update('tour_users', $users, "user_id = ".$user_id);
					
					
					$this->m_authen->setsession('team_active', 1);
					 
					$res_edit = 100;
			
		} else {
			$logs = $this->process_id."|1||M_team.edit_team|edit_team|4|user_id:".$user_id.",team_name:".$team_name.",user_ip:".$user_ip."\n";
			$this->utility->writeLog("edit_team_profile",$logs);
			$res_edit = FALSE;
		}
		
		return $res_edit;
	}
	
	function create_member_team($team_id, $member_name,$member_logo){
		$time_process = date("Y-m-d H:i:s", time());
		
		$members = array(
		   'team_id' => $team_id,
		   'member_name' => $member_name,
		   'member_logo' => $member_logo,
		   'member_active' => 1,
		   'create_date' => $time_process
		);
	
		$this->db->insert('tour_members' , $members);
		$member_id = $this->db->insert_id();
		$logs = $this->process_id."|1||M_team.create_member_team|create_member_name|2|team_id:".$team_id.",member_name:".$member_name.",member_logo:".$member_logo."\n";
		$this->utility->writeLog("register_team",$logs);
		
	}
	
function edit_member_team($member_id, $member_name,$member_logo){
		$time_process = date("Y-m-d H:i:s", time());
		
		$members = array(
		   'member_name' => $member_name,
		   'member_logo' => $member_logo,
		   'member_active' => 1,
		   'update_date' => $time_process
		);
	
		$this->db->update('tour_members', $members, "member_id = ".$member_id);
		$logs = $this->process_id."|1||M_team.edit_member_team|edit_member_name|2|member_id:".$member_id.",member_name:".$member_name.",member_logo:".$member_logo."\n";
		$this->utility->writeLog("edit_team_profile",$logs);
		
	}
	
	
	function team_join_tourlist($teamID = 0)
	{
		
		$this->db->select('a.tour_id');

		$this->db->from('tour_team_regis a');
		$this->db->where('a.team_id', $teamID); 
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			$tourlist = array();
			foreach ($query->result() as $row){
					$tourlist[count($tourlist)] = $row->tour_id;
			}
			
			return $tourlist;
		}else {
			return false;
		}
		
	}
	
    function team_number_in_tournament($tourID = 0){
    	
		$this->db->select('a.tour_id,a.team_id,b.team_name,b.team_logo');

		$this->db->from('tour_team_regis a');
		$this->db->join('tour_teams b', 'a.team_id = b.team_id');
		$this->db->where('a.tour_id', $tourID); 
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	function load_team_detail($user_id = 0)
	{
		$member_deital = "";
		$user_id = intval($user_id);
		$sql ="select a.team_id, a.user_id, a.team_name, a.team_desc, a.team_logo, a.member_num, a.team_win, a.team_lose,a.team_point from tour_teams a,tour_users b WHERE a.user_id = b.user_id AND b.user_active = 1 AND b.user_id = ".$user_id."  limit 1"; 
		
		$product_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			
			foreach ($query->result() as $row){
				$tour_join = $this->team_join_tourlist($row->team_id);
				$team_member_list = $this->team_member_list($row->team_id);
			
				$member_deital = array(
				   'team_id' => $row->team_id,
				   'user_id' => $row->user_id,
				   'team_name' => $row->team_name,
				   'team_desc' =>  $row->team_desc,
				   'team_logo' =>  $row->team_logo,
				   'member_num' =>$row->member_num,
				   'team_win' =>$row->team_win,
				   'team_lose' => $row->team_lose,
				   'team_point' => $row->team_point,
				   'tour_join' => $tour_join,
				   'member_list' => $team_member_list
				);
			}
		}else {
			$member_deital = "";
		}
		
		return $member_deital;
	}
	
   function team_detail_by_team_id($teamID = 0)
	{
		$member_deital = "";
		$teamID = intval($teamID);
		$sql ="select a.team_id, a.user_id, a.team_name, a.team_desc, a.team_logo, a.member_num, a.team_win, a.team_lose,a.team_point,a.team_active from tour_teams a,tour_users b WHERE a.user_id = b.user_id AND b.user_active = 1 AND a.team_id = ".$teamID."  limit 1"; 
		
		$product_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			
			foreach ($query->result() as $row){
				$tour_join = $this->team_join_tourlist($row->team_id);
				$team_member_list = $this->team_member_list($row->team_id);
			
				$member_deital = array(
				   'team_id' => $row->team_id,
				   'user_id' => $row->user_id,
				   'team_name' => $row->team_name,
				   'team_desc' =>  $row->team_desc,
				   'team_logo' =>  $row->team_logo,
				   'member_num' =>$row->member_num,
				   'team_win' =>$row->team_win,
				   'team_lose' => $row->team_lose,
				   'team_point' => $row->team_point,
				'team_active' => $row->team_active,
				   'tour_join' => $tour_join,
				   'member_list' => $team_member_list
				);
			}
		}else {
			$member_deital = "";
		}
		
		return $member_deital;
	}
	
	function team_member_list($teamID){
		$this->db->select('a.member_id, a.team_id, a.member_name, a.member_logo, a.member_active');

		$this->db->from('tour_members a');
		$this->db->where('a.team_id', $teamID); 
		//$this->db->limit(1);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	}
	
	function check_duplicate_team($team_name) {
		$exists = $this->db->select('team_name')->where('team_name', $team_name)->get('tour_teams')->num_rows();

		return $exists;
	}
	
	function check_duplicate_join($team_id,$tour_id) {
		$exists = $this->db->select('team_id')->where('team_id', $team_id)->where('tour_id', $tour_id)->get('tour_team_regis')->num_rows();

		return $exists;
	}
	
	function curl_api($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$response = curl_exec($ch);
	
		if (curl_errno($ch)) {
			return false;
		} else {
			curl_close($ch);
		}
	
		return $response;
	}

}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */