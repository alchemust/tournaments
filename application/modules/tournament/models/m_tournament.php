<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_tournament extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database('default');
		$this->process_id = $this->session->userdata('session_id');
	}
	
	function validation($mode ="add")
	{
		$res = false;
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('tour_title', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('tour_excerpt', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('tour_rule', '', 'trim');
		$this->form_validation->set_rules('tour_award', '', 'trim');
		$this->form_validation->set_rules('tour_regis_start', '', 'trim');
		$this->form_validation->set_rules('tour_regis_end', '', 'trim');
		$this->form_validation->set_rules('tour_match_start', '', 'trim');
		$this->form_validation->set_rules('tour_match_end', '', 'trim');
		$this->form_validation->set_rules('tour_point', '', 'trim');
		$this->form_validation->set_rules('tour_hilight', '', 'trim');
		$this->form_validation->set_rules('tour_active', '', 'trim');
		
		if ($this->form_validation->run() == FALSE)
		{
			return $res;
		}
		else
		{
			if($mode == "add") {
				$res = $this->create_tournament();
			} else {
				$res = $this->edit_tournament();
			}
			return $res;
		}
	}

	
	function tournament_hilight(){
		$sql = "select tour_id, game_id, tour_title, tour_excerpt, tour_rule, tour_award, tour_regis_start, tour_regis_end, tour_match_start, tour_match_end, tour_small_img, tour_medium_img, tour_large_img, tour_team_num , tour_point, tour_hilight, tour_active from tour_tournaments where tour_hilight = 1 AND tour_active = 1 ORDER BY update_date DESC";
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}

	public function record_count($active = 1, $hilight = 0) {
		$this->db->where('tour_active', $active); 
		$this->db->where('tour_hilight', $hilight); 
        $query = $this->db->get("tour_tournaments");
		
        return $query->num_rows();
    }
	
	public function all_record_count() {

        return $this->db->count_all("tour_tournaments");
	}

	

	function tournament_list($limit = 0, $start = 10, $admin=0){
		$this->db->select('a.tour_id, a.game_id, b.game_name, b.game_logo, a.tour_title, a.tour_excerpt, a.tour_rule, a.tour_award, a.tour_regis_start, a.tour_regis_end, a.tour_match_start, a.tour_match_end, a.tour_small_img, a.tour_medium_img, a.tour_large_img, a.tour_team_num, a.tour_point, a.tour_hilight, a.tour_active, a.start_tournament');

		$this->db->from('tour_tournaments a');
		$this->db->join('tour_games b', 'a.game_id = b.game_id','left');
		if($admin == 0) { 
			//$this->db->where('a.tour_hilight', 0); 
			$this->db->where('a.tour_active', 1); 
		}
		$this->db->order_by("a.tour_id", "DESC"); 
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		//$tournament_list = "select tour_id, game_id, tour_title, tour_excerpt, tour_rule, tour_award, tour_regis_start, tour_regis_end, tour_match_start, tour_match_end, tour_small_img, tour_medium_img, tour_large_img, tour_point, tour_hilight, tour_active from tour_tournaments where tour_hilight = 0 AND tour_active = 1 limit ".$limit.", ".$start."  ORDER BY update_date DESC ";
		
		
		//$product_list = array();
		//$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	
	function tournament_sidebar_list($limit = 10){
		$this->db->select('a.tour_id, a.game_id, a.tour_title, a.tour_excerpt, a.tour_rule, a.tour_award, a.tour_regis_start, a.tour_regis_end, a.tour_match_start, a.tour_match_end, a.tour_small_img, a.tour_medium_img, a.tour_large_img, a.tour_team_num, a.tour_point, a.tour_hilight, a.tour_active');

		$this->db->from('tour_tournaments a');
		$this->db->where('a.tour_active', 1); 
		$this->db->limit($limit);
		$this->db->order_by("tour_team_num", "DESC"); 
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}

	function tournament_detail($tourID = 0)
	{
		
		$this->db->select('a.tour_id, a.game_id, a.tour_title,  c.game_name, c.game_logo, a.tour_excerpt, a.tour_rule, a.tour_award, a.tour_regis_start, a.tour_regis_end, a.tour_match_start, a.tour_match_end, a.tour_small_img, a.tour_medium_img, a.tour_large_img, a.tour_team_num, a.tour_point, a.tour_hilight, a.tour_active, a.challonge_id, a.chalonge_url, a.start_tournament, b.full_challonge_url,b.live_image_url');

		$this->db->from('tour_tournaments a');
		$this->db->join('challonge_tournaments b', 'a.tour_id = b.tour_id', 'left');
		$this->db->join('tour_games c', 'a.game_id = c.game_id', 'left');
		$this->db->where('a.tour_id', $tourID); 
		$this->db->limit(1);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	public function tour_round_date($tourID = 0) {
		$this->db->select('a.round_date_id,a.tour_id, a.round,a.round_date');

		$this->db->from('tour_rounds_date a');
		$this->db->join('tour_tournaments b', 'a.tour_id = b.tour_id');
		$this->db->where('a.tour_id', $tourID); 
		$this->db->order_by("a.round", "ASC"); 
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	}
	
	
	function create_tournament()
	{
		$res_add = 0;
		
		$time_process = date("Y-m-d H:i:s", time());
		$tour_id= intval($this->input->post('tour_id'));
		$tour_title = trim($this->input->post('tour_title'));
		$game_id= intval($this->input->post('game_id'));
		$tour_excerpt = trim($this->input->post('tour_excerpt'));
		$tour_rule = trim($this->input->post('tour_rule'));
		$tour_award = trim($this->input->post('tour_award'));
		$tour_regis_start = trim($this->input->post('tour_regis_start'));
		$tour_regis_end = trim($this->input->post('tour_regis_end'));
		$tour_match_start = trim($this->input->post('tour_match_start'));
		$tour_match_end = trim($this->input->post('tour_match_end'));
		$tour_point = intval($this->input->post('tour_point'));
		$tour_hilight = intval($this->input->post('tour_hilight'));
		$tour_active = intval($this->input->post('tour_active'));
		$old_tour_large_img = trim($this->input->post('old_tour_large_img'));
		$old_tour_medium_img = trim($this->input->post('old_tour_medium_img'));
		$old_tour_small_img = trim($this->input->post('old_tour_small_img'));
		
		$tournament = array(
		   'tour_title' => $tour_title,
		   'game_id' => $game_id,
		   'tour_excerpt' =>  $tour_excerpt,
		   'tour_rule' =>  $tour_rule,
		   'tour_award' => $tour_award,
		   'tour_regis_start' => $tour_regis_start,
		   'tour_regis_end' => $tour_regis_end,
		   'tour_match_start' => $tour_match_start,
		   'tour_match_end' => $tour_match_end,
		   'tour_point' => $tour_point,
		   'tour_hilight' => $tour_hilight,
		   'tour_active' => $tour_active,
		   'update_date' => $time_process
		);
		
		//Upload Large Img File
		$large_img_res = $this->utility->uploaded_file($_FILES["tour_large_img"], "", "add");
		$tournament["tour_large_img"] = ($large_img_res["result"] == true) ? $large_img_res["file_path"] : "";
		
		//Upload medium Img File
		$medium_img_res = $this->utility->uploaded_file($_FILES["tour_medium_img"], "", "add");
		$tournament["tour_medium_img"] = ($medium_img_res["result"] == true) ? $medium_img_res["file_path"] : "";
		
		//Upload small Img File
		$small_img_res = $this->utility->uploaded_file($_FILES["tour_small_img"], "", "add");
		$tournament["tour_small_img"] = ($small_img_res["result"] == true) ? $small_img_res["file_path"] : "";
		
		
		$this->db->insert('tour_tournaments' , $tournament);
		$tour_id = $this->db->insert_id();
		$logs = $this->process_id."|1||m_tournament.edit_tournament|Edit_Tournament|2|tour_id:".$tour_id.",detail:".implode($tournament)."\n";
			$this->utility->writeLog("back_tournament",$logs);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = $tour_id;
		return $res_add;
		
	}
	
	
	public function edit_round_date(){
		//$logs = var_dump($this->input->post('round_date'));
		//$this->utility->writeLog("back_tournament",$logs);
		$time_process = date("Y-m-d H:i:s", time());
		$round_date = $this->input->post('round_date');
		foreach ($round_date as $round_date_id=>$value) {
			//echo $round_date_id."=>".$value; 
			$edit_round_date = array(
			   'round_date' => $value,
			   'update_date' => $time_process
			);
			$this->db->update('tour_rounds_date', $edit_round_date, "round_date_id = ".$round_date_id);
		$logs = $this->process_id."|1||m_tournament.edit_round_date|Edit_Tournament_Round_Date|2|round_date_id:".	$round_date_id.",round_date:".$value."\n";
			$this->utility->writeLog("back_tournament",$logs);
		}
		
		$res_edit = true;
		
		return $res_edit;
	}
	
	function edit_tournament()
	{
		$res_edit = false;
		
		$time_process = date("Y-m-d H:i:s", time());
		$tour_id= intval($this->input->post('tour_id'));
		$tour_title = trim($this->input->post('tour_title'));
		$game_id= intval($this->input->post('game_id'));
		$tour_excerpt = trim($this->input->post('tour_excerpt'));
		$tour_rule = trim($this->input->post('tour_rule'));
		$tour_award = trim($this->input->post('tour_award'));
		$tour_regis_start = trim($this->input->post('tour_regis_start'));
		$tour_regis_end = trim($this->input->post('tour_regis_end'));
		$tour_match_start = trim($this->input->post('tour_match_start'));
		$tour_match_end = trim($this->input->post('tour_match_end'));
		$tour_point = intval($this->input->post('tour_point'));
		$tour_hilight = intval($this->input->post('tour_hilight'));
		$tour_active = intval($this->input->post('tour_active'));
		$old_tour_large_img = trim($this->input->post('old_tour_large_img'));
		$old_tour_medium_img = trim($this->input->post('old_tour_medium_img'));
		$old_tour_small_img = trim($this->input->post('old_tour_small_img'));
		
		$tournament = array(
		   'tour_title' => $tour_title,
		   'game_id' => $game_id,
		   'tour_excerpt' =>  $tour_excerpt,
		   'tour_rule' =>  $tour_rule,
		   'tour_award' => $tour_award,
		   'tour_regis_start' => $tour_regis_start,
		   'tour_regis_end' => $tour_regis_end,
		   'tour_match_start' => $tour_match_start,
		   'tour_match_end' => $tour_match_end,
		   'tour_point' => $tour_point,
		   'tour_hilight' => $tour_hilight,
		   'tour_active' => $tour_active,
		   'update_date' => $time_process
		);
		
		//Upload Large Img File
		$large_img_res = $this->utility->uploaded_file($_FILES["tour_large_img"], $old_tour_large_img, "edit");
		$tournament["tour_large_img"] = ($large_img_res["result"] == true) ? $large_img_res["file_path"] : $old_tour_large_img;
		
		//Upload medium Img File
		$medium_img_res = $this->utility->uploaded_file($_FILES["tour_medium_img"], $old_tour_medium_img, "edit");
		$tournament["tour_medium_img"] = ($medium_img_res["result"] == true) ? $medium_img_res["file_path"] : $old_tour_medium_img;
		
	    //Upload small Img File
		$small_img_res = $this->utility->uploaded_file($_FILES["tour_small_img"], $old_tour_small_img, "edit");
		$tournament["tour_small_img"] = ($small_img_res["result"] == true) ? $small_img_res["file_path"] : $old_tour_small_img;
		
		
		$this->db->update('tour_tournaments', $tournament, "tour_id = ".$tour_id);
		$logs = $this->process_id."|1||m_tournament.edit_tournament|Edit_Tournament|2|tour_id:".$tour_id.",detail:".implode($tournament)."\n";
			$this->utility->writeLog("back_tournament",$logs);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_edit = true;
		return $res_edit;
		
	}

	function start_tournament($tour_id){
		$res = false;
		$time_process = date("Y-m-d H:i:s", time());
		
		$tour_id = intval($tour_id);
		$tour_detail = $this->tournament_detail($tour_id);
		$tour_title = $tour_detail[0]->tour_title;
		$tour_excerpt = $tour_detail[0]->tour_excerpt;
		
		//Create Challonge tournament 
		$create_res = $this->create_challonge_tournament($tour_id, $tour_title, $tour_excerpt);
		
		if($create_res["result"] == true) {			
			
			if($challonge_tour_id && $chalonge_url) {
				
				// Create participants for Tournament with Challonge API
				$this->create_participants($tour_id, $create_res["challonge_id"]); 
				
				// Start Challonge Tour
				$this->start_challonge_tour($tour_id, $create_res["challonge_id"]);  
			
				// Get Challonge Matches 
				$this->get_challonge_matches($tour_id, $create_res["challonge_id"]);
				
				$res = TRUE;
			}
		} 
		
		return $res;
	}

	function create_challonge_tournament($tour_id,$tour_title,$tour_excerpt){
		$challonge_res = array();
		
		$time_process = date("Y-m-d H:i:s", time());
		$tour_id = intval($tour_id);
		$tour_title = htmlspecialchars($tour_title);
		$tour_excerpt = htmlspecialchars($tour_excerpt);
		
		$this->load->library('challonge/ChallongeAPI');
		ChallongeAPI::$api_key = challongeAPI_KEY;
		$tournament = new ChallongeTournament();
		$tournament->setParams(array(
			'tournament' => array(
				'name' => $tour_title,
				'tournament_type' => 'single elimination',
				'description' => $tour_excerpt,
				'url' => 'fps_tour_'.$tour_id.'_'.time()
			)
		));
		
		// Call Challonge API for Create Tournament  
		$xml = $tournament->reqCreate();
		
		if($xml->asXML()) {			
			//Save Challonge XML Log 
		    //$xml->save("log/challonge_xml/fps_tour_".$tour_id."_".time().".xml");
		
			$challonge_tour_id = intval($xml->{'id'});
			$chalonge_name = trim($xml->{'name'});
			$chalonge_url = trim($xml->{'url'});
			$chalonge_winner_id = intval($xml->{'winner-id'});
			$chalonge_desc = trim($xml->{'description'});
			$chalonge_tournament_type = trim($xml->{'tournament-type'});
			$chalonge_state = trim($xml->{'state'});
			$full_challonge_url= trim($xml->{'full-challonge-url'});
			$live_image_url= trim($xml->{'live-image-url'});
			
			if($challonge_tour_id && $chalonge_url) {
				$challonge_res["result"] = true; 
				$challonge_res["challonge_id"] = $challonge_tour_id;
				$chalonge_url["chalonge_url"] = $chalonge_url;
				
				$challonge_tournament = array(
					'challonge_tour_id' => $challonge_tour_id,
					'tour_id' => $tour_id,
					'name' =>  $chalonge_name,
					'url' => $chalonge_url,
					'winner_id' => $chalonge_winner_id,
					'tournament_type' => $chalonge_tournament_type,
					'description' => $chalonge_desc,
					'state' => $chalonge_state,
					'full_challonge_url' => $full_challonge_url,
					'live_image_url' => $live_image_url,
					'create_date' => $time_process
				);
				
				// Create Tournament for Challonge API
				$this->db->insert('challonge_tournaments' , $challonge_tournament);
				//$challonge_tour_id = $this->db->insert_id();
				
				// Write Log about Create Tournament for Challonge API 
				$logs = $this->process_id."|1||m_tournament.start_tournament|create_challonge_tournament|2|tour_id:".$tour_id.",challonge_tour_id:".$challonge_tour_id.",chalonge_name:".$chalonge_name.",chalonge_url:".$chalonge_url.",full_challonge_url:".$full_challonge_url.",live_image_url:".$live_image_url.",xml_res:".$xml->asXML()."\n";
				$this->utility->writeLog("challonge_api",$logs);
				
				//Save Challonge XML Log 
				$logs = $xml->asXML();
				$this->utility->writeLog($chalonge_url,$logs);
				//$this->utility->writeLog($chalonge_url,$xml->asXML() ,"log/challonge_xml/");
				
				$tournament = array(
					'challonge_id' => $challonge_tour_id,
					'chalonge_url' => $chalonge_url,
					'start_tournament' =>  1,
					'update_date' => $time_process
				);
				
				// Update challonge tournament ID in tour_tournaments Table
				$this->db->update('tour_tournaments', $tournament, "tour_id = ".$tour_id);
				
				// Write Log about Update Challonge tournament ID
				$logs = $this->process_id."|1||m_tournament.start_tournament|update_tournament_table|2|tour_id:".$tour_id.",challonge_tour_id:".$challonge_tour_id.",chalonge_url:".$chalonge_url."\n";
				$this->utility->writeLog("challonge_api",$logs);
			} else {
				$challonge_res["result"] = false; 
				$challonge_res["challonge_id"] = 0;
				$chalonge_url["chalonge_url"] = "";
			}
		} else {
			$challonge_res["result"] = false; 
			$challonge_res["challonge_id"] = 0;
			$chalonge_url["chalonge_url"] = "";
		}
		
		return $challonge_res;
	}
	
	function create_participants($tour_id, $challonge_tour_id){
		
		$time_process = date("Y-m-d H:i:s", time());
		
		// Query team Data for Tournament 
		$sql ="select a.tour_id,b.team_id,b.team_name,b.user_id,b.participant_id from tour_team_regis a, tour_teams b where a.tour_id = $tour_id AND a.team_id = b.team_id";
		$query = $this->db->query($sql);
		$team_list = $query->result();
		$count = 0;
		
		if(!empty($team_list)) {
			
			$this->load->library('challonge/challongeapi');
			ChallongeAPI::$api_key = challongeAPI_KEY;
			$tournament = new ChallongeParticipant($challonge_tour_id);
			
			foreach($team_list as $team){ 
				if(!empty($team_list->participant_id)) { 
					$participant_id = 0;
					$participant_name = "";
					
					$tournament->setParams(array(
						'participant_id' => intval($team->team_id),
						'participant' => array(
							'name' => trim($team->team_name)
						)
					));
					
					// Call Challonge API for Create Participants  
					$xml = $tournament->reqCreate();
					$participant_id =  $xml->{'id'};
					$participant_name =  trim($xml->{'name'});
					$team_id = intval($team->team_id);
					
					$challonge_participants = array(
						'participant_id' => $participant_id,
						'challonge_tour_id' => $challonge_tour_id,
						'team_id' =>  intval($team->team_id),
						'participant_name' => trim($team->team_name),
						'create_date' => $time_process
					);
					
					// Create Participants for Challonge API
					$this->db->insert('challonge_participants' , $challonge_participants);
					
					$tour_teams = array(
						'participant_id' => $participant_id,
						'update_date' => $time_process
					);
					
					// Update Participants ID in tour_team Table
					$this->db->update('tour_teams', $tour_teams, "team_id = ".$team_id);
					//$participant_id = $this->db->insert_id();
					
					// Write Log about Create Participants for Challonge API
					$logs = $this->process_id."|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:".$tour_id.",challonge_tour_id:".$challonge_tour_id.",team_id:".$team->team_id.",participant_id:".$participant_id.",participant_name:".$team->team_name.",xml_res:".$xml->asXML()."\n";
					$this->utility->writeLog("challonge_api",$logs);
				} else {
					// Call Challonge API for Create Participants  
					$xml = $tournament->reqCreate();
					$participant_id =  $xml->{'id'};
					$participant_name =  trim($xml->{'name'});
					
					$challonge_participants = array(
						'participant_id' => $participant_id,
						'challonge_tour_id' => $challonge_tour_id,
						'team_id' =>  intval($team->team_id),
						'participant_name' => trim($team->team_name),
						'create_date' => $time_process
					);
					
					// Create Participants for Challonge API
					$this->db->insert('challonge_participants' , $challonge_participants);
					
					// Write Log about Create Participants for Challonge API
					$logs = $this->process_id."|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:".$tour_id.",challonge_tour_id:".$challonge_tour_id.",team_id:".$team->team_id.",participant_id:".$participant_id.",participant_name:".$team->team_name.",xml_res:".$xml->asXML()."\n";
					$this->utility->writeLog("challonge_api",$logs); 
				}
			}
			
		}
	}
	
	function start_challonge_tour($tour_id,$challonge_tour_id) {
		$res = false;
		
		$this->load->library('challonge/challongeapi');
		$tournament = new ChallongeTournament();
		$tournament->setParams(array(
			'tournament' => $challonge_tour_id
		));
		
		// Call Challonge API for Start Challonge Tournament
		$xml = $tournament->reqStart($challonge_tour_id);
		
		$tour_state = trim($xml->{'state'});
		$challonge_tournaments = array(
			'state' => $tour_state
		);
		
		// Update challonge tournament ID in tour_tournaments Table
		$this->db->update('challonge_tournaments', $challonge_tournaments, "challonge_tour_id = ".$challonge_tour_id);
		
		// Write Log about Start Challonge Tournament API
		$logs = $this->process_id."|1||m_tournament.start_challonge_tour|Start_Challonge_Tournament|2|tour_id:".$tour_id.",challonge_tour_id:".$challonge_tour_id.",state:".$tour_state.",xml_res:".$xml->asXML()."\n";
		$this->utility->writeLog("challonge_api",$logs);
		
		$res = TRUE;
		return $res; 
	}
	
	function  get_challonge_matches($tour_id, $challonge_tour_id){
		$time_process = date("Y-m-d H:i:s", time());
		
		$this->load->library('challonge/challongeapi');
		
		$tournament = new ChallongeMatch($challonge_tour_id);
		
		// Call Challonge API for Get Challonge Matches
		$xml = $tournament->reqIndex();
		
		// Write Log about Get Challonge Matches API
		$logs = $this->process_id."|1||m_tournament.get_challonge_matches|get_challonge_matches|2|tour_id:".$tour_id.",challonge_tour_id:".$challonge_tour_id.",xml_res:".$xml->asXML()."\n";
		$this->utility->writeLog("challonge_api",$logs);	
		
		// Get Match Detail List By Challonge Matches API 
		$arr = $xml->{'match'};
		foreach($arr as $child) {
				
			$challonge_matches = array(
				'challonge_match_id' => $child->{'id'} ,
				'challonge_tour_id' => intval($challonge_tour_id),
				'tour_id' => intval($tour_id),
				'identifier' =>  trim($child->{'identifier'}) ,
				'player1_id' => intval($child->{'player1-id'}),
				'player2_id' => intval($child->{'player2-id'}),
				'winner_id' => intval($child->{'winner-id'}),
				'loser_id' => intval($child->{'loser-id'}),
				'round' => intval($child->{'round'}),
				'state' => trim($child->{'state'}),
				'started_at' => trim($child->{'started-at'}),
				'scores_csv' => trim($child->{'scores-csv'}),
				'create_date' => $time_process
			);
			
			// Create Challonge Matches for Challonge API
			$this->db->insert('challonge_matches' , $challonge_matches);
			$challonge_match_id = $this->db->insert_id();
			
			// Write Log about Start Challonge Tournament API
			$logs = $this->process_id."|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:".$tour_id.",challonge_tour_id:".$challonge_tour_id.",challonge_match_id:".$challonge_match_id.",detail:".implode($challonge_matches)."\n";
			$this->utility->writeLog("challonge_api",$logs);
			
		}
		
		$res = TRUE;
		return $res; 
	}
		
	public function update_team_num($tourID = 0){
		$time_process = date("Y-m-d H:i:s", time());
		$sql ="update tour_tournaments SET tour_team_num = tour_team_num+1, update_date = '$time_process' WHERE  tour_id = $tourID limit 1";
		$query = $this->db->query($sql);
		if($query) {
			$logs = $this->process_id."|1||m_tournament.update_team_num|Update_Team_num|2|tour_id:".$tourID."\n";
			$this->utility->writeLog("join_tournament",$logs);
			$tour_detail = $this->tournament_detail($tourID);
			
			foreach ($tour_detail as $row){
				$tour_team_num = $row->tour_team_num;
			}
			
			return $tour_team_num;
		} else {
			return false;
		}
	}

}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */