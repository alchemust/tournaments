<?php foreach($tour_detail as $tour) {?>
<div class="content">
<div class="span8">
               	<div class="tournaments_wrapper">
                    <h1 class="header">
                        
                        <?php echo $tour->tour_title;?></h1>
                     <div class="post">
                <div class="img_post">
                	<?php $tour_img = ($tour->tour_medium_img) ? IMGPATH_URL."/".$tour->tour_medium_img : other_asset_url('no_image.jpg', 'frontend', 'images');?>
                    <?php $tour_game_logo = ($tour->game_logo) ? IMGPATH_URL."/".$tour->game_logo : other_asset_url('no_image.jpg', 'frontend', 'images');?>
                    <img src="/timthumb.php?src=<?php echo $tour_img?>&zc=1&w=185&h=195&a=r" class="postthumbnail">
                    <img class="logo_post" border="0" src="<?=$tour_game_logo;?>" alt="">
                </div>
                <div class="postlayout">
                <h2><a href="/tournament/detail/<?php echo $tour->tour_id;?>"><?php echo $tour->tour_title;?></a></h2>
                <div class="clear"></div>
                 <div class="postintro"><?php echo $tour->tour_excerpt;?></div>
                 
                 <div class="postteam">
                    <div class="plan">
                      <div class="plan-name-bronze">
                        <span class="plan-feature">ผู้เข้าแข่งขัน</span>
                        <h4 id="team_num_<?php echo $tour->tour_id; ?>"><?php echo $tour->tour_team_num;?></h4>
                      </div>
                    </div>
                    <?php if($tour->start_tournament == 1) { ?>
                    	  <span href="#main_tour" class="btn btn btn-info btn-plan-select" ><i class="icon-white icon-ok"></i> เริ่มแล้ว</span>
                         <?php } else { ?>
		                    <?php if($login && ($user_type !=1)){?>
			                    	<?php if(in_array($tour->tour_id, $tour_join)) { ?>
			                            <span href="#main_tour" class="btn btn-success btn-plan-select"><i class="icon-white icon-ok"></i> ร่วมแล้ว</span>
			                            <?php } else {?>
			                            
			                            <span onclick="tournament_join('<?php echo $tour->tour_id; ?>',this);" class="btn btn-warning btn-plan-select" id="btn_join_<?php echo $tour->tour_id; ?>" data-loading-text="Loading..." data-complete-text="<i class='icon-white icon-screenshot'></i> เข้าร่วม"><i class="icon-white icon-screenshot"></i> เข้าร่วม</span>
										<?php } ?>
		                    <?php } else { ?>
		                    		<span onclick="javascript:alert('ขออภัยค่ะ คุณยังไม่ทำการล็อคอินเข้าสู่ระบบค่ะ');" class="btn btn-warning btn-plan-select" id="btn_join_<?php echo $tour->tour_id; ?>" data-loading-text="Loading..." data-complete-text="<i class='icon-white icon-screenshot'></i> เข้าร่วม"><i class="icon-white icon-screenshot"></i> เข้าร่วม</span>
                    <?php } ?>
                    <?php } ?>	
                 </div>
                 <p class="postbuttom">
                     <span class="button"><a href="/schedule/<?php echo $tour->tour_id;?>">ตารางการแข่งขัน</a></span>
                 </p>
                
             </div>
        </div>
                </div>
                
                <div class="bs-docs-example">
                  <ul id="myTab" class="nav nav-tabs">
                   <li class="active"><a href="#team" data-toggle="tab">ทีมที่เข้าแข่งขัน</a></li>
                    <li class=""><a href="#home" data-toggle="tab">กฎกติกา</a></li>
                    <li class=""><a href="#profile" data-toggle="tab">ของรางวัล</a></li>
                    
                     
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="#dropdown1" data-toggle="tab">@fat</a></li>
                        <li><a href="#dropdown2" data-toggle="tab">@mdo</a></li>
                      </ul>
                    </li>-->
                  </ul>
                  <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade " id="home" style="text-align: left;margin-left: 10px;">
                      <p><?php echo $tour->tour_rule;?></p>
                    </div>
                    <div class="tab-pane fade" id="profile" style="text-align: left;margin-left: 10px;">
                      <p><?php echo $tour->tour_award;?></p>
                    </div>
                    <div class="tab-pane fade active in" id="team">
                      <p>
                      		<?php if($team_list) { ?>
                      		<div class="row">
                      			<?php foreach($team_list as $team_detail) {?>
                      			   <?php $team_img = ($team_detail->team_logo) ? IMGPATH_URL."/".$team_detail->team_logo : other_asset_url('no_image.jpg', 'frontend', 'images');?>
							    <div class="col-lg-4">
							    	 <a href="/team_info/<?php echo $team_detail->team_id;?>" class="inline_calendar" ><img src="/timthumb.php?src=<?php echo $team_img;?>&zc=1&w=80&h=80" class="postthumbnail"></a>
							    	<h5><a href="/team_info/<?php echo $team_detail->team_id;?>" class="inline_calendar"><?php echo $team_detail->team_name; ?></a></h5>
							    </div>
							    <?php } ?>
							</div>
                      		<?php } else {?>
                      		   ยังไม่มีทีมแข่งร่วมการแข่งขัน 
                      		<?php } ?>
                     		
                      </p>
                    </div>
                    
                    
                  </div>
                </div>
      </div>    
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#fancybox").css({'width': '200px', 'height': '200px'}); 
	  $("a.inline_calendar").fancybox({
		 'width' : 630,
		 'height' : 520,
		 'autoSize': false,
		 'autoScale' : false,
		 'transitionIn' : 'none',
		 'transitionOut' : 'none',
		 'type' : 'iframe'
	 });

});


</script>
 <?php } ?>