<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tournament extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	
		$this->load->model('m_tournament');
		$this->load->model('team/m_team');

		$this->load->model('news/m_news');
		$this->load->model('matches/m_matches');
		$this->load->library('pagination');
	}
	
	
	public function index()
	{
		$data_sidebar = array(
				"login" => 0,
				"user_id" => "",
				"user_name" => "",
				"team_name" => "",
				"team_logo" => "",
				"team_active" => 0
		);
	
		
		if ($this->m_authen->is_logged_in() === TRUE) {
			
			$user_id = ($this->m_authen->getsession("user_id")) ? $this->m_authen->getsession("user_id") : 0;
			$team_active = $this->m_authen->getsession("team_active");
			$team_detail = $this->m_team->load_team_detail($user_id);
			$team_logo = ($team_detail["team_logo"]) ? IMGPATH_URL."/".$team_detail["team_logo"] : "/assets/modules/frontend/images/no_images.jpg";
			
			$data_sidebar = array(
				"login" => 1,
				"user_id" => $this->m_authen->getsession("user_id"),
				"user_name" => $this->m_authen->getsession("user_login"),
				"team_name" => $team_detail["team_name"],
				"team_logo" => $team_logo,
				"team_win" => $team_detail["team_win"],
				"team_lose" => $team_detail["team_lose"],
				"team_point" => $team_detail["team_point"],
				"team_active" => $this->m_authen->getsession("team_active")
			);
		} 
		
		$data_sidebar["news_list"] = $this->m_news->news_list();
		$data_sidebar["team_rankings"] = $this->m_team->team_rankings_list();
		$data_sidebar["hot_tour"] = $this->m_tournament->tournament_sidebar_list();
		
		$this->load->view('frontend/header_main');
		$this->load->view('tournament_view',$data_sidebar);
		$this->load->view('frontend/footer');
	}
	
	public function schedule()
	{	
		$tour_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data_main["tour_detail"] = $this->m_tournament->tournament_detail($tour_id);
		$data_main["round_date"] = $this->m_tournament->tour_round_date($tour_id);
		//$data_main["schedule_list"] = $this->m_matches->tour_match_list($tour_id);
		//$page_url = $data_main["tour_detail"][0]->full_challonge_url."/module";
		/*
		$homepage = file_get_contents($page_url);
		echo "<base href='http://challonge.com/'>";
		echo $homepage;
        */
		$this->load->view('frontend/header_main');
		$this->load->view('schedule_view', $data_main);
		$this->load->view('frontend/footer');
	}
	
	public function schedule_iframe() {
		//$this->output->cache(5);
		
		$this->load->model('m_frontend');
		
		$page_url = $_GET["url"];
		$homepage = file_get_contents($page_url);
		
		/*
		$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript 
		               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags 
		               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly 
		               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA 
		); */
		//$homepage = preg_replace($search, '', $homepage); 
		$clean_html = preg_replace("#<div class='hide' id='bottom_bar'>(.*?)</div>#is", '', $homepage);
		$clean_html = preg_replace("#<div class='live_stamp hide'>(.*?)</div>#is", '', $clean_html);
		
		$data_main["base"] = "<base href='http://challonge.com/'>";
		$data_main["html"] = $clean_html;
		
		$data_sidebar = $this->m_frontend->sidebar_info();
		//var_dump($data_sidebar);
		$this->load->view('schedule_iframe', $data_main);
		$this->load->view('schedule_script', $data_sidebar);
	}
	
	public function detail()
	{
		$this->load->model('m_frontend');
		$data_sidebar = array();
		
		$data_sidebar = $this->m_frontend->sidebar_info();
		$tour_id = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		
		$data_main["tour_detail"] = $this->m_tournament->tournament_detail($tour_id);
		$data_main["team_list"] = $this->m_team->team_number_in_tournament($tour_id);
		$data_main["user_type"] = $data_sidebar["user_type"];
		$data_main["team_id"] = $data_sidebar["team_id"];
		$data_main["tour_join"] =$data_sidebar["tour_join"];
		$data_main["login"] = $data_sidebar["login"];
		
		$data_sidebar["news_list"] = $this->m_news->news_list();
		$data_sidebar["team_rankings"] = $this->m_team->team_rankings_list();
		$data_sidebar["hot_tour"] = $this->m_tournament->tournament_sidebar_list();
		
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar',$data_sidebar);
		$this->load->view('detail_view',$data_main);
		$this->load->view('frontend/footer');
		
	}
	
	public function teamlist()
	{
		if ($this->m_authen->is_logged_in() === TRUE) {
			$have_login = 1;
			$user_id = $this->m_authen->getsession("user_id");
			$team_active = $this->m_authen->getsession("team_active");
			$team_detail = $this->m_team->load_team_detail($user_id);
			$team_logo = ($team_detail["team_logo"]) ? IMGPATH_URL."/".$team_detail["team_logo"] : "/assets/modules/frontend/images/no_images.jpg";
			
			$data_sidebar = array(
				"login" => 1,
				"user_id" => $this->m_authen->getsession("user_id"),
				"user_name" => $this->m_authen->getsession("user_login"),
				"team_name" => $team_detail["team_name"],
				"team_logo" => $team_logo,
				"team_win" => $team_detail["team_win"],
				"team_lose" => $team_detail["team_lose"],
				"team_point" => $team_detail["team_point"],
				"team_active" => $this->m_authen->getsession("team_active")
			);
		} else {
			$data_sidebar = array(
				"login" => 0,
				"user_id" => "",
				"user_name" => "",
				"team_name" => "",
				"team_logo" => "",
				"team_active" => 0
			);
		}
		$data_sidebar["news_list"] = $this->m_news->news_list();
		$data_sidebar["team_rankings"] = $this->m_team->team_rankings_list();
		$data_sidebar["hot_tour"] = $this->m_tournament->tournament_sidebar_list();
		
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar',$data_sidebar);
		$this->load->view('teamlist_view');
		$this->load->view('frontend/footer');
		
	}
	
	public function teamdetail()
	{
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar');
		$this->load->view('teamdetail_view');
		$this->load->view('frontend/footer');
		
	}
	
	
	public function add_tournament_process(){
		$res = false;
		$res = $this->m_tournament->validation('add');
		if($res == true) {
			echo "1";
		} else {
			echo "0";
		}
	}
	
	public function validation(){
		echo $this->m_login->validation(2);
	}
	
	public function start_tournament(){
		$res = false;
		$tour_id = 4;
		
		//$tour_id = intval($this->input->post('tour_id'));
		$res = $this->m_tournament->start_tournament($tour_id);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */