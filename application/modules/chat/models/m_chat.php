<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_chat extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database('default');
	}
	
	public function all_record_count() {

        return $this->db->count_all("tour_chat");
	}

	function recent_chat($limit = 10)
	{
		
		$this->db->select('a.chat_id, a.tour_id, c.tour_title, a.challonge_match_id, a.team_id, b.team_name, b.team_logo, a.chat_msg, a.chat_active,a.create_date ');

		//$this->db->where('news_active', 1); 
		$this->db->join('tour_teams b', 'a.team_id = b.team_id');
		$this->db->join('tour_tournaments c', 'a.tour_id = c.tour_id');
		$this->db->limit($limit);
		$this->db->order_by("a.update_date", "DESC"); 
		$query = $this->db->get('tour_chat a');
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	
	}
	
	function match_chat($matchID,$limit = 20)
	{
		
		$this->db->select('a.chat_id, a.tour_id, c.tour_title, a.challonge_match_id, a.team_id, b.team_name, b.team_logo, a.chat_msg, a.chat_active,a.create_date ');

		//$this->db->where('news_active', 1); 
		$this->db->join('tour_teams b', 'a.team_id = b.team_id');
		$this->db->join('tour_tournaments c', 'a.tour_id = c.tour_id');
		$this->db->where('a.challonge_match_id', $matchID); 
		$this->db->limit($limit);
		$this->db->order_by("a.update_date", "DESC"); 
		$query = $this->db->get('tour_chat a');
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	
	}
	
	
}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */