<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class app_logs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	
	}
	
	public function index()
	{
		if ($this->m_authen->is_logged_in() === TRUE && $this->m_authen->getsession("user_type") == 1) {
			$feed_dir = "";
			foreach($this->getFiles() as $file) {
			$date_th = date ("Y-m-d H:i:s", filemtime("./logs/".$file));
			$date_th = $this->datethai($date_th);
			$feed_dir .= "<li><span class='date'>".$date_th."</span><span class='file'><a href='/logs/".$file."'>$file</a></span></li>";
			}
			//return $files;
	
			$data = array(
				"list_file" => $feed_dir
			);
			
			$this->load->view('logs_view',$data);
		} else {
			redirect('/blackend/login', 'location', 301);
		}
		
	}
	
	function getFiles(){
		$files=array();
		if($dir=opendir('./logs')){
			while($file=readdir($dir)){
				if($file!='.' && $file!='..' && $file!='index.html' && $file!=basename(__FILE__)){
					$files[]=$file;
				}   
			}
			closedir($dir);
		}
		natsort($files); //sort
		return $files;
	}
	
	function datethai($strDate) {
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		
		$date_th = "$strDay $strMonthThai $strYear $strHour:$strMinute:$strSeconds";
		return $date_th;
		
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */