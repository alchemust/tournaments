<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	
		$this->load->model('m_login');
		//$this->load->model('backend/products');
	}

	public function index()
	{
		$this->load->view('login_view');
	}

	public function validation()
	{
		$this->load->view('login_view');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */