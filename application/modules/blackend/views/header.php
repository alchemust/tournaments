<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Main | <?php echo SITENAME; ?></title>
<meta name="robots" content="noindex, nofollow">
<meta name="googlebot" content="noindex, nofollow">
<link rel="shortcut icon" href="http://www.fpsthailand.com/favicon.ico" > 
<meta charset="utf-8">


<link rel="stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/skins/gray.css" title="gray">
<link rel="alternate stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/skins/orange.css" title="orange">
<link rel="alternate stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/skins/red.css" title="red">
<link rel="alternate stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/skins/purple.css" title="purple">
<link rel="alternate stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/skins/yellow.css" title="yellow">
<link rel="alternate stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/skins/black.css" title="black">
<link rel="alternate stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/skins/blue.css" title="blue">

<link rel="stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/superfish.css">
<link rel="stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/uniform.default.css">
<link rel="stylesheet" type="text/css" href="<?php base_url();?>/assets/modules/backend/css/smoothness/jquery-ui-1.8.8.custom.css">


<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/jquery-ui-1.8.8.custom.min.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/jquery.uniform.min.js"></script>

<!--
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/superfish.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/Delicious_500.font.js"></script>


<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/switcher.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="<?php base_url();?>/assets/modules/backend/js/facebox.js"></script> 
-->
<?=js_asset('tiny_mce/tiny_mce.js', 'backend'); ?>
<?=js_asset('jquery.fancybox.js','frontend'); ?>
<?=css_asset('jquery.fancybox.css','frontend')?>
<script type='text/javascript'>
    // On load, style typical form elements
    $(function () {
        $("select, input").uniform();
    });
</script>
<base href="<?php base_url();?>/assets/modules/backend/">
</head>
<body>

<header id="top">
    <div class="container_12 clearfix">
        <div id="logo" class="grid_5">
            <!-- replace with your website title or logo -->
            <a id="site-title" href="/blackend/main"><span>FPSThailand</span> BackOffice</a>
            <a id="view-site" href="http://tournaments.fpsthailand.com/" target="_blank">View Site</a>

        </div>

        <div class="grid_4" id="colorstyle">
            <div>Change Color</div>
            <a href="#" rel="blue"></a>
            <a href="#" rel="green"></a>
            <a href="#" rel="red"></a>
            <a href="#" rel="purple"></a>
            <a href="#" rel="orange"></a>

            <a href="#" rel="yellow"></a>
            <a href="#" rel="black"></a>
            <a href="#" rel="gray"></a>
        </div>

        <div id="userinfo" class="grid_3">
            Welcome, Administrator
        </div>

    </div>
</header>

<nav id="topmenu">
    <div class="container_12 clearfix">
        <div class="grid_12">
            <ul id="mainmenu" class="sf-menu">
                <li <?php echo ($page == "main") ? "class='current'" : "" ?>;><a href="/blackend/main">Dashboard</a></li>
                <li <?php echo ($page == "tournaments") ? "class='current'" : "" ?>><a href="/blackend/tournaments">Tournaments</a></li>
				 <li <?php echo ($page == "matches") ? "class='current'" : "" ?>><a href="/blackend/matches">Matches</a></li>
                <li <?php echo ($page == "teams") ? "class='current'" : "" ?>><a href="/blackend/teams">Teams</a></li>
                <li <?php echo ($page == "games") ? "class='current'" : "" ?>><a href="/blackend/games">Games</a></li>
                <li <?php echo ($page == "advertising") ? "class='current'" : "" ?>><a href="/blackend/advertising">Advertising</a></li>
                <li <?php echo ($page == "system_logs") ? "class='current'" : "" ?>><a href="/blackend/system_logs">System Logs</a></li>
                <!--<li><a href="forms.html">Forms</a></li>
                <li><a href="#">Sample Pages</a>
                    <ul>
                        <li><a href="news.html">News</a></li>
                        <li><a href="gallery.html">Photo Gallery</a></li>

                        <li><a href="settings.html">Settings</a></li>
                        <li><a href="login.html">Login</a></li>
                        <li><a href="1-column.html">1 Column Page</a></li>
                        <li><a href="3-columns.html">3 Columns Page</a></li>
                        <li><a href="sticky-footer.html">Sticky Footer</a></li>
                    </ul>

                </li>
                <li><a href="#">Layout Width</a>
                    <ul id="layoutwidth">
                        <li><a href="#" rel="fixed">Fixed</a></li>
                        <li><a href="#" rel="fluid">Fluid</a></li>
                    </ul>
                </li>
				-->
            </ul>
            <ul id="usermenu">
                
                <li><a href="/blackend/logout" target="_self">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
