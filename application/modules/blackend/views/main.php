<script type="text/javascript">
jQuery(function($) {

    /* flot
    ------------------------------------------------------------------------- */
    var d1 = [
		<?php foreach($total_user_static as $user_static) {?>
		[<?php echo $user_static->unix_time;?>, <?php echo $user_static->total_user;?>], 
		<?php } ?>
		];
   // var d2 = [[1293814800000, 32], [1293901200000, 42], [1293987600000, 59], [1294074000000, 57], [1294160400000, 47], [1294246800000, 56], [1294333200000, 59]];

    $.plot($('#pageviews'), [
        { label: 'Users',  data: d1}
        //{ label: 'Teams',  data: d2}
    ], {
        series: {
            lines: { show: true },
            points: { show: true }
        },
        xaxis: {
            mode: 'time',
            timeformat: '%b %d'
        }
    });

});
</script>
<section id="content">
    <section class="container_12 clearfix">
        <section id="main" class="grid_9 push_3">
            <article id="dashboard">
                <h1>Dashboard</h1>

                <h2>Statistics</h2>
                <div class="statistics">

                    <table>
                        <tr>
                            <td>Users</td>
                            <td><a href="#"><?php echo $total_users;?></a></td>
                        </tr>
                        <tr>
                            <td>Team</td>
                            <td><a href="#"><?php echo $total_team;?></a></td>
                        </tr>
                        <tr>
                            <td>Tournament</td>
                            <td><a href="#"><?php echo $total_tournament;?></a></td>
                        </tr>
                        <tr>
                            <td>Matches</td>
                            <td><a href="#"><?php echo $total_match;?></a></td>
                        </tr>
                        <tr>
                            <td>Torunament Register</td>
                            <td><a href="#"><?php echo $total_team;?></a></td>
                        </tr>
                        <tr>
                            <td>Chats</td>
                            <td><a href="#"><?php echo $total_chat;?></a></td>
                        </tr>

                    </table>
                </div>
                <div id="pageviews" style="width:420px;height:250px;"></div>
                <div class="clear"></div>
				
                <!--
                <h2>Quick Links</h2>
                <section class="icons">
                    <ul>

                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Home.png', 'backend', 'images')?>" />
                                <span>Home</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">

                                <img src="<?=other_asset_url('eleganticons/Paper.png', 'backend', 'images')?>" />
                                <span>Articles</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Paper-pencil.png', 'backend', 'images')?>" />
                                <span>Write Article</span>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Speech-Bubble.png', 'backend', 'images')?>" />
                                <span>Comments</span>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Photo.png', 'backend', 'images')?>" />
                                <span>Photos</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">

                                <img src="<?=other_asset_url('eleganticons/Folder.png', 'backend', 'images')?>" />
                                <span>File Manager</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Person-group.png', 'backend', 'images')?>" />
                                <span>User Manager</span>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Config.png', 'backend', 'images')?>" />
                                <span>Settings</span>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Piechart.png', 'backend', 'images')?>" />
                                <span>Statistics</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">

                                <img src="<?=other_asset_url('eleganticons/Info.png', 'backend', 'images')?>" />
                                <span>About</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/Mail.png', 'backend', 'images')?>" />
                                <span>Messages</span>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="<?=other_asset_url('eleganticons/X.png', 'backend', 'images')?>" />
                                <span>Logout</span>
                            </a>
                        </li>

                    </ul>
                </section>
				-->
                <h2>Recent Chat</h2>
                <ul class="comments">
                	<?php foreach($recent_chat as $chat) {?>
                    <li>
                        <div class="comment-body clearfix">
                            <img class="comment-avatar" src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $chat->team_logo;?>&zc=1&w=100&h=100" />
                            <a href="/team/<?php echo $chat->team_id;?>" target="_blank"><?php echo $chat->team_name;?></a> Chat On <a href="/match_detail/<?php echo $chat->challonge_match_id;?>"  target="_blank"><?php echo $chat->tour_title;?> [ Match : <?php echo $chat->challonge_match_id;?>]</a> :
                            <div><?php echo $chat->chat_msg?></div>

                        </div>
                        <div class="links">
                            <span class="date"><?php echo $chat->create_date?></span>
                            <a href="#" class="delete">Delete</a>
                        </div>
                    </li>
					<?php }?>
                </ul>
                <div class="links">
					<!--
                    <a class="button" href="#">View All</a>
                    -->
                </div>
            </article>
        </section>

        