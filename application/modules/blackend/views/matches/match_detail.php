<?php 
foreach($match_detail as $detail) {?>
<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Match Detail : <?php echo $detail->challonge_match_id;?> </h1>
                <?php if(isset($code)) { ?> 
                	<?php if($code == 201){ ?>
                    <div class="error msg">
                    ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                    <?php } ?>
				<?php } ?>
                <form id="myForm" name="myForm"  class="uniform"  method="post" action="/blackend/edit_match" enctype="multipart/form-data" >
				
					<fieldset>
						<legend>Detail : </legend>
                        
						<dl class="inline">
                        	<dt><label for="name">Tournament Name</label></dt>
							<dd>
								<input type="text" id="tour_title" name="tour_title" class="medium required" size="50" value="<?php echo $detail->tour_title;?>" disabled="disabled" />
								<small>Tournament Title</small>
							</dd>
                            <dt><label for="name">Round</label></dt>
							<dd>
								<input type="text" id="tour_title" name="tour_title" class="medium required" size="50" value="<?php echo $detail->round;?>" disabled="disabled" />
								<small>Round Title</small>
							</dd>
                            <dt><label>Winner Team</label></dt>
                                <dd>
                                    <label><input type="radio" id="winner_id" name="winner_id" value="<?php echo $detail->player1_id;?>" <?php echo ($detail->winner_id == $detail->player1_id) ? "checked=\"checked\"" : "";?>  /><?php echo $detail->player1_name;?></label>
                                    <label><input type="radio" id="winner_id" name="winner_id" value="<?php echo $detail->player2_id;?>" <?php echo ($detail->winner_id == $detail->player2_id) ? "checked=\"checked\"" : "";?> /><?php echo $detail->player2_name;?></label>
                                </dd>
							

					</fieldset>
                    
                    <div class="buttons">
								<button type="submit" class="button">Submit</button>
								<button type="button" class="button white">Cancel</button>
					</div>    
                    <input type="hidden" id="challonge_match_id" name="challonge_match_id" value="<?php echo $detail->challonge_match_id;?>"  />
                    <input type="hidden" id="tour_id" name="tour_id" value="<?php echo $detail->tour_id;?>"  />
                    <input type="hidden" id="challonge_tour_id" name="challonge_tour_id" value="<?php echo $detail->challonge_tour_id;?>"  />
                    <input type="hidden" id="player1_id" name="player1_id" value="<?php echo $detail->player1_id;?>"  />
                     <input type="hidden" id="player2_id" name="player2_id" value="<?php echo $detail->player2_id;?>"  />
                    <fieldset>
						<legend>Match Chat  : </legend>
						<?php if($recent_chat) { ?>
                <ul class="comments">
                	<?php foreach($recent_chat as $chat) {?>
                    <li>
                        <div class="comment-body clearfix">
                            <img class="comment-avatar" src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $chat->team_logo;?>&zc=1&w=100&h=100" />
                            <a href="/team_info/<?php echo $chat->team_id;?>" target="_blank"><?php echo $chat->team_name;?></a> :
                            <div><?php echo $chat->chat_msg?></div>

                        </div>
                        <div class="links">
                            <span class="date"><?php echo $chat->create_date?></span>
                            <a href="#" class="delete">Delete</a>
                        </div>
                    </li>
					<?php }?>
                </ul>
                <?php } ?>
                </fieldset>
 				</form>
                
                
                </article>
                <!--
				<h2>Message Boxes</h2>

				<div class="success msg">
				This is a success message. Click to close.
				</div>

				<div class="error msg">
				This is an error message. Click to close.
				</div>

				<div class="warning msg">
				This is a warning message. Click to close.
				</div>

				<div class="information msg">
				This is an information message. Click to close.
				</div>
			
			<article class="buttons">
				<h1>Buttons</h1>

				<h2>Standard Buttons</h2>
				<section class="buttons">
					<button class="button blue">Blue Button</button>
					<button class="button green">Green Button</button>
					<button class="button red">Red Button</button>
					<button class="button purple">Purple Button</button>
					<button class="button orange">Orange Button</button>
					<button class="button yellow">Yellow Button</button>
					<button class="button black">Black Button</button>
					<button class="button gray">Gray Button</button>
					<button class="button white">White Button</button>
				</section>

				<h2>Small Buttons</h2>
				<section class="buttons">
					<button class="button small blue">Blue Button</button>
					<button class="button small green">Green Button</button>
					<button class="button small red">Red Button</button>
					<button class="button small purple">Purple Button</button>
					<button class="button small orange">Orange Button</button>
					<button class="button small yellow">Yellow Button</button>
					<button class="button small black">Black Button</button>
					<button class="button small gray">Gray Button</button>
					<button class="button small white">White Button</button>
				</section>

				<h2>Big Buttons</h2>
				<section class="buttons">
					<button class="button big blue">Blue Button</button>
					<button class="button big green">Green Button</button>
					<button class="button big red">Red Button</button>
					<button class="button big purple">Purple Button</button>
					<button class="button big orange">Orange Button</button>
					<button class="button big yellow">Yellow Button</button>
					<button class="button big black">Black Button</button>
					<button class="button big gray">Gray Button</button>
					<button class="button big white">White Button</button>
				</section>
			</article>
            -->
		</section>
<script>
function start_tour(){
	form = window.document.getElementById("myForm");
	form.action = "/blackend/start_tournament";
	window.document.getElementById("myForm").submit();
}
</script>
<?php } ?>
	