<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Matches</h1>
				<?php if(isset($code)) { ?> 
                	<?php if($code == 201){ ?>
                    <div class="error msg">
                    ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                    <?php } ?>
				<?php } ?>
				<form >
					<table id="table1" class="gtable sortable">
						<thead>
							<tr>
								<!--<th><input type="checkbox" class="checkall" /></th>-->
								<th width="3">Match ID</th>
								<th width="150">Tournament Name</th>
                                <th >Round</th>
                                <th width="130">Team Name 1</th>
                                <th width="130">Team Name 2</th>
								<th width="80"></th>
							</tr>
						</thead>
						<tbody>
							<?php if($match_list) { ?>
                        	<?php foreach($match_list as $match) { ?>
							<tr>
								<!--<td><input type="checkbox" /></td>-->
                                
								<td><?php echo $match->challonge_match_id;?></td>
								<td><?php echo $match->tour_title;?></td>
                                <td><?php echo $match->round;?></td>
								<td ><?php echo $match->player1_name;?></td>
                                <td><?php echo $match->player2_name;?></td>
								<td>
									<img class="move" src="images/icons/arrow-move.png" alt="Move" title="Move" />
									<a href="/blackend/match_detail/<?php echo $match->challonge_match_id;?>" title="Edit"><img src="images/icons/edit.png" alt="Edit" /></a>
									<a href="#" title="Delete"><img src="images/icons/cross.png" alt="Delete" /></a>
								</td>
							</tr>
                            <?php } ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="tablefooter clearfix">
						<div class="actions">
							<select>
								<option>Action:</option>
								<option>Delete</option>
								<option>Move</option>
							</select>
							<button class="button small">Apply</button>
						</div>
						<div class="pagination">
                        	<?php echo $links;?>
							<!--<a href="#">Prev</a>
							<a href="#" class="current">1</a>
							<a href="#">2</a>
							<a href="#">3</a>
							<a href="#">4</a>
							<a href="#">5</a>
							...
							<a href="#">78</a>
							<a href="#">Next</a>-->
						</div>
					</div>
				</form>
			
				
			</article>
		</section>
		