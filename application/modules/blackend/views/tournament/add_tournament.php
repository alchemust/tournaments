<div class="grid_16" id="content">
   <!-- CONTENT TITLE -->

    <div class="grid_9">
    <h1 class="dashboard"><a href="/blackend/tournaments" style="text-decoration:underline;">Tournament</a> > Add New Tournament</h1>
    </div>
    <!-- CONTENT TITLE RIGHT BOX -->
    
    <div class="clear">
    </div>
<!--    TEXT CONTENT OR ANY OTHER CONTENT START     -->
    <div class="grid_15" id="textcontent">
    <!--<h2>This is a h2 subheading</h2>
    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla accumsan  mauris a enim aliquet at elementum diam condimentum. Donec et sem eros.  Morbi mollis accumsan pellentesque. Duis ultricies, purus in sodales  luctus, urna dolor ultrices ligula, luctus faucibus ante dolor sit amet  tortor. Fusce non purus eros, id pulvinar ligula. Quisque ullamcorper  placerat libero. Mauris pretium purus eu nibh adipiscing pretium. Nam  libero ipsum, laoreet quis convallis id, viverra id dolor. Praesent  dignissim nisl a mauris ultrices eget ultrices libero adipiscing. Fusce  eget pretium nunc. </p>
    <p>Phasellus elit ipsum, euismod sit amet dignissim sed, varius at  velit. Praesent in tortor sem. Suspendisse potenti. Sed auctor laoreet  metus, quis luctus augue ullamcorper ut. Aenean ultricies interdum  pellentesque. Integer eget quam leo, ut vulputate purus. Suspendisse  semper commodo tellus, quis commodo ligula condimentum vel. </p>-->
<form method="post" action="/backend/edit_product_process" enctype="multipart/form-data" >
    	<label>Tournament Name</label>
        <input type="text" id="txt_title" name="txt_title" class="smallInput wide" value="" />
        <br />
        <label>URL Ref</label> <span id="msg_product_name"></span>
        <input type="text" id="txt_postname" name="txt_postname" class="smallInput wide" value="" />
        <br />
        
		<label>วันที่เริ่มการแข่งขัน</label>
        <input type="text" id="txt_price" name="txt_price"  class="smallInput wide" value=""/>
        <label>วันที่สิ้นสุดการแข่งขัน</label>
        <input type="text" id="txt_price" name="txt_price"  class="smallInput wide" value=""/>
        <!--WYSIWYG Editor is linked to the textarea with id: #wysiwyg-->
        <label>รายละเอียดการแข่งขัน</label>
        <textarea id="txt_desc"  name="txt_desc" rows="20" cols="80" style="width: 100%"></textarea>
        <br />
        <label>กติกาการแข่งขัน</label>
        <textarea id="txt_desc"  name="txt_desc" rows="20" cols="80" style="width: 100%"></textarea>
        <br>
        <label>Hilight</label>
        
        <input type="checkbox" id="chk_hilight" name="chk_hilight"  value="1" />
        <br>
        <label>Games Categorys</label>
        <select class="smallInput" id="cbo_catg" name="cbo_catg"> 
        	<option value="0" >Selete Game</option>
            
                <option value="" >Point Black</option>
                <option value="" >SF</option>
         		<option value="" >AVA</option>
                <option value="" >CSI</option>
                <option value="" >BlackFire</option>
        </select>
        <input type="hidden" name="product_id" id="product_id" value=""  />
        <input type="hidden" name="url" id="url" value="/backend/edit_product/"  />
        <br/>
        <br/>
		 <label>Images <a href="/backend/add_images/" class="inline_calendar" title="เพิ่มรูปภาพ">เพิ่มรูปภาพ</a></label>
         
        <iframe id="img_list" name="img_list" src="/blackend/image_list/" width="900px" height="300px" style="border: solid 1px #666;"></iframe>


         
			<!--<label>You can use any if the buttons below to submit this form (these are A tags)</label>-->
       <!-- BUTTONS -->
       <br/>
        <br/>
        <input class="login_button" type="submit" value="Add Tournament">
        <!--<a class="button_grey"><span>Submit Form</span></a>
        <a class="button_ok"><span>Update information</span></a>
        <a class="button_notok"><span>Delete this record</span></a>
        <a class="button_grey_round"><span>This is a rounded button</span></a>-->
    </form><br />

    <div class="clear"></div><br />
    <!--NOTIFICATION MESSAGES-->
        <!--<p class="info" id="success"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
        <p class="info" id="error"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
        <p class="info" id="warning"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
        <p class="info" id="info"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>-->    
    </div>
    <div class="clear"> </div>
   <script type="text/javascript">
$(document).ready(function() {
    /*
	$("a.inline_calendar").fancybox({
          helpers: {
              title : {
                  type : 'float'
              }
          }
      });
	  */
	 
	  $("a.inline_calendar").fancybox({
		 'width' : '700',
		 'height' : '600',
		 'autoScale' : false,
		 'transitionIn' : 'none',
		 'transitionOut' : 'none',
		 'type' : 'iframe'
	 });
	 
 
});

function edit_media(id) {
	
	 $.fancybox({
		 'width' : '700',
		 'height' : '600',
		 'autoScale' : false,
		 'transitionIn' : 'none',
		 'transitionOut' : 'none',
		 'type' : 'iframe',
		 'href' : '/media/edit_images/'+id
	 });
}

function close_popup(){
	$('#img_list')[0].contentWindow.location.reload(true);
	$.fancybox.close();
	
	
}
	

	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

$(document).ready(function(){
	//alert($("#msg_catg_name").val());
	$("form").submit(function() {
	  if ($("#txt_postname").val() == "") {
			//$("span").text("Validated...").show();
			alert("กรุณาใส่ข้อมูล URL Link ด้วยค่ะ");
			$("#txt_postname").focus();
			$("#txt_postname").css("border","solid 2px red");
			//$("#msg_catg_name").text("Not valid!").show().fadeOut(1000);
			return false;
	  } else if ($("#cbo_catg").val() == "0") {
			//$("span").text("Validated...").show();
			alert("กรุณาเลือก Category ด้วยค่ะ");
			$("#cbo_catg").focus();
			$("#cbo_catg").css("border","solid 2px red");
			//$("#msg_catg_name").text("Not valid!").show().fadeOut(1000);
			return false;
	  } else if(!valid_postname($("#txt_postname").val())) {
			alert("กรุณาใส่ข้อมูล URL Link เฉพาะตัวเลขและภาษาอังกฤษด้วยค่ะ");
			$("#txt_postname").focus();
			$("#txt_postname").css("border","solid 2px red");
			return false;
	  }
	  
	  return true;
	});
	
	valid_postname = function(val){
        var filter = /^[a-zA-Z0-9_-]*$/;
        if(filter.test(val)){		
            return true;
        }else{

            return false;
        }
    };
});

</script>
<!-- END CONTENT-->    
  </div>
  <div class="clear"> </div>
  </div>