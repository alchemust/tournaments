<?php 
foreach($tour_detail as $detail) {?>
<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_12">
			<article>
				<h1>Tournament Detail </h1>
                <?php if(isset($code)) { ?> 
                	<?php if($code == 201){ ?>
                    <div class="error msg">
                    ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                    <?php } ?>
				<?php } ?>
                <form id="myForm" name="myForm"  class="uniform"  method="post" action="/blackend/edit_tournament" enctype="multipart/form-data" >
				<div style=" text-align:right;">
                <?php if($detail->start_tournament == 0) { ?>
                	<button class="button red" id="btn-start" name="btn-start" onclick="start_tour();">Start Torunament</button>
                <?php } ?>
                </div>
					<fieldset>
						<legend>Detail</legend>
						<dl class="inline">
							<dt><label for="name">Tournament Name</label></dt>
							<dd>
								<input type="text" id="tour_title" name="tour_title" class="medium required" size="50" value="<?php echo $detail->tour_title;?>" />
								<small>Tournament Title</small>
							</dd>

							<dt><label for="desc">Tournament Desc</label></dt>
							<dd><textarea id="tour_excerpt" name="tour_excerpt" class="medium"><?php echo $detail->tour_excerpt;?></textarea></dd>

							<dt><label for="Game">Game</label></dt>
							<dd>
								<select size="1" id="game_id" name="game_id" class="medium required">	
                                	<option value="0">Select Game</option>
                                    <?php foreach($game_list as $game){ ?>
									<option value="<?php echo $game->game_id;?>" <?php echo ($game->game_id == $detail->game_id) ? "selected=\"selected\"" : ""; ?>><?php echo $game->game_name;?></option>
									<?php } ?>
								</select>
							</dd>
							
                            <dt><label for="wysiwyg">Tournament Rule</label></dt>
							<dd><textarea id="tour_rule" name="tour_rule"  rows="30" ><?php echo $detail->tour_rule;?></textarea></dd>
                            
                            <dt><label for="wysiwyg">Tournament Award</label></dt>
							<dd><textarea id="tour_award" name="tour_award" rows="30" ><?php echo $detail->tour_award;?></textarea></dd>
                            
                            <dt><label for="Image Large">Image Large</label></dt>
							<dd><img src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $detail->tour_large_img;?>&zc=1&w=100&h=100" width="100" height="100" />
                            		<a href="<?php echo IMGPATH_URL?>/<?php echo $detail->tour_large_img;?>" class="fancybox">View</a>
                                   <br/>
                                <input type="file" id="tour_large_img" name="tour_large_img"  />
                                <input type="hidden" name="old_tour_large_img" id="old_tour_large_img" value="<?php echo $detail->tour_large_img;?>"  />
								<small>ขนาดรูปไม่เกิน 600x400</small>
                        	</dd>
                            
                            <dt><label for="Image Medium">Image Medium</label></dt>
							<dd><img src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $detail->tour_medium_img;?>&zc=1&w=100&h=100" width="100" height="100" />
                            		<a href="<?php echo IMGPATH_URL?>/<?php echo $detail->tour_medium_img;?>" class="fancybox">View</a>
                                   <br/>
                                <input type="file" id="tour_medium_img" name="tour_medium_img" />
                                <input type="hidden" name="old_tour_medium_img" id="old_tour_medium_img" value="<?php echo $detail->tour_medium_img;?>"  />
                                <small>ขนาดรูปไม่เกิน 185x195</small>

                        	</dd>
                            
                            <dt><label for="Image Small">Image Small</label></dt>
							<dd><img src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $detail->tour_small_img;?>&zc=1&w=100&h=100" width="100" height="100" />
                            		<a href="<?php echo IMGPATH_URL?>/<?php echo $detail->tour_small_img;?>" class="fancybox">View</a>
                                   <br/>
                                <input type="file" id="tour_small_img" name="tour_small_img"  />
                                <input type="hidden" name="old_tour_small_img" id="old_tour_small_img" value="<?php echo $detail->tour_small_img;?>"  />
                                <small>ขนาดรูปไม่เกิน 80x80</small>
                                
                        	</dd>
                            
                            <dt><label for="wysiwyg">Tournament Point</label></dt>
							<dd><input type="text" id="tour_point" name="tour_point" class="medium required" size="10" value="<?php echo $detail->tour_point;?>" /></dd>
                            
                            <dt><label>Set Hilight</label></dt>
							<dd>
								<label><input type="radio" id="tour_hilight" name="tour_hilight" value="1" <?php echo ($detail->tour_hilight == 1) ? "checked=\"checked\"" : "";?>  />Yes</label>
								<label><input type="radio" id="tour_hilight" name="tour_hilight" value="0" <?php echo ($detail->tour_hilight == 0) ? "checked=\"checked\"" : "";?> />No</label>
							</dd>
                            
                            <dt><label>Tournament Active</label></dt>
							<dd>
								<label><input type="radio" id="tour_active" name="tour_active" value="1" <?php echo ($detail->tour_active == 1) ? "checked=\"checked\"" : "";?>  />Yes</label>
								<label><input type="radio" id="tour_active" name="tour_active" value="0" <?php echo ($detail->tour_active == 0) ? "checked=\"checked\"" : "";?> />No</label>
							</dd>
						</dl>
					</fieldset>
					<?php if($detail->start_tournament == 1) { ?>
					<?php if($round_date) {?>  
                    <fieldset>
						<legend>Round Date</legend>
                        <dl class="inline">
                        	<?php foreach($round_date as $round_detail){ ?>
                        	<dt><label for="dob">Round <?php echo $round_detail->round; ?></label></dt>
							<dd>
								<input type="text" name="<?php echo $round_detail->round_date_id; ?>" id="<?php echo $round_detail->round_date_id; ?>" class="round_date" maxlength="10" class="small" value="<?php echo $round_detail->round_date;?>" />
							</dd>
							<?php } ?>
                        </dl>
                        <div class="buttons">
								<button type="button" class="button" id="btn_edit_round" >Edit Round Date</button>
						</div>  
                    </fieldset>
                    <?php } ?>
                    <?php } ?>
                    <fieldset>
						<legend>Date time</legend>
                        <dl class="inline">
                        	<dt><label for="dob">Start Register Date</label></dt>
							<dd>
								<input type="text" name="tour_regis_start" id="tour_regis_start" maxlength="10" class="small" value="<?php echo $detail->tour_regis_start;?>"  />
							</dd>
                            
                            <dt><label for="dob">End Register Date</label></dt>
							<dd>
								<input type="text" name="tour_regis_end" id="tour_regis_end" maxlength="10" class="small" value="<?php echo $detail->tour_regis_end;?>"  />
							</dd>
                            
                            <dt><label for="dob">Start Tournament Date</label></dt>
							<dd>
								<input type="text" name="tour_match_start" id="tour_match_start" maxlength="10" class="small" value="<?php echo $detail->tour_match_start;?>"  />
							</dd>
                            <dt><label for="dob">End Tournament Date</label></dt>
							<dd>
								<input type="text" name="tour_match_end" id="tour_match_end" maxlength="10" class="small" value="<?php echo $detail->tour_match_end;?>" />
							</dd>
                            
                        </dl>
                    </fieldset>
                     
                    <div class="buttons">
								<button type="submit" class="button">Submit Button</button>
								<button type="button" class="button white">Cancel Button</button>
					</div>    
                    <input type="hidden" id="tour_id" name="tour_id" value="<?php echo $detail->tour_id;?>"  />
 				</form>
                </article>
		</section>
	</section>
</section>	
<script>
function start_tour(){
	form = window.document.getElementById("myForm");
	form.action = "/blackend/start_tournament";
	window.document.getElementById("myForm").submit();
}

$(document).ready(function() {
	//$('#tour_rule').wysiwyg();
	//$('#tour_award').wysiwyg();
	$(".fancybox").fancybox();
	$("#btn_edit_round").click(function(){
		$("#btn_edit_round").prop('disabled', true);
		var round_date = {};
		$('.round_date').each(function(){
			round_date[$(this).attr('id')] = $(this).val();
		});
		
		$.ajax({
			url: '/blackend/edit_round_date',
			type: "POST",			
			data : {round_date : round_date },
			success: function(response){
				switch (response.code) {
					case "100": location.reload(); break;
						
					default: 
						alert("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว"); 
						$("#btn_edit_round").prop('disabled', false);
						break;
				}
			},
			error: function() {
				alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
				$("#btn_edit_round").prop('disabled', false);
				location.reload();
			 }
		});
		
	});
	/*
	$('a[rel*=facebox]').click(function(){
		$(this).facebox();
	});*/	
	
	
	//$.datepicker.setDefaults( $.datepicker.regional[ "th" ] );
	$('#tour_regis_start').datepicker({ dateFormat: "yy-mm-dd" });
	$('#tour_regis_end').datepicker({ dateFormat: "yy-mm-dd" });
	$('#tour_match_start').datepicker({ dateFormat: "yy-mm-dd" });
	$('#tour_match_end').datepicker({ dateFormat: "yy-mm-dd" });
	<?php if($round_date) {foreach($round_date as $round_detail){ ?>
	$('#<?php echo $round_detail->round_date_id; ?>').datetimepicker({ dateFormat: "yy-mm-dd" });
	<?php }} ?>
	
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		width: "100%",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

});
</script>
<?php } ?>
	