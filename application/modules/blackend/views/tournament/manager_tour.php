<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Tournaments</h1>
				<?php if(isset($code)) { ?> 
                	<?php if($code == 201){ ?>
                    <div class="error msg">
                    ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                    <?php } ?>
				<?php } ?>
				<form >
					<table id="table1" class="gtable sortable">
						<thead>
							<tr>
								<!--<th><input type="checkbox" class="checkall" /></th>-->
								<th width="29">ID</th>
								<th width="350">Tournament Name</th>
								<th>Team</th>
								<th>Start</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
                        	<?php foreach($tour_list as $tour) { ?>
							<tr>
								<!--<td><input type="checkbox" /></td>-->
                                
								<td><?php echo $tour->tour_id;?></td>
								<td><?php echo $tour->tour_title;?></td>
								<td><?php echo $tour->tour_team_num;?></td>
								<td><?php echo $tour->start_tournament;?></td>
								<td>
									<img class="move" src="images/icons/arrow-move.png" alt="Move" title="Move" />
									<a href="/blackend/tournament_detail/<?php echo $tour->tour_id;?>" title="Edit"><img src="images/icons/edit.png" alt="Edit" /></a>
									<a href="#" title="Delete"><img src="images/icons/cross.png" alt="Delete" /></a>
								</td>
							</tr>
                            <?php } ?>
							
						</tbody>
					</table>
					<div class="tablefooter clearfix">
						<div class="actions">
							<!-- <select>
								<option>Action:</option>
								<option>Delete</option>
								<option>Move</option>
							</select>
							<button class="button small">Apply</button> -->
						</div>
						<div class="pagination">
                        	<?php echo $links;?>
						</div>
					</div>
				</form>
			
				
			</article>
		</section>
		