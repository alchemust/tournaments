<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Main | <?php echo SITENAME; ?></title>
<?=css_asset('960.css')?>
<?=css_asset('reset.css')?>
<?=css_asset('text.css')?>
<?=css_asset('popup.css')?>
<?=css_asset('smoothness/ui.css')?>
<?=js_asset('jquery-1.8.2.min.js'); ?>
</head>

<body>


		 <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
              <tr>
                <th width="10" scope="col">Round</th>
                <th width="120" scope="col">ทีมเหย้า</th>
                <th width="120" scope="col">ทีมเยือน </th>
                <th width="130" scope="col">ผลการแข่งขัน</th>
                <th width="130" scope="col">เวลาการแข่งขัน</th>
                <th width="90" scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
            
              <tr>
                <td width="10">1</td>
                <td>Team1</td>
                <td>mith</td>
                <td><b>mith Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
              <tr>
                <td width="10">1</td>
                <td>Team2</td>
                <td>Team3</td>
                <td><b>Team3 Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
              <tr>
                <td width="10">1</td>
                <td>Team4</td>
                <td>Team5</td>
                <td><b>Cm Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
              <tr>
                <td width="10">1</td>
                <td>Team6</td>
                <td>Team7</td>
                <td><b>Cm Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
              <tr>
                <td width="10">1</td>
                <td>Team8</td>
                <td>Team9</td>
                <td><b>Team8 Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
 			  <tr>
                <td width="10">1</td>
                <td>Team10</td>
                <td>Team11	</td>
                <td><b>Team10 Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
              <tr>
                <td width="10">2</td>
                <td>Team12</td>
                <td>Team13	</td>
                <td><b>Team12 Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
              <tr>
                <td width="10">2</td>
                <td>mith</td>
                <td>Team3		</td>
                <td><b>Team3 Win</b>	</td>
                <td>2013-06-29 04:07:39</td>
                
                <td width="90"><a href="javascript:void(0);" class="approve_icon" title="Approve" onclick="approve_product('1')"></a><a href="javascript:void(0);" onclick="reject_product('1')" class="reject_icon" title="Reject"></a><a href="javascript:void(0);" onclick="edit_media('1')" class="edit_icon" title="Edit"></a>  <a href="javascript:void(0);" onclick="delete_media('1')" class="delete_icon" title="Delete"></a></td>
              </tr>
            </tbody>
          </table>
		
 <script type="text/javascript">
  
function delete_media(id){
		
		var url='/backend/delete_media/';
		
  		var dataSet={ media_id:id};   
  		//var dataSet={ id:id};   
  		if(confirm('คุณต้องการลบข้อมุลนี้หรือไม่ ?')==true) {
			$.post(url,dataSet,function(data){  
				
				if(data == '1'){
					
					alert('ข้อมูลได้ทําการลบเรียบร้อยแล้วค่ะ');
					//show_score();
					window.location.reload(true);
				}else if(data == '2'){
					alert('ขออภัยค่ะ กรุณาเริ่มใหม่อีกครั้งค่ะ');
					window.location.reload(true);
					//close_loading();
				}else{
					alert(data);
					//close_loading();
				}
				
				
				
				
			 });
		} else {
			return false;
		}
    //	return false;
 		
	}

function edit_media(id){
		
		if(id != null) {
			window.parent.edit_media(id);
		} else {
			return false;
		}
    //	return false;
 		
	}
  </script>
</body>
</html>