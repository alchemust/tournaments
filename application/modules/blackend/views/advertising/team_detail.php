<?php 
foreach($tour_detail as $detail) {?>
<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Team Detail</h1>
                <form id="myForm"  class="uniform"  method="post" action="/blackend/edit_tournament" enctype="multipart/form-data" >
					<fieldset>
						<legend>Detail</legend>
						<dl class="inline">
							<dt><label for="name">Tournament Name</label></dt>
							<dd>
								<input type="text" id="tour_title" name="tour_title" class="medium required" size="50" value="<?php echo $detail->tour_title;?>" />
								<small>Tournament Title</small>
							</dd>

							<dt><label for="desc">Tournament Desc</label></dt>
							<dd><textarea id="tour_excerpt" name="tour_excerpt" class="medium"><?php echo $detail->tour_excerpt;?></textarea></dd>

							<dt><label for="Game">Game</label></dt>
							<dd>
								<select size="1" id="game_id" name="game_id" class="medium required">	
                                	<option value="0">Select Game</option>
                                    <?php foreach($game_list as $game){ ?>
									<option value="<?php echo $game->game_id;?>" <?php echo ($game->game_id == $detail->game_id) ? "selected=\"selected\"" : ""; ?>><?php echo $game->game_name;?></option>
									<?php } ?>
								</select>
							</dd>
							
                            <dt><label for="wysiwyg">Tournament Rule</label></dt>
							<dd><textarea id="tour_rule" name="tour_rule" class="medium" rows="6"><?php echo $detail->tour_rule;?></textarea></dd>
                            
                            <dt><label for="wysiwyg">Tournament Award</label></dt>
							<dd><textarea id="tour_award" name="tour_award" class="medium" rows="6"><?php echo $detail->tour_award;?></textarea></dd>
                            
                            <dt><label for="Image Large">Image Large</label></dt>
							<dd><img src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $detail->tour_large_img;?>&zc=1&w=100&h=100" width="100" height="100" />
                            		<a href="<?php echo IMGPATH_URL?>/<?php echo $detail->tour_large_img;?>" rel="facebox">View</a>
                                   <br/>
                                <input type="file" id="tour_large_img" name="tour_large_img"  />
                                <input type="hidden" name="old_tour_large_img" id="old_tour_large_img" value="<?php echo $detail->tour_large_img;?>"  />
								<small>ขนาดรูปไม่เกิน 600x400</small>
                        	</dd>
                            
                            <dt><label for="Image Medium">Image Medium</label></dt>
							<dd><img src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $detail->tour_medium_img;?>&zc=1&w=100&h=100" width="100" height="100" />
                            		<a href="<?php echo IMGPATH_URL?>/<?php echo $detail->tour_medium_img;?>" rel="facebox">View</a>
                                   <br/>
                                <input type="file" id="tour_medium_img" name="tour_medium_img" />
                                <input type="hidden" name="old_tour_medium_img" id="old_tour_medium_img" value="<?php echo $detail->tour_medium_img;?>"  />
                                <small>ขนาดรูปไม่เกิน 185x195</small>

                        	</dd>
                            
                            <dt><label for="Image Small">Image Small</label></dt>
							<dd><img src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $detail->tour_small_img;?>&zc=1&w=100&h=100" width="100" height="100" />
                            		<a href="<?php echo IMGPATH_URL?>/<?php echo $detail->tour_small_img;?>" rel="facebox">View</a>
                                   <br/>
                                <input type="file" id="tour_small_img" name="tour_small_img"  />
                                <input type="hidden" name="old_tour_small_img" id="old_tour_small_img" value="<?php echo $detail->tour_small_img;?>"  />
                                <small>ขนาดรูปไม่เกิน 80x80</small>
                                
                        	</dd>
                            
                            <dt><label for="wysiwyg">Tournament Point</label></dt>
							<dd><input type="text" id="tour_point" name="tour_point" class="medium required" size="10" value="<?php echo $detail->tour_point;?>" /></dd>
                            
                            <dt><label>Set Hilight</label></dt>
							<dd>
								<label><input type="radio" id="tour_hilight" name="tour_hilight" value="1" <?php echo ($detail->tour_hilight == 1) ? "checked=\"checked\"" : "";?>  />Yes</label>
								<label><input type="radio" id="tour_hilight" name="tour_hilight" value="0" <?php echo ($detail->tour_hilight == 0) ? "checked=\"checked\"" : "";?> />No</label>
							</dd>
                            
                            <dt><label>Tournament Active</label></dt>
							<dd>
								<label><input type="radio" id="tour_active" name="tour_active" value="1" <?php echo ($detail->tour_active == 1) ? "checked=\"checked\"" : "";?>  />Yes</label>
								<label><input type="radio" id="tour_active" name="tour_active" value="0" <?php echo ($detail->tour_active == 0) ? "checked=\"checked\"" : "";?> />No</label>
							</dd>
						</dl>
					</fieldset>
                    <fieldset>
						<legend>Date time</legend>
                        <dl class="inline">
                        	<dt><label for="dob">Start Register Date</label></dt>
							<dd>
								<input type="text" name="tour_regis_start" id="tour_regis_start" maxlength="10" class="small" value="<?php echo $detail->tour_regis_start;?>" />
							</dd>
                            
                            <dt><label for="dob">End Register Date</label></dt>
							<dd>
								<input type="text" name="tour_regis_end" id="tour_regis_end" maxlength="10" class="small" value="<?php echo $detail->tour_regis_end;?>" />
							</dd>
                            
                            <dt><label for="dob">Start Tournament Date</label></dt>
							<dd>
								<input type="text" name="tour_match_start" id="tour_match_start" maxlength="10" class="small" value="<?php echo $detail->tour_match_start;?>" />
							</dd>
                            <dt><label for="dob">End Tournament Date</label></dt>
							<dd>
								<input type="text" name="tour_match_end" id="tour_match_end" maxlength="10" class="small" value="<?php echo $detail->tour_match_end;?>" />
							</dd>
                            
                        </dl>
                    </fieldset>
                    <div class="buttons">
								<button type="submit" class="button">Submit Button</button>
								<button type="button" class="button white">Cancel Button</button>
					</div>    
                    <input type="hidden" id="tour_id" name="tour_id" value="<?php echo $detail->tour_id;?>"  />
 				</form>
                </article>
                <!--
				<h2>Message Boxes</h2>

				<div class="success msg">
				This is a success message. Click to close.
				</div>

				<div class="error msg">
				This is an error message. Click to close.
				</div>

				<div class="warning msg">
				This is a warning message. Click to close.
				</div>

				<div class="information msg">
				This is an information message. Click to close.
				</div>
			
			<article class="buttons">
				<h1>Buttons</h1>

				<h2>Standard Buttons</h2>
				<section class="buttons">
					<button class="button blue">Blue Button</button>
					<button class="button green">Green Button</button>
					<button class="button red">Red Button</button>
					<button class="button purple">Purple Button</button>
					<button class="button orange">Orange Button</button>
					<button class="button yellow">Yellow Button</button>
					<button class="button black">Black Button</button>
					<button class="button gray">Gray Button</button>
					<button class="button white">White Button</button>
				</section>

				<h2>Small Buttons</h2>
				<section class="buttons">
					<button class="button small blue">Blue Button</button>
					<button class="button small green">Green Button</button>
					<button class="button small red">Red Button</button>
					<button class="button small purple">Purple Button</button>
					<button class="button small orange">Orange Button</button>
					<button class="button small yellow">Yellow Button</button>
					<button class="button small black">Black Button</button>
					<button class="button small gray">Gray Button</button>
					<button class="button small white">White Button</button>
				</section>

				<h2>Big Buttons</h2>
				<section class="buttons">
					<button class="button big blue">Blue Button</button>
					<button class="button big green">Green Button</button>
					<button class="button big red">Red Button</button>
					<button class="button big purple">Purple Button</button>
					<button class="button big orange">Orange Button</button>
					<button class="button big yellow">Yellow Button</button>
					<button class="button big black">Black Button</button>
					<button class="button big gray">Gray Button</button>
					<button class="button big white">White Button</button>
				</section>
			</article>
            -->
		</section>
<?php } ?>
	