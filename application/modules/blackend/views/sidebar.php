<aside id="sidebar" class="grid_3 pull_9">
            <div class="box search">
                <form>
                    <label for="s">Search:</label>

                    <input id="s" type="text" size="20" />
                    <button class="button small">Go</button>
                </form>
            </div>
            <?php if($page == "tournaments") {?>
            <div class="box menu">
                <h2>Tournament Menu</h2>
                <section>
                    <ul>
                    	<li><a href="/blackend/tournaments">Manage Tournament</a></li>
                        <li><a href="/blackend/create_tournament">Create Tournament</a></li>
                        
                    </ul>
                </section>
            </div>
            <?php } ?>
            <?php if($page == "games") {?>
            <div class="box menu">
                <h2>Game Menu</h2>
                <section>
                    <ul>
                    	<li><a href="/blackend/games">Manage Games</a></li>
                        <li><a href="/blackend/create_game">Create Games</a></li>
                        
                    </ul>
                </section>
            </div>
            <?php } ?>
            <!--
            <div class="box info">

                <h2>Info</h2>
                <section>
                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                </section>
            </div>
            <div class="box">
                <h2>Lorem Ipsum</h2>
                <section>

                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                </section>
            </div>-->
        </aside>
    </section>
</section>