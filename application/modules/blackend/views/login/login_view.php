<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Tournaments Backend</title>
   <meta name="robots" content="nofollow,noindex" />
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <?=css_asset('login_style.css', 'backend')?>
  <?=js_asset('jquery-1.8.2.min.js', 'backend'); ?>
  <?=js_asset('jquery.form.js', 'backend'); ?>
  <?=js_asset('jquery.validate.min.js', 'backend'); ?>
  
 
     <script> 
        $(document).ready(function() {
			var bg_h = $(document).height(); 
            
			$("#login_form").validate({
				   messages: {
					login_user: "Username !",
					login_pass: "Password !"
				   },
				   onkeyup: false,
				   submitHandler: function() {
						$('#overlay').attr('style', 'display: block;');
						$('#wrap-pop').attr('style', 'display: block;');
						$('#login').attr('style', 'display: block;');
						$('#overlay').css('height', bg_h+'px');

					   var login_user = $("#login_user").val();
					   var login_pass = $("#login_pass").val();
					   
					   $.ajax({
							url: '/blackend/validation',
							type: "POST",
							data: { login_user: login_user,login_pass:login_pass },
							success: function(data){
								if (data==1){
									window.location = "/blackend/main";
								}
								else
								{
									 alert("ขออภัยค่ะ คุณกรอก User และ Password ไม่ถูกต้อง"); 
									 $('#overlay').attr('style', 'display: none;');
									 $('#wrap-pop').attr('style', 'display: none;');
									 $('#login').attr('style', 'display: none;');
									 $('#overlay').css('height', 'auto');
								}
							}
						});
				   }
			});
			
			/*
			$("#btn_submit").click(function () {
				//alert("test");
				$('#overlay').attr('style', 'display: block;');
				$('#wrap-pop').attr('style', 'display: block;');
				$('#login').attr('style', 'display: block;');
				$('#overlay').css('height', bg_h+'px');
			});
			*/
			
        }); 
    </script> 
 </head>

 <body>
		<div id="wrapper">
            <div id="overlay" style="display:none ;"></div>
            <div id="wrap-pop" style="display:none;">
                <div id="login" style="display:none ;">
                	<div class="loading-box">
                	<p>Please wait ...</p>
                    <p><img src="<?=other_asset_url('loader-bar.gif', 'backend', 'images')?>"></p>
                    </div>
                </div>
                
            </div>
		<form id="login_form" name="login_form" method="post" action="" class="login-form" >
		
			<div class="header">
			<h1>Torunaments Login</h1>
			<span>Please Login with FPSThailand Member in This System.</span>
			</div>
		
			<div class="content">
			<input id="login_user" name="login_user" type="text" class="input username" placeholder="Username" />
			<div class="user-icon"></div>
			<input id="login_pass" name="login_pass" type="password" class="input password" placeholder="Password" />
			<div class="pass-icon"></div>		
			</div>

			<div class="footer">
			<input type="submit" name="btn_submit" value="Login" class="button" />
			
			</div>
		
		</form>

	</div>
	<div class="gradient"></div>

 </body>
</html>
