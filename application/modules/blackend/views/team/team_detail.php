<?php 
//var_dump($team_detail);
?>
<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Team Detail</h1>
                <form id="myForm"  class="uniform"  method="post" action="/blackend/edit_tournament" enctype="multipart/form-data" >
					<fieldset>
						<legend>Detail</legend>
						<dl class="inline">
							<dt><label for="name">Team Name</label></dt>
							<dd>
								<input type="text" id="team_name" name="team_name" class="medium required" size="50" value="<?php echo $team_detail["team_name"];?>" />
								<small>Tournament Title</small>
							</dd>

							<dt><label for="desc">Team Desc</label></dt>
							<dd><textarea id="team_desc" name="team_desc" class="medium"><?php echo $team_detail["team_desc"];?></textarea></dd>
							
                            
                            
                            <?php  $i = 0; while ($i < 6) { ?>
                            <dt><label for="Image Large">Team Member <?php echo $num = $i+1;?></label></dt>
                            <?php //foreach ($team_detail["member_list"] as $members => $member_list) { ?>
                           <?php $member_logo = ($team_detail["member_list"][$i]->member_logo) ? IMGPATH_URL."/".$team_detail["member_list"][$i]->member_logo : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
							<dd>
							<input type="text"  id="member_name[]" name="member_name[]" class="small required" size="50" value="<?php echo $team_detail["member_list"][$i]->member_name; ?>">
							<br/>
							
							<img src="/timthumb.php?src=<?php echo $member_logo;?>&zc=1&w=40&h=40" />
                            		<a href="<?php echo $member_logo;?>" class="fancybox">View</a>
                                   
                                <input type="file" id="img_member[]" name="img_member[]"  />
                                <input type="hidden" name="img_member_current[]" id="img_member_current[]" value="<?php echo ($team_detail["member_list"][$i]->member_logo) ? $team_detail["member_list"][$i]->member_logo : "" ?>"  />
								<small>ขนาดรูปไม่เกิน 80x80</small>
								
                        	</dd>
                        	<?php //} ?>
                            <?php $i++;} ?>
                            
                            <dt><label for="wysiwyg">Team Score </label></dt>
							<dd><input type="text" id="team_point" name="team_point" class="medium required" size="10" value="<?php echo $team_detail["team_point"]; ?>" /></dd>
                            <dt><label for="wysiwyg">Team Win </label></dt>
							<dd><input type="text" id="team_win" name="team_win" class="medium required" size="10" value="<?php echo $team_detail["team_win"]; ?>" /></dd>
                            <dt><label for="wysiwyg">Team Lose </label></dt>
							<dd><input type="text" id="team_lose" name="team_lose" class="medium required" size="10" value="<?php echo $team_detail["team_lose"]; ?>" /></dd>
                            <dt><label>Team Active</label></dt>
							<dd>
								<label><input type="radio" id="team_active" name="team_active" value="1" <?php echo ($team_detail["team_active"] == 1) ? "checked=\"checked\"" : "";?>  />Yes</label>
								<label><input type="radio" id="team_active" name="team_active" value="0" <?php echo ($team_detail["team_active"] == 0) ? "checked=\"checked\"" : "";?> />No</label>
							</dd>
						</dl>
					</fieldset>
                    
                    <div class="buttons">
								<button type="submit" class="button">Submit Button</button>
								<button type="button" class="button white">Cancel Button</button>
					</div>    
                    <input type="hidden" id="team_id" name=""team_id"" value="<?php echo $team_detail["team_id"];?>"  />
 				</form>
                </article>
                <!--
				<h2>Message Boxes</h2>

				<div class="success msg">
				This is a success message. Click to close.
				</div>

				<div class="error msg">
				This is an error message. Click to close.
				</div>

				<div class="warning msg">
				This is a warning message. Click to close.
				</div>

				<div class="information msg">
				This is an information message. Click to close.
				</div>
			
			<article class="buttons">
				<h1>Buttons</h1>

				<h2>Standard Buttons</h2>
				<section class="buttons">
					<button class="button blue">Blue Button</button>
					<button class="button green">Green Button</button>
					<button class="button red">Red Button</button>
					<button class="button purple">Purple Button</button>
					<button class="button orange">Orange Button</button>
					<button class="button yellow">Yellow Button</button>
					<button class="button black">Black Button</button>
					<button class="button gray">Gray Button</button>
					<button class="button white">White Button</button>
				</section>

				<h2>Small Buttons</h2>
				<section class="buttons">
					<button class="button small blue">Blue Button</button>
					<button class="button small green">Green Button</button>
					<button class="button small red">Red Button</button>
					<button class="button small purple">Purple Button</button>
					<button class="button small orange">Orange Button</button>
					<button class="button small yellow">Yellow Button</button>
					<button class="button small black">Black Button</button>
					<button class="button small gray">Gray Button</button>
					<button class="button small white">White Button</button>
				</section>

				<h2>Big Buttons</h2>
				<section class="buttons">
					<button class="button big blue">Blue Button</button>
					<button class="button big green">Green Button</button>
					<button class="button big red">Red Button</button>
					<button class="button big purple">Purple Button</button>
					<button class="button big orange">Orange Button</button>
					<button class="button big yellow">Yellow Button</button>
					<button class="button big black">Black Button</button>
					<button class="button big gray">Gray Button</button>
					<button class="button big white">White Button</button>
				</section>
			</article>
            -->
		</section>
<script>
$(document).ready(function() {
	//$('#tour_rule').wysiwyg();
	//$('#tour_award').wysiwyg();
	$(".fancybox").fancybox();
	$("#btn_edit_round").click(function(){
		$("#btn_edit_round").prop('disabled', true);
		var round_date = {};
		$('.round_date').each(function(){
			round_date[$(this).attr('id')] = $(this).val();
		});
		
		$.ajax({
			url: '/blackend/edit_round_date',
			type: "POST",			
			data : {round_date : round_date },
			success: function(response){
				switch (response.code) {
					case "100": location.reload(); break;
						
					default: 
						alert("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว"); 
						$("#btn_edit_round").prop('disabled', false);
						break;
				}
			},
			error: function() {
				alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
				$("#btn_edit_round").prop('disabled', false);
				location.reload();
			 }
		});
		
	});
	
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		width: "100%",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

});
</script>
	