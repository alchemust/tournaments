<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Games</h1>
				<?php if(isset($code)) { ?> 
                	<?php if($code == 201){ ?>
                    <div class="error msg">
                    ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                    <?php } ?>
				<?php } ?>
				<form >
					<table id="table1" class="gtable sortable">
						<thead>
							<tr>
								<!--<th><input type="checkbox" class="checkall" /></th>-->
								<th width="29">ID</th>
								<th width="150">Games Logo</th>
								<th width="300">Games Name</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
                        	<?php foreach($game_list as $game) { ?>
							<tr>
								<!--<td><input type="checkbox" /></td>-->
                                
								<td><?php echo $game->game_id;?></td>
								<td><img src="/timthumb.php?src=<?php echo IMGPATH_URL?>/<?php echo $game->game_logo;?>&zc=1&w=40&h=40" width="40" height="40" />
								</td>
								<td><?php echo $game->game_name;?></td>
								<td>
									<img class="move" src="images/icons/arrow-move.png" alt="Move" title="Move" />
									<a href="/blackend/game_detail/<?php echo $game->game_id;?>" title="Edit"><img src="images/icons/edit.png" alt="Edit" /></a>
									<a href="#" title="Delete"><img src="images/icons/cross.png" alt="Delete" /></a>
								</td>
							</tr>
                            <?php } ?>
							
						</tbody>
					</table>
					<div class="tablefooter clearfix">
						<div class="actions">
							<!-- <select>
								<option>Action:</option>
								<option>Delete</option>
								<option>Move</option>
							</select>
							<button class="button small">Apply</button> -->
						</div>
						<div class="pagination">
                        	<?php echo $links;?>
						</div>
					</div>
				</form>
			
				
			</article>
		</section>
		