<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Tournament Detail </h1>
                <?php if(isset($code)) { ?> 
                	<?php if($code == 201){ ?>
                    <div class="error msg">
                    ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                    <?php } ?>
				<?php } ?>
                <form id="myForm" name="myForm"  class="uniform"  method="post" action="/blackend/add_tournament" enctype="multipart/form-data" >
				<div style=" text-align:right;">
                
                </div>
					<fieldset>
						<legend>Detail</legend>
						<dl class="inline">
							<dt><label for="name">Tournament Name</label></dt>
							<dd>
								<input type="text" id="tour_title" name="tour_title" class="medium required" value="" />
								<small>Tournament Title</small>
							</dd>

							<dt><label for="desc">Tournament Desc</label></dt>
							<dd><textarea id="tour_excerpt" name="tour_excerpt" class="medium"></textarea></dd>

							<dt><label for="Game">Game</label></dt>
							<dd>
								<?php //var_dump($game_list);?>
								<select size="1" id="game_id" name="game_id" class="medium required">	
                                	<option value="0" selected="selected">Select Game</option>
                                    <?php foreach($game_list as $game){ ?>
									<option value="<?php echo $game->game_id;?>"><?php echo $game->game_name;?></option>
									<?php } ?>
								</select>
							</dd>
							
                            <dt><label for="wysiwyg">Tournament Rule</label></dt>
							<dd><textarea id="tour_rule" name="tour_rule"  rows="30" cols="75"></textarea></dd>
                            
                            <dt><label for="wysiwyg">Tournament Award</label></dt>
							<dd><textarea id="tour_award" name="tour_award" rows="30" cols="75" ></textarea></dd>
                            
                            <dt><label for="Image Large">Image Large</label></dt>
							<dd><img src="/timthumb.php?src=<?=other_asset_url('no_image.jpg', 'frontend', 'images');?>&zc=1&w=100&h=100" width="100" height="100" />
                            		
                                   <br/>
                                <input type="file" id="tour_large_img" name="tour_large_img"  />
                              
								<small>ขนาดรูปไม่เกิน 600x400</small>
                        	</dd>
                            
                            <dt><label for="Image Medium">Image Medium</label></dt>
							<dd><img src="/timthumb.php?src=<?=other_asset_url('no_image.jpg', 'frontend', 'images');?>&zc=1&w=100&h=100" width="100" height="100" />
                            		
                                   <br/>
                                <input type="file" id="tour_medium_img" name="tour_medium_img" />
                                <small>ขนาดรูปไม่เกิน 185x195</small>
                        	</dd>
                            
                            <dt><label for="Image Small">Image Small</label></dt>
							<dd><img src="/timthumb.php?src=<?=other_asset_url('no_image.jpg', 'frontend', 'images');?>&zc=1&w=100&h=100" width="100" height="100" />
                            		
                                   <br/>
                                <input type="file" id="tour_small_img" name="tour_small_img"  />
                                <small>ขนาดรูปไม่เกิน 80x80</small>
                        	</dd>
                            
                            <dt><label for="wysiwyg">Tournament Point</label></dt>
							<dd><input type="text" id="tour_point" name="tour_point" class="medium required" size="10" value="" /></dd>
                            
                            <dt><label>Set Hilight</label></dt>
							<dd>
								<label><input type="radio" id="tour_hilight" name="tour_hilight" value="1"  />Yes</label>
								<label><input type="radio" id="tour_hilight" name="tour_hilight" value="0" checked="checked"  />No</label>
							</dd>
                            
                            <dt><label>Tournament Active</label></dt>
							<dd>
								<label><input type="radio" id="tour_active" name="tour_active" value="1" />Yes</label>
								<label><input type="radio" id="tour_active" name="tour_active" value="0" checked="checked"  />No</label>
							</dd>
						</dl>
					</fieldset>
					
                    <fieldset>
						<legend>Date time</legend>
                        <dl class="inline">
                        	<dt><label for="dob">Start Register Date</label></dt>
							<dd>
								<input type="text" name="tour_regis_start" id="tour_regis_start" maxlength="10" class="small" value=""  />
							</dd>
                            
                            <dt><label for="dob">End Register Date</label></dt>
							<dd>
								<input type="text" name="tour_regis_end" id="tour_regis_end" maxlength="10" class="small" value="" />
							</dd>
                            
                            <dt><label for="dob">Start Tournament Date</label></dt>
							<dd>
								<input type="text" name="tour_match_start" id="tour_match_start" maxlength="10" class="small" value="" />
							</dd>
                            <dt><label for="dob">End Tournament Date</label></dt>
							<dd>
								<input type="text" name="tour_match_end" id="tour_match_end" maxlength="10" class="small" value="" />
							</dd>
                            
                        </dl>
                    </fieldset>
                     
                    <div class="buttons">
								<button type="submit" class="button">Submit Button</button>
								<button type="button" class="button white">Cancel Button</button>
					</div>    
 				</form>
                </article>
		</section>
<script>
function start_tour(){
	form = window.document.getElementById("myForm");
	form.action = "/blackend/start_tournament";
	window.document.getElementById("myForm").submit();
}

$(document).ready(function() {

	$("#btn_edit_round").click(function(){
		$("#btn_edit_round").prop('disabled', true);
		var round_date = {};
		$('.round_date').each(function(){
			round_date[$(this).attr('id')] = $(this).val();
		});
		
		$.ajax({
			url: '/blackend/edit_round_date',
			type: "POST",			
			data : {round_date : round_date },
			success: function(response){
				switch (response.code) {
					case "100": location.reload(); break;
						
					default: 
						alert("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว"); 
						$("#btn_edit_round").prop('disabled', false);
						break;
				}
			},
			error: function() {
				alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
				$("#btn_edit_round").prop('disabled', false);
				location.reload();
			 }
		});
		
	});
	/*
	$('a[rel*=facebox]').click(function(){
		$(this).facebox();
	});*/	
	
	//$.datepicker.setDefaults( $.datepicker.regional[ "th" ] );
	$('#tour_regis_start').datepicker({ dateFormat: "yy-mm-dd" });
	$('#tour_regis_end').datepicker({ dateFormat: "yy-mm-dd" });
	$('#tour_match_start').datepicker({ dateFormat: "yy-mm-dd" });
	$('#tour_match_end').datepicker({ dateFormat: "yy-mm-dd" });
	
	
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		width: "100%",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

});
</script>

	