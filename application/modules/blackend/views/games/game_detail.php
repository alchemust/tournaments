<?php 
foreach($game_detail as $detail) {?>
<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_9 push_3">
			<article>
				<h1>Games Detail </h1>
                <?php if(isset($code)) { ?> 
                	<?php if($code == 201){ ?>
                    <div class="error msg">
                    ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                    <?php } ?>
				<?php } ?>
                <form id="myForm" name="myForm"  class="uniform"  method="post" action="/blackend/edit_game" enctype="multipart/form-data" >
				<div style=" text-align:right;">
                
                </div>
					<fieldset>
						<legend>Detail</legend>
						<dl class="inline">
							<dt><label for="name">Game Name</label></dt>
							<dd>
								<input type="text" id="game_name" name="game_name" class="medium required" size="50" value="<?php echo $detail->game_name;?>" />
								<small>Game Name</small>
							</dd>

							<dt><label for="desc">Game Desc</label></dt>
							<dd><textarea id="game_desc" name="game_desc" class="medium"><?php echo $detail->game_desc;?></textarea></dd>
							<?php $game_logo = ($detail->game_logo) ? IMGPATH_URL."/".$detail->game_logo : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                            <dt><label for="Image Large">Game Logo</label></dt>
							<dd><img src="/timthumb.php?src=<?php echo $game_logo;?>&zc=1&w38&h=35" width="38" height="35" />
                            		<a href="<?php echo IMGPATH_URL?>/<?php echo $detail->game_logo;?>" class="fancybox">View</a>
                                   <br/>
                                <input type="file" id="game_logo" name="game_logo"  />
                                <input type="hidden" name="current_game_logo" id="current_game_logo" value="<?php echo $detail->game_logo;?>"  />
								<small>ขนาดรูปไม่เกิน 38x35</small>
                        	</dd>
                            
                            
					</fieldset>
					
                     
                    <div class="buttons">
								<button type="submit" class="button">Submit Button</button>
								<button type="button" class="button white">Cancel Button</button>
					</div>    
                    <input type="hidden" id="game_id" name="game_id" value="<?php echo $detail->game_id;?>"  />
 				</form>
                </article>
		</section>
<script>
function start_tour(){
	form = window.document.getElementById("myForm");
	form.action = "/blackend/start_tournament";
	window.document.getElementById("myForm").submit();
}

$(document).ready(function() {
	//$('#tour_rule').wysiwyg();
	//$('#tour_award').wysiwyg();
	$(".fancybox").fancybox();
	$("#btn_edit_round").click(function(){
		
		
		$.ajax({
			url: '/blackend/edit_game',
			type: "POST",			
			data : {round_date : round_date },
			success: function(response){
				switch (response.code) {
					case "100": location.reload(); break;
						
					default: 
						alert("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว"); 
						$("#btn_edit_round").prop('disabled', false);
						break;
				}
			},
			error: function() {
				alert("ระบบเกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"); 
				$("#btn_edit_round").prop('disabled', false);
				location.reload();
			 }
		});
		
	});
	/*
	$('a[rel*=facebox]').click(function(){
		$(this).facebox();
	});*/	
	
	

});
</script>
<?php } ?>
	