<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Users extends CI_Model {

	function __construct()
	{
		parent::__construct();

		//LOAD DB DEFAULT
		$this->load->library('Utility');
		$this->load->database('default');
	}

	function index()
	{
		
	}
	
	
	function reject_user()
	{
		$res_add = false;
	
		$user_id = trim($this->input->post('user_id'));

		$time_process = date("Y-m-d H:i:s", time());
		
		$users = array(
			   'active' => 0,
			   'update_date' => $time_process
		);
		
		$this->db->update('tbl_users', $users, "user_id = ".$user_id);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}
	
	function users_list($option = 0,$limit = 10)
	{
		if($option == 0) { 
			$sql ="select a.* from tbl_users a limit 0,$limit"; 
		} else {
			$sql ="select a.* from tbl_options a
WHERE a.group_id = $groupId AND a.active = 1 limit 0,$limit"; 
		}
		$pages_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}

	function page_detail($pageId = 0)
	{
		
		$sql ="select a.* from tbl_options a WHERE a.option_id = $pageId AND a.active = 1 limit 1"; 
		$page_detail = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{	
			foreach($query->result() as $detail) {
				$page_detail["option_id"] = $detail->option_id;
				$page_detail["option_name"] = $detail->option_name;
				$option_value = stripslashes($detail->option_value);
				//var_dump($option_value);
  				//$option_value = json_decode($option_value);
				//var_dump($option_value);
				//$page_detail["option_value"] = $detail->option_value;
				$page_detail["option_value"] = $option_value; 
				
				$page_detail["active"] = $detail->active;	
			}
			return $page_detail;
		}else {
			return false;
		}
		
	}
	
	function images_list($productID = 0)
	{
		
		$sql ="select a.*,b.product_name from tbl_medias a, tbl_products b
WHERE a.product_id = $productID AND a.product_id = b.product_id"; 
		
		$product_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}

	function validation($type ="1")
	{
		$res = false;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('txt_title', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_desc', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_keywords', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_author', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_robot', '', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			return $res;
		}
		else
		{
			if($mode == "add") {
				$res = $this->add_page();
			} else {
				$res = $this->edit_page();
			}
			return $res;
		}
	}
	

	function add_product()
	{
		$res_add = false;
		
		$product_name = trim($this->input->post('txt_title'));
		$product_desc = trim($this->input->post('txt_desc'));
		$product_price = intval($this->input->post('txt_price'));
		$product_catg= intval($this->input->post('cbo_catg'));
		$time_process = date("Y-m-d H:i:s", time());
		
		$products = array(
			   'catg_id' => $product_catg,
			   'product_name' => $product_name,
			   'product_desc' =>  $product_desc,
			   'product_excerpt' =>  $product_desc,
			   'product_price' => $product_price,
			   'create_date' => $time_process
			);
	
		$this->db->insert('tbl_products' , $products);
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}
	
	function edit_page()
	{
		$res_add = false;
		header("content-type:text/javascript;charset=utf-8");	
		
		$page_title = trim($this->input->post('txt_title'));
		$page_desc = trim($this->input->post('txt_desc'));
		$page_keywords = trim($this->input->post('txt_keywords'));
		$page_author= trim($this->input->post('txt_author'));
		$page_robot= trim($this->input->post('txt_robot'));
		
		$option_id= intval($this->input->post('option_id'));
		$time_process = date("Y-m-d H:i:s", time());
		
		$option_value = array(
			   'title' => rawurlencode($page_title),
			   'description' => rawurlencode($page_desc),
			   'keywords' => rawurlencode($page_keywords),
			   'author' => rawurlencode($page_author),
			   'robots' => rawurlencode($page_robot)
			);
			
		$option_json = json_encode($option_value);

		$products = array(
			   'option_value' =>  $option_json,
			   
			   'update_date' => $time_process
			);
			
		$this->db->update('tbl_options', $products, "option_id = ".$option_id);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}


	function authorize($row="")
	{
		$this->load->library('session');
		list($expire, $domain, $path, $prefix) = $this->m_authen->initcookie();

		$array['id'] = $row['user_id'];
		$array['user_login'] = $row['user_login'];
		$array['display_name'] = $row['display_name'];
		//$array['key'] = mb5('ck'.$row['']);

		$this->m_authen->setcookies('authorized', json_encode($array));

		//set session
		$admindata = array(
                   'id'  => $row['user_id'],
                   'user_login'     => $row['user_login'],
                   'display_name'     => $row['display_name'],
                   'logged_in' => TRUE
		);

		$this->session->set_userdata($admindata);
		echo 1;
	}
	
}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */