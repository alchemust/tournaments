<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_login extends CI_Model {

	function __construct()
	{
		parent::__construct();

		//LOAD DB DEFAULT
		$this->load->library('Utility');
		
		$this->load->database('default');
	}

	function index()
	{
		$this->validation();
	}

	function validation($mode ="1")
	{
		$res = 0;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login_user', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('login_pass', '', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			return $res;
		}
		else
		{
			if($mode == "1") {
				$res = $this->process(1);
			} else if($mode == "2") {
				$res = $this->process($type);
			} 
			//$this->process();
		}
		return $res; 
	}

	function process($type=1)
	{
		$username = trim($this->input->post('login_user'));
		$password = trim($this->input->post('login_pass'));
		log_message('info',$username." ".$password);
		
		$sql ="SELECT ID,user_id,user_name,user_type FROM tour_users WHERE user_name = '".$username ."' AND user_password = '".$password."' AND user_type = '".$type."' AND user_active='1' ";
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $this->authorize($query->row_array());
		}else {
			return 0;
		}
	}

	function authorize($row="")
	{
		$res_sess = 0;
		$this->load->library('session');
		//list($expire, $domain, $path, $prefix) = $this->m_authen->initcookie();

		$array['id'] = $row['user_id'];
		$array['user_name'] = $row['user_name'];
		$array['user_type'] = $row['user_type'];
		
		//$array['key'] = mb5('ck'.$row['']);

		//$this->m_authen->setcookies('authorized', json_encode($array));

		//set session
		$admindata = array(
                   'id'  => $row['user_id'],
                   'user_login'     => $row['user_name'],
				   'user_type'     => $row['user_type'],
                   'logged_in' => TRUE
		);

		$this->session->set_userdata($admindata);
		$res_sess = 1;
		return $res_sess;
	}
	
	function users_list($option = 0,$limit = 10)
	{
		if($option == 0) { 
			$sql ="select a.* from tbl_users a limit 0,$limit"; 
		} else {
			$sql ="select a.* from tbl_users a
			WHERE a.group_id = $groupId AND a.active = 1 limit 0,$limit"; 
		}
		$pages_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	function user_detail($userId = 0)
	{
		
		$sql ="SELECT user_id,user_login,user_pass, display_name FROM tbl_users WHERE user_id = $userId limit 1"; 
		
		$product_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	function edit_user()
	{
		$res_add = false;
		
		$login_user = trim($this->input->post('login_user'));
		$login_pass = trim($this->input->post('login_pass'));
		$user_id = trim($this->input->post('user_id'));

		$time_process = date("Y-m-d H:i:s", time());
		
		$users = array(
			   'user_login' => $login_user,
			   'user_pass' => $login_pass,
			   'update_date' => $time_process
			);
	
		$this->db->update('tbl_users', $users, "user_id = ".$user_id);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}
	
	
	function reject_user()
	{
		$res_add = false;
	
		$user_id = trim($this->input->post('user_id'));

		$time_process = date("Y-m-d H:i:s", time());
		
		$users = array(
			   'active' => 0,
			   'update_date' => $time_process
		);
	
		$this->db->update('tbl_users', $users, "user_id = ".$user_id);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}
	
	function add_user()
	{
		$res_add = false;
		
		$login_user = trim($this->input->post('login_user'));
		$login_pass = trim($this->input->post('login_pass'));
		$user_id = trim($this->input->post('user_id'));

		$time_process = date("Y-m-d H:i:s", time());
		
		$users = array(
			   'user_login' => $login_user,
			   'user_pass' => $login_pass,
			   'user_status' => 1,
			   'user_registered' => $time_process,
			   'create_date' => $time_process,
			   'update_date' => $time_process
			);
	
		$this->db->insert('tbl_users', $users);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}
	
	function check_login(){
		if ($this->m_authen->is_logged_in() === TRUE && $this->m_authen->getsession("user_type") == 1) {
			return TRUE;
		} else {
			redirect('/blackend/login', 'location', 301);
			exit;
		}
	}

}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */