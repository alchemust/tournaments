<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class blackend extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	
		$this->load->model('m_login');
		$this->load->library('pagination');
		//$this->load->model('backend/products');
	}
	
	public function main()
	{
		$this->m_login->check_login();
		$this->load->model('matches/m_matches');
		$this->load->model('tournament/m_tournament');
		$this->load->model('team/m_team');
		$this->load->model('chat/m_chat');
		$this->load->model('users/m_users');
		$data = array(
			'page' => 'main'
		);
		
		date_default_timezone_set('Asia/Bangkok');
		$datetime = date("Y-m-d", time());
		$fromDt = date('Y-m-d', strtotime($datetime .' -15 day'));
		$toDt = date('Y-m-d', strtotime($datetime .' +15 day'));
		
		$data_main["total_match"] = $this->m_matches->all_record_count();
		$data_main["total_tournament"] = $this->m_tournament->all_record_count();
		$data_main["total_team"] = $this->m_team->all_record_count();
		$data_main["total_chat"] = $this->m_chat->all_record_count();
		$data_main["total_users"] = $this->m_users->all_record_count();
		$data_main["total_user_static"] = $this->m_users->total_user_static($fromDt,$toDt);
		$data_main["recent_chat"] = $this->m_chat->recent_chat(20);
		
		$this->load->view('header',$data);
		$this->load->view('main',$data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		 
	}
	
	############### Match Controller ######################
	public function matches(){
		$this->m_login->check_login();
		
		$this->load->model('matches/m_matches');
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$config = array();
        $config["base_url"] = base_url() . "/blackend/matches/";
        $config["total_rows"] = $this->m_matches->all_record_count();
		$config["num_links"] = 10;
        $config["per_page"] = 50;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
    	$config["num_links"] = round($choice);
		$config['first_url'] = base_url() . "/blackend/matches/";
		
		$config['cur_tag_open'] = '<a class="current">';
		$config['cur_tag_close'] = '</a>';
		
        $this->pagination->initialize($config);

        $data_main["match_list"] = $this->m_matches->
            match_list($config["per_page"], $page,1);
        $data_main["links"] = $this->pagination->create_links();
		
		
		$data = array(
			'page' => 'matches'
		);
		$this->load->view('header',$data);
		$this->load->view('matches/manager_match',$data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function match_detail()
	{
		
		$this->m_login->check_login();
		$data_main = array();
		
		$match_id =  $this->uri->segment(3);
		$this->load->model('matches/m_matches');
		$this->load->model('chat/m_chat');
		if($this->input->get('code')) 
			$data_main["code"] = intval($this->input->get('code'));	
		
		$data_main["match_detail"] = $this->m_matches->match_detail($match_id);
		$data_main["recent_chat"] = $this->m_chat->match_chat($match_id);
		$data = array(
			'page' => 'matches'
		);
		
		$this->load->view('header',$data);
		$this->load->view('matches/match_detail', $data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function edit_match()
	{
		
		$this->load->model('matches/m_matches');
		$res = $this->m_matches->validation("edit");
		$challonge_match_id = intval($this->input->post('challonge_match_id'));
		if($res == true) {
			$data = array(
					'page' => 'matches'
			);
				
			$data_main["match_id"] = $challonge_match_id;
			$this->load->view('header',$data);
			$this->load->view('matches/match_complete', $data_main);
			$this->load->view('sidebar',$data);
			$this->load->view('footer');		
		} else {
			redirect('/blackend/match_detail/'.$challonge_match_id.'?code=201', 'refresh');
		}
		
	}
	############### End Of Match Controller ######################
	
	############### Teams Controller ##########################
	public function teams()
	{
		
		$this->m_login->check_login();
		
		$this->load->model('team/m_team');
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$config = array();
        $config["base_url"] = base_url() . "/blackend/teams/";
        $config["total_rows"] = $this->m_team->all_record_count();
		$config["num_links"] = 10;
        $config["per_page"] = 50;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
    	$config["num_links"] = round($choice);
		$config['first_url'] = base_url() . "/blackend/teams/";
		
		$config['cur_tag_open'] = '<a class="current">';
		$config['cur_tag_close'] = '</a>';
		
        $this->pagination->initialize($config);

        $data_main["team_list"] = $this->m_team->
            team_list($config["per_page"], $page,1);
        $data_main["links"] = $this->pagination->create_links();
		
		
		$data = array(
			'page' => 'teams'
		);
		$this->load->view('header',$data);
		$this->load->view('team/manager_team',$data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	############### End Of Teams Controller ######################
	
	############### Tournaments Controller ##########################
	public function tournaments() {
		$this->m_login->check_login();
		
		$this->load->model('tournament/m_tournament');
		
		if($this->input->get('code')) 
			$data_main["code"] = intval($this->input->get('code'));	
			
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$config = array();
        $config["base_url"] = base_url() . "/blackend/tournaments/";
        $config["total_rows"] = $this->m_tournament->all_record_count();
		$config["num_links"] = 10;
        $config["per_page"] = 50;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
    	$config["num_links"] = round($choice);
		$config['first_url'] = base_url() . "/blackend/tournaments/";
		
		$config['cur_tag_open'] = '<a class="current">';
		$config['cur_tag_close'] = '</a>';
		
        $this->pagination->initialize($config);

        $data_main["tour_list"] = $this->m_tournament->
            tournament_list($config["per_page"], $page,1);
        $data_main["links"] = $this->pagination->create_links();
		
		$data = array(
			'page' => 'tournaments'
		);
		
		$this->load->view('header',$data);
		$this->load->view('tournament/manager_tour', $data_main);
		$this->load->view('sidebar',$data);
		$this->load->view('footer');		
	}
	
	public function create_tournament()
	{
		
		$this->m_login->check_login();
		$data_main = array();
		
		$tour_id =  $this->uri->segment(3);
		$this->load->model('tournament/m_tournament');
		$this->load->model('games/m_games');
		
		$data_main["tour_detail"] = $this->m_tournament->tournament_detail($tour_id);
		$data_main["game_list"] = $this->m_games->game_list();
		
		$data = array(
			'page' => 'tournaments'
		);
		
		
		$this->load->view('header',$data);
		$this->load->view('tournament/create_tournament', $data_main);
		//$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function tournament_detail()
	{
		
		$this->m_login->check_login();
		$data_main = array();
		
		$tour_id =  $this->uri->segment(3);
		$this->load->model('tournament/m_tournament');
		$this->load->model('games/m_games');
		if($this->input->get('code')) 
			$data_main["code"] = intval($this->input->get('code'));	
		
		$data_main["tour_detail"] = $this->m_tournament->tournament_detail($tour_id);
		$data_main["game_list"] = $this->m_games->game_list();
		$data_main["round_date"] = $this->m_tournament->tour_round_date($tour_id);
		$data = array(
			'page' => 'tournaments'
		);
		
		
		$this->load->view('header',$data);
		$this->load->view('tournament/tournament_detail', $data_main);
		//$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function edit_round_date(){
		$this->load->model('tournament/m_tournament');
		$res = $this->m_tournament->edit_round_date("edit");
		if($res == true) {
			echo 100;
		} else{
			echo 200;
		}
	}
	
	public function team_detail()
	{
		
		$this->m_login->check_login();
		$data_main = array();
		
		$team_id =  $this->uri->segment(3);
		$this->load->model('team/m_team');
		
		$data_main["team_detail"] = $this->m_team->team_detail_by_team_id($team_id);
		
		
		$data = array(
			'page' => 'teams'
		);
		
		
		$this->load->view('header',$data);
		$this->load->view('team/team_detail', $data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function edit_tournament(){
		$this->load->model('tournament/m_tournament');
		$res = $this->m_tournament->validation("edit");
		
		if($res == true) {
			$tour_id = intval($this->input->post('tour_id'));
			redirect('/blackend/tournament_detail/'.$tour_id, 'refresh');
		} else {
			$tour_id = intval($this->input->post('tour_id'));
			redirect('/blackend/tournament_detail/'.$tour_id, 'refresh');
		}
	}
	
	public function add_tournament(){
		$this->load->model('tournament/m_tournament');
		$res = $this->m_tournament->validation("add");
		
		if($res > 0) {
			redirect('/blackend/tournament_detail/'.$res, 'refresh');
		} else {
			redirect('/blackend/create_tournament', 'refresh');
		}
	}
	
	public function advertising(){
		$data = array(
			'page' => 'advertising'
		);
		$this->load->view('header',$data);
		$this->load->view('advertising/manager_advert');
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function image_list()
	{
		
		$data_detail = array();
		
		//$this->load->view('header', $data);
		$this->load->view('tournament/image_list',  $data_detail );
		//$this->load->view('footer', $data);
		
		
	}
	
	public function schedule_list()
	{
		
		$data_detail = array();
		
		//$this->load->view('header', $data);
		$this->load->view('tournament/schedule_list',  $data_detail );
		//$this->load->view('footer', $data);
		
	}
	
	
	public function validation(){
		echo $this->m_login->validation();
	}
	
	public function start_tournament(){
		$res = false;
		
		$this->load->model('tournament/m_tournament');
		
		$tour_id = intval($this->input->post('tour_id'));	
		if(isset($tour_id)) {
			$res = $this->m_tournament->start_tournament($tour_id);
			if($res == true) { 
				$data = array(
					'page' => 'tournaments'
				);
				
				$data_main = array(
					'tour_id' => $tour_id
				);
				
				$this->load->view('header',$data);
				$this->load->view('tournament/tournament_complete', $data_main);
				$this->load->view('sidebar',$data);
				$this->load->view('footer');		
			} else {
				if($tour_id == 0) {
					redirect('/blackend/tournaments?code=201', 'refresh');
				} else {
					redirect('/blackend/tournament_detail/'.$tour_id.'?code=201', 'refresh');
				}
			}
		} else {
			redirect('/blackend/tournaments?code=201', 'refresh');
		}
		
	}
	
	############### Games Controller ##########################
	public function games()
	{
		
		$this->m_login->check_login();
		
		$this->load->model('games/m_games');
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$config = array();
        $config["base_url"] = base_url() . "/blackend/games/";
        $config["total_rows"] = $this->m_games->all_record_count();
		$config["num_links"] = 10;
        $config["per_page"] = 50;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
    	$config["num_links"] = round($choice);
		$config['first_url'] = base_url() . "/blackend/games/";
		
		$config['cur_tag_open'] = '<a class="current">';
		$config['cur_tag_close'] = '</a>';
		
        $this->pagination->initialize($config);

        $data_main["game_list"] = $this->m_games->
            game_manage($config["per_page"], $page,1);
        $data_main["links"] = $this->pagination->create_links();
		
		
		$data = array(
			'page' => 'games'
		);
		$this->load->view('header',$data);
		$this->load->view('games/manager_games',$data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function game_detail()
	{
		
		$this->m_login->check_login();
		$data_main = array();
		
		$team_id =  $this->uri->segment(3);
		$this->load->model('games/m_games');
		if($this->input->get('code')) 
			$data_main["code"] = intval($this->input->get('code'));	
		$data_main["game_detail"] = $this->m_games->game_detail($team_id);
		
		
		$data = array(
			'page' => 'games'
		);
		
		
		$this->load->view('header',$data);
		$this->load->view('games/game_detail', $data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	public function create_game(){
		$this->m_login->check_login();
		$data_main = array();
		
		if($this->input->get('code')) 
			$data_main["code"] = intval($this->input->get('code'));	
			
		$data = array(
			'page' => 'games'
		);
		
		
		$this->load->view('header',$data);
		$this->load->view('games/create_game', $data_main);
		$this->load->view('sidebar');
		$this->load->view('footer');		
	}
	
	public function add_new_game(){
		$this->load->model('games/m_games');
		$res = $this->m_games->validation("add");
		
		if($res > 0) {
			redirect('/blackend/game_detail/'.$res, 'refresh');
		} else {
			redirect('/blackend/create_game?code=201', 'refresh');
		}
	}
	
	public function edit_game(){
		$this->load->model('games/m_games');
		$res = $this->m_games->validation("edit");
		
		if($res == true) {
			$game_id = intval($this->input->post('game_id'));
			redirect('/blackend/game_detail/'.$game_id."?code=100", 'refresh');
		} else {
			$game_id = intval($this->input->post('game_id'));
			redirect('/blackend/game_detail/'.$game_id."?code=201", 'refresh');
		}
	}
	
	
	############### End Of Games Controller ######################
	
	############### Other Controller ##########################
	public function system_logs(){
		$this->m_login->check_login();
		$data = array('page' => 'system_logs');
		$this->load->view('header',$data);
		$this->load->view('system_logs');
		$this->load->view('footer');		
	}
	public function login() { $this->load->view('login/login_view'); }
	public function logout() {
		$this->session->sess_destroy();
		redirect('/blackend/login', 'location', 301);
	}
	############### End Of Other Controller ######################
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */