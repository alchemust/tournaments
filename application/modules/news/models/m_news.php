<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_news extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database('default');
	}

	function news_list($limit = 5)
	{
		
		$this->db->select('news_title, news_desc, news_link');

		$this->db->where('news_active', 1); 
		$this->db->limit($limit);
		$this->db->order_by("update_date", "DESC"); 
		$query = $this->db->get('tour_news');
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	
	}
	
	function reject_user()
	{
		$res_add = false;
	
		$user_id = trim($this->input->post('user_id'));

		$time_process = date("Y-m-d H:i:s", time());
		
		$users = array(
			   'active' => 0,
			   'update_date' => $time_process
		);
		
		$this->db->update('tbl_users', $users, "user_id = ".$user_id);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}
	
	function users_list($option = 0,$limit = 10)
	{
		if($option == 0) { 
			$sql ="select a.* from tbl_users a limit 0,$limit"; 
		} else {
			$sql ="select a.* from tbl_options a
WHERE a.group_id = $groupId AND a.active = 1 limit 0,$limit"; 
		}
		$pages_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}

	
}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */