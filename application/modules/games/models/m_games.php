<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_games extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database('default');
		$this->process_id = $this->session->userdata('session_id');
	}
	
	function game_list()
	{
		
		$this->db->select('game_id, game_name, game_desc, game_logo');

		//$this->db->where('news_active', 1); 
		//$this->db->limit($limit);
		$this->db->order_by("game_id", "ASC"); 
		$query = $this->db->get('tour_games');
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	
	}
	function index()
	{
		$this->validation();
	}
	function validation($mode ="add")
	{
		$res = false;
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('game_name', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('game_desc', '', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			return $res;
		}
		else
		{
			if($mode == "add") {
				$res = $this->create_game();
			} else {
				$res = $this->edit_game();
			}
			return $res;
		}
	}
	public function all_record_count() {

        return $this->db->count_all("tour_games");
	}
	
	function game_detail($gameID = 0){
		$this->db->select('game_id, game_name, game_desc, game_logo');


		$this->db->from('tour_games');
		//$this->db->join('tour_users b', 'a.user_id = b.user_id');
		$this->db->where('game_id', $gameID); 
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	function game_manage($limit = 0, $start = 10, $admin=0){
		$this->db->select('game_id, game_name, game_desc, game_logo');


		$this->db->from('tour_games');
		//$this->db->join('tour_users b', 'a.user_id = b.user_id');
		
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}
	
	function create_game() {
		$res_edit = false;
		
		$time_process = date("Y-m-d H:i:s", time());
		$game_name = trim($this->input->post('game_name'));
		$game_desc = trim($this->input->post('game_desc'));
		
		$game_detail = array(
		   'game_name' => $game_name,
		   'game_desc' => $game_desc,
		   'create_date' => $time_process
		);
		
		if( !empty($_FILES['game_logo']['name']) ) { 
				preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $_FILES['game_logo']['name'], $matches);
				// Location of thumbnail on server.
				$source_ext = strtolower($matches[0]);	
				if (!(($source_ext == "jpg" || $source_ext == "gif" || $source_ext == "png") && ($_FILES["tour_large_img"]["type"] == "image/jpeg" || $_FILES["game_logo"]["type"] == "image/gif" || $_FILES["game_logo"]["type"] == "image/png"))) {
				
					$thumbFileName = time().rand(0,100);
					$datetime = date('Y-m');
				
					$dirImg = IMGPATH."/".$datetime."/";
					
					if(!file_exists($dirImg)) mkdir($dirImg,0777);
					@chmod($dirImg,0777);
					
					$loc = $dirImg . $thumbFileName.$source_ext;
					$game_logo = $datetime."/".$thumbFileName.$source_ext;
					
					// Attempt to move the uploaded thumbnail to the thumbnail directory.
					if ( !empty($_FILES['game_logo']['tmp_name']) && move_uploaded_file($_FILES['game_logo']['tmp_name'], $loc) ) 
						
						$thumbUploaded = true;
						$game_detail["game_logo"] = $game_logo;
				}
		} else {
			$thumbUploaded = false;
		}
		
		
		$this->db->insert('tour_games' , $game_detail);
		$game_id = $this->db->insert_id();
		$logs = $this->process_id."|1||m_games.create_game|Creaet_New_Game|2|game_id:".$game_id.",detail:".implode($game_detail)."\n";
		$this->utility->writeLog("back_games",$logs);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = $game_id;
		return $res_add;
		
		
	}
	
	function edit_game() {
		$res_edit = false;
		
		$time_process = date("Y-m-d H:i:s", time());
		$game_id= intval($this->input->post('game_id'));
		$game_name = trim($this->input->post('game_name'));
		
		$game_desc = trim($this->input->post('game_desc'));
		$current_game_logo = trim($this->input->post('current_game_logo'));
		
		$game_detail = array(
		   'game_name' => $game_name,
		   'game_desc' => $game_desc,
		   'update_date' => $time_process
		);
		
		if( !empty($_FILES['game_logo']['name']) ) { 
				preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $_FILES['game_logo']['name'], $matches);
				// Location of thumbnail on server.
				$source_ext = strtolower($matches[0]);	
				if (!(($source_ext == "jpg" || $source_ext == "gif" || $source_ext == "png") && ($_FILES["tour_large_img"]["type"] == "image/jpeg" || $_FILES["game_logo"]["type"] == "image/gif" || $_FILES["game_logo"]["type"] == "image/png"))) {
					if(!empty($current_game_logo)) {
						@unlink(IMGPATH ."/". $current_game_logo);
					}
					$thumbFileName = time().rand(0,100);
					$datetime = date('Y-m');
				
					$dirImg = IMGPATH."/".$datetime."/";
					
					if(!file_exists($dirImg)) mkdir($dirImg,0777);
					@chmod($dirImg,0777);
					
					$loc = $dirImg . $thumbFileName.$source_ext;
					$game_logo = $datetime."/".$thumbFileName.$source_ext;
					
					// Attempt to move the uploaded thumbnail to the thumbnail directory.
					if ( !empty($_FILES['game_logo']['tmp_name']) && move_uploaded_file($_FILES['game_logo']['tmp_name'], $loc) ) 
						
						$thumbUploaded = true;
						$game_detail["game_logo"] = $game_logo;
				}
		} else {
			$thumbUploaded = false;
		}
		
		$this->db->update('tour_games', $game_detail, "game_id = ".$game_id);
		$logs = $this->process_id."|1||m_games.edit_game|Edit_Game_Detail|2|game_id:".$game_id.",detail:".implode($game_detail)."\n";
			$this->utility->writeLog("back_games",$logs);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_edit = true;
		return $res_edit;
		
	}

	
}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */