<div class="content">
	<?php if($tour_hilight){?>
    <div class="span8 center">
        <div class="hilight">
            <div id="myCarousel" class="carousel slide vertical">
                <!-- Carousel items -->
                <div class="carousel-inner">
                	
						<?php $count_hilight=0;foreach($tour_hilight as $obj=>$hilight) { ?>
                        <div class="item <?php echo ($count_hilight ==0) ? "active" : ""; ?>">
                            <img src="/timthumb.php?src=<?php echo IMGPATH_URL."/".$hilight->tour_large_img;?>&zc=1&w=620&h=400" width="620" height="400" alt="">
                            <div class="carousel-caption">
                              <h4><?php echo $hilight->tour_title;?></h4>
                              <p><?php echo $hilight->tour_excerpt;?></p>
                            </div>
                        </div>
                        <?php $count_hilight++;} ?>
                    
                </div>
                <?php if($count_hilight > 1) { ?>
                <!-- Carousel nav -->
                
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                <?php } ?>
            </div>
        </div>
     </div>
     <?php } ?>
     <div class="tournaments_wrapper center" id="main_tour">
     	<?php if($tour_list) { ?>
        <?php foreach($tour_list as $tour) {
?>
        <div class="post">
                <div class="img_post">
                	<?php $tour_img = ($tour->tour_medium_img) ? IMGPATH_URL."/".$tour->tour_medium_img : other_asset_url('no_image.jpg', 'frontend', 'images');?>
                    <?php $tour_game_logo = ($tour->game_logo) ? IMGPATH_URL."/".$tour->game_logo : other_asset_url('no_image.jpg', 'frontend', 'images');?>
                    <img src="/timthumb.php?src=<?php echo $tour_img?>&zc=1&w=185&h=195&a=r" class="postthumbnail">
                    <img class="logo_post" border="0" src="<?=$tour_game_logo;?>" alt="">
                </div>
                <div class="postlayout">
                <h2><a href="/tournament/detail/<?php echo $tour->tour_id;?>"><?php echo $tour->tour_title;?></a></h2>
                <div class="clear"></div>
                 <div class="postintro"><?php echo $tour->tour_excerpt;?></div>
                 
                 <div class="postteam">
                    <div class="plan">
                      <div class="plan-name-bronze">
                        <span class="plan-feature">ผู้เข้าแข่งขัน</span>
                        <h4 id="team_num_<?php echo $tour->tour_id; ?>"><?php echo $tour->tour_team_num;?></h4>
                      </div>
                    </div>
                    <?php if($tour->start_tournament == 1) { ?>
                    	  <span href="#main_tour" class="btn btn btn-info btn-plan-select" ><i class="icon-white icon-ok"></i> เริ่มแล้ว</span>
                         <?php } else { ?>
		                    <?php if($login && ($user_type !=1)){?>
			                    	<?php if(in_array($tour->tour_id, $tour_join)) { ?>
			                            <span href="#main_tour" class="btn btn-success btn-plan-select"><i class="icon-white icon-ok"></i> ร่วมแล้ว</span>
			                            <?php } else {?>
			                            
			                            <span onclick="tournament_join('<?php echo $tour->tour_id; ?>',this);" class="btn btn-warning btn-plan-select" id="btn_join_<?php echo $tour->tour_id; ?>" data-loading-text="Loading..." data-complete-text="<i class='icon-white icon-screenshot'></i> เข้าร่วม"><i class="icon-white icon-screenshot"></i> เข้าร่วม</span>
			                             
										<?php } ?>
		                    <?php } else { ?>
		                    		<span onclick="javascript:alert('ขออภัยค่ะ คุณยังไม่ทำการล็อคอินเข้าสู่ระบบค่ะ');" class="btn btn-warning btn-plan-select" id="btn_join_<?php echo $tour->tour_id; ?>" data-loading-text="Loading..." data-complete-text="<i class='icon-white icon-screenshot'></i> เข้าร่วม"><i class="icon-white icon-screenshot"></i> เข้าร่วม</span>
                    <?php } ?>
                    <?php } ?>	
                 </div>
                 <p class="postbuttom">
                    <span class="button"><a href="/tournament/detail/<?php echo $tour->tour_id;?>#teamlist">ดูรายชื่อสมาชิก</a></span> <span class="button"><a href="/schedule/<?php echo $tour->tour_id;?>">ตารางการแข่งขัน</a></span>
                 </p>
                
             </div>
        </div>
        <?php } ?>
        <?php } ?>
        
         <div class="clear"></div>
         <input name="team_id" id="team_id" type="hidden" value="<?php echo $team_id;?>" />
	</div>   
    
    <?php if($links) { ?>
    <div class="pagination pagination-small pagination-centered" id="main-pagination">
    	<?php echo $links;?>
    </div>
    <?php } ?>
    <script>
    $('.carousel').carousel({
      interval: 3000
    });
	
    </script>
</div>
