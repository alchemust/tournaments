<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('team/m_team');
		$this->load->model('tournament/m_tournament');
		$this->load->model('news/m_news');
		$this->load->library('pagination');
		$this->load->model('m_frontend');
	}
	
	
	public function index()
	{
		$data_main = array();
		$data_sidebar = array();
		
		$data_sidebar = $this->m_frontend->sidebar_info();
		
		$tour_hilight = $this->m_tournament->tournament_hilight();
		$data_main["tour_hilight"] = $tour_hilight;
		
		//var_dump($data_main); 
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$config = array();
        $config["base_url"] = base_url() . "page";
        $config["total_rows"] = $this->m_tournament->record_count();
		$config["num_links"] = 10;
        $config["per_page"] = 5;
        $config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
    	$config["num_links"] = round($choice);
		$config['first_url'] = base_url(); 
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#main-pagination">';
		$config['cur_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
		
		$data_main["user_type"] = $data_sidebar["user_type"];
		$data_main["team_id"] = $data_sidebar["team_id"];
		$data_main["tour_join"] =$data_sidebar["tour_join"];
		$data_main["login"] = $data_sidebar["login"];
        $data_main["tour_list"] = $this->m_tournament->
            tournament_list($config["per_page"], $page);
        $data_main["links"] = $this->pagination->create_links();
		
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar',$data_sidebar);
		$this->load->view('main_view', $data_main);
		$this->load->view('frontend/footer');
	}
	
	function test_login() {
		$admindata = array(
                   'ID'  => 5,
				   'user_id'     => 21165,
                   'user_name'     => "alchemust",
				   'user_email'     => "james_0011@hotmail.com",
				   'user_type'     => 2,
                   'logged_in' => TRUE,
				   'team_active' => 1
		);
		
		$admindata = array(
                   'ID'  => 6,
				   'user_id'     => 12345,
                   'user_name'     => "OKC",
				   'user_email'     => "james0011@gmail.com",
				   'user_type'     => 2,
                   'logged_in' => TRUE,
				   'team_active' => 1
		);
		
		$this->m_authen->set_authorize_member($admindata);
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */