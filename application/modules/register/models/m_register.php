<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_register extends CI_Model {
	var $process_id;
	var $key;
	
	function __construct()
	{
		parent::__construct();

		//LOAD DB DEFAULT
		$this->key = KEY;
		$this->process_id = $this->session->userdata('session_id');
		$this->load->database('default');
		$this->load->model('users/m_users');
	}

	function index()
	{
		$this->validation();
	}
	
	function user_validation($mode ="1")
	{
		$res = 0;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login_user', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('login_pass', '', 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			return $res;
		}
		else
		{
			if($mode == "1") {
				$res = $this->fps_validation();
			} else if($mode == "2") {
				$res = $this->process($type);
			}
			 
			return $res; 
			
		}
	}

	function fps_validation($type=1)
	{
		$res = "";
		//$this->key = "ThrEsTlEr21FpsMith";
		$app_id = 1;
		$username = trim($this->input->post('login_user'));
		$password = trim($this->input->post('login_pass'));
		$m = md5($this->key.$username.$password.$app_id);
		
		$url_send_mt = "http://fpsthailand.com/fps_api/members/restricted?app_id=".$app_id."&key=".$this->key."&u=".$username."&p=".$password."&m=".$m;
		$res = $this->curl_api($url_send_mt);
		$member_res = json_decode($res);
		
		if($member_res->{'code'} == "1000") {
			$logs = $this->process_id."|1||M_register.fps_validation|fps_login|2|curl_result:".$res."\n";
			$this->utility->writeLog("login",$logs);
			if($this->create_member($member_res)) {
				$res= '{"code": "1000"}';
			} else {
				$members_profile = $this->m_users->load_members_detail($member_res->{'user_row'}->{'user_id'});
				$this->m_authen->set_authorize_member($members_profile);
				$res= '{"code": "1000"}';
			}
				
		}
		return $res;
		/*
		log_message('info',$username." ".$password);
		
		$sql ="SELECT user_id,user_name,user_pwd FROM tour_users WHERE user_name = '".$username ."' AND user_pwd = '".$password."' AND user_type = '".$type."' AND user_active='1' ";
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $this->authorize($query->row_array());
		}else {
			return 0;
		}*/
	}

	function set_authorize_member($row="")
	{
		$res_sess = 0;
		/*
		list($expire, $domain, $path, $prefix) = $this->m_authen->initcookie();

		$array['id'] = $row['user_id'];
		$array['user_name'] = $row['user_name'];
		$array['user_type'] = $row['user_type'];
		
		//$array['key'] = mb5('ck'.$row['']);

		$this->m_authen->setcookies('authorized', json_encode($array));
		*/
		//set session
		$admindata = array(
                   'ID'  => $row['ID'],
				   'user_id'     => $row['user_id'],
                   'user_login'     => $row['user_name'],
				   'user_email'     => $row['user_email'],
				   'user_type'     => $row['user_type'],
                   'logged_in' => TRUE,
				   'team_active' => $row['team_active']
		);

		$this->session->set_userdata($admindata);
		$logs = $this->process_id."|1||M_register.set_authorize_member|set_session|1|res:".implode(",",$admindata)."\n";
		$this->utility->writeLog("login",$logs);
		$res_sess = 1;
		return $res_sess;
	}
	
	private function create_member($member) {
		$res_add = false;
		
		if(!$this->check_duplicate_member($member->{'user_row'}->{'user_id'})) {
			$user_id = intval($member->{'user_row'}->{'user_id'});
			$user_name = trim($member->{'user_row'}->{'username'});
			$user_password = trim($member->{'user_row'}->{'user_password'});
			$user_email = trim($member->{'user_row'}->{'user_email'});
			$user_ip = $this->input->ip_address();
			$time_process = date("Y-m-d H:i:s", time());
			
			$users = array(
				   'user_id' => $user_id,
				   'user_type' => 2,
				   'group_id' =>  1,
				   'user_name' =>  $user_name,
				   'user_password' => $user_password,
				   'user_email' => $user_email,
				   'team_active' => 0,
				   'user_ip' => $user_ip,
				   'create_date' => $time_process
				);
		
			$this->db->insert('tour_users' , $users);
			$member_id = $this->db->insert_id();
			$logs = $this->process_id."|1||M_register.create_member|create_member|2|member_id:".$member_id.",user_id:".$user_id.",user_type:1,group_id:1,user_email:".$user_email.",user_ip:".$user_ip."\n";
			$this->utility->writeLog("register",$logs);
			$this->utility->array_put_to_position($users, $member_id, 0, 'ID');
			$this->m_authen->set_authorize_member($users);
			
			$res_add = TRUE;
		} else {
			$res_add = FALSE;
		}
		return $res_add;
	}
	
	function load_members_detail($user_id = 0)
	{
		$member_deital = "";
		$user_id = intval($user_id);
		$sql ="select a.ID, a.user_id, a.user_type, a.group_id, a.user_name, a.user_password, a.user_email, a.team_active,a.user_ip from tour_users a WHERE a.user_active = 1 AND a.user_id = ".$user_id."  limit 1"; 
		
		$product_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			foreach ($query->result() as $row){
				$member_deital = array(
				   'ID' => $row->ID,
				   'user_id' => $row->user_id,
				   'user_type' => $row->user_type,
				   'group_id' =>  $row->group_id,
				   'user_name' =>  $row->user_name,
				   'user_email' =>$row->user_email,
				   'team_active' =>$row->team_active,
				   'user_ip' => $row->user_ip
				);
			}
		}else {
			$member_deital = "";
		}
		
		return $member_deital;
	}
	
	function check_duplicate_member($id) {
		$exists = $this->db->select('user_id')->where('user_id', $id)->get('tour_users')->num_rows();

		return $exists;
	}
	
	function curl_api($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$response = curl_exec($ch);
	
		if (curl_errno($ch)) {
			return false;
		} else {
			curl_close($ch);
		}
	
		return $response;
	}

}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */