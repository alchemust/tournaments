<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class register extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('m_register');
		$this->load->model('team/m_team');
		$this->load->model('m_frontend');
		//$this->load->model('backend/products');
	}
	
	public function login()
	{
		$this->load->view('frontend/header_main');
		$this->load->view('login_view');
		$this->load->view('frontend/footer');
	}
	
	public function form()
	{
		//echo  $user = $this->session->userdata('logged_in');
		$data_sidebar = array();
		
		if ($this->m_authen->is_logged_in() === TRUE) {
			$data = array(
				"hash_id" => md5(KEY.APPID.$this->m_authen->getsession("session_id")),
				"user_id" => $this->m_authen->getsession("user_id"),
				"app_id" => APPID
			);
			
			$data_sidebar = $this->m_frontend->sidebar_info();
			
			$this->load->view('frontend/header_main');
			$this->load->view('frontend/left_sidebar',$data_sidebar);
			$this->load->view('form_view',$data);
			$this->load->view('frontend/footer');
			
		} else {
			redirect('/', 'location', 301);
		}
	}
	
	public function complete() {
		//echo  $user = $this->session->userdata('logged_in');
		if ($this->m_authen->is_logged_in() === TRUE) {
			
			$data_sidebar = $this->m_frontend->sidebar_info();
			
			$this->load->view('frontend/header_main');
			$this->load->view('frontend/left_sidebar',$data_sidebar);
			$this->load->view('complete');
			$this->load->view('frontend/footer');
			
		} else {
			redirect('/', 'location', 301);
		}
	}

	public function user_validation() {
		echo $this->m_register->user_validation();
	}
	
	public function join_validation(){
		echo $this->m_team->join_validation();
	}
	
	public function team_validation() {
		//var_dump($_POST);
		//var_dump($_FILES);
		$res = $this->m_team->team_validation(); 
		if($res == 100){
			redirect('/register/complete/', 'location', 301);
		} else {
			redirect('/register/form/', 'location', 301);
		}
		
	}
	
	function upload(){
		if ( !empty($_FILES['file']['name']) ) { 
			preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $_FILES['img_product']['name'], $matches);
			// Location of thumbnail on server.
			$source_ext = strtolower($matches[0]);	
			$thumbFileName = time();
			$datetime = date('Y-m');
		
			$dirImg = IMGPATH."/".$datetime."/";
			
			if(!file_exists($dirImg)) mkdir($dirImg,0777);
			chmod($dirImg,0777);
			
			$loc = $dirImg . $thumbFileName.$source_ext;
			$product_img = $datetime."/".$thumbFileName.$source_ext;
			
			// Attempt to move the uploaded thumbnail to the thumbnail directory.
			if ( !empty($_FILES['img_product']['tmp_name']) && move_uploaded_file($_FILES['img_product']['tmp_name'], $loc) ) 
				//@unlink(CONTENT_DIR . $new_img);
				$thumbUploaded = true;
						
		} else {
			$thumbUploaded = false;
			
		}
	}
	
	public function user_logout(){
		echo $this->m_authen->logout(); 

	}
	public function test(){
		echo $this->m_team->check_duplicate_join(1,1);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */