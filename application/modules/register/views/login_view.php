<table width="926" border="0" align="center">  
  <tr> 
    <td width="920" valign="top"><table id="Table_01" width="920" height="67" border="0" cellpadding="0" cellspacing="0"> 
      <tr> 
        <td><img src="<?=other_asset_url('Untitled-2_01.jpg', 'frontend', 'images')?>" width="2" height="2" alt=""></td> 
        <td valign="top"><img src="<?=other_asset_url('Untitled-2_02.jpg', 'frontend', 'images')?>" width="920" height="2" alt=""></td> 
        <td><img src="<?=other_asset_url('Untitled-2_03.jpg', 'frontend', 'images')?>" width="2" height="2" alt=""></td> 
      </tr> 
      <tr> 
        <td background="<?=other_asset_url('Untitled-2_04.jpg', 'frontend', 'images')?>"><img src="<?=other_asset_url('Untitled-2_04.jpg', 'frontend', 'images')?>" width="2" height="" alt=""></td> 
        <td valign="top" bgcolor="1f1f1f"><table width="920" height="636" border="0"> 
          
          <tr> 
            <td height="802" valign="top">
                		<h1 class="header">
                        สมัครสมาชิก เพื่อสร้างทีม<div style="padding-top: 5px;"></div>
                        </h1>
                        
                		<div class="registers_wrapper">
                        	<div class="rule">
		<div class="content">
			<h2>www.fpsthailand.com - สมัครสมาชิก</h2>
			<p>ในขั้นตอนการสมัครสมาชิก  "www.fpsthailand.com"  ผู้สมัครจะต้องกรอกข้อมูลตามความเป็นจริง หากมีการเปลี่ยนแปลงใดๆ  "www.fpsthailand.com" ขอให้ท่านแก้ไข โดยการเปลี่ยนแปลงข้อมูลดังกล่าวจากปุ่ม "แก้ไขข้อมูลสมาชิก"<br><br>

1.  "www.fpsthailand.com" ให้บริการรับ และส่งอีเมล์ ผ่านทางเวปและระบบออนไลน์ของ "www.fpsthailand.com" แก่สมาชิกทุกท่าน โดยไม่คิดค่าบริการใดๆ ทั้งสิ้น ซึ่งทางสมาชิกต้องจัดหาอุปกรณ์ในการติดต่อเข้า ระบบอินเทอร์เน็ตพร้อมทั้งเป็นผู้รับผิดชอบในการจ่ายค่าบริการ โทรศัพท์ และค่าบริการอินเทอร์เน็ตเอง ชื่อติดต่อบริการ ( login name) ต้องใช้ภาษาอังกฤษเท่านั้น และต้องมีความยาว ระหว่าง 6-18 ตัวอักษร ใช้ได้เฉพาะตัวอักษร a-z, 0-9, -, _ ไม่เว้นช่องว่าง<br><br>

2. ไม่ว่าท่านจะอยู่มุมไหนของโลก ก็สามารถใช้บริการ "www.fpsthailand.com" เพียงมีคอมพิวเตอร์ที่เชื่อมต่ออินเทอร์เน็ตได้ ทั้งนี้อยู่ในความรับผิดชอบของผู้ใช้  "www.fpsthailand.com" ที่จะต้องปฏิบัติตามกฎระเบียบ ที่มีผลบังคับใช้ในประเทศต่างๆ  "www.fpsthailand.com" ขอสงวนสิทธิ์ในการให้บริการ และหยุดให้บริการเมื่อใดก็ได้ ตามแต่ความเหมาะสม โดยมิต้องบอกกล่าวให้ท่านทราบล่วงหน้า  "www.fpsthailand.com" ถือว่าความเป็นส่วนตัวเป็นเรื่องสำคัญมากสำหรับ การติดต่อสื่อสาร ทั้งนี้เพื่อความเป็นส่วนตัวของท่านเอง  "www.fpsthailand.com" ขอแจ้งให้ท่านทราบว่า เป็นหน้าที่ของท่านเอง ในการรักษาชื่อติดต่อบริการ ( login name) และรหัสผ่าน ( password) ให้ปลอดภัย ไม่บอกให้ผู้อื่นทราบ<br><br>

3.  "www.fpsthailand.com"  เปิดให้บริการสำหรับการใช้เพื่อส่วนตัวเท่านั้น ห้ามใช้  "www.fpsthailand.com"  เพื่อผลประโยชน์ทางธุรกิจในทุกรูปแบบ ทั้งการแอบอ้าง หรือขายบริการต่อ (resale) หากท่านทำความเสียหาย ให้กับผู้อื่น ทาง  "www.fpsthailand.com" จะไม่รับผิดชอบต่อข้อเสียหายในทุกกรณี <br><br>

4. สมาชิก "www.fpsthailand.com"  จะไม่นำบริการไปใช้ในกิจกรรมที่ละเมิดความเป็นส่วนตัวของผู้อื่น รวมทั้งกิจกรรมที่ผิดกฎหมาย หรือขัดต่อความสงบเรียบร้อย และศีลธรรมอันดีของสังคม ทาง  "www.fpsthailand.com"  ไม่รับผิดชอบต่อสิ่งที่ท่านละเมิดและสร้างความเสียหาย ให้กับผู้อื่นในทุกกรณี  "www.fpsthailand.com" เปิดให้บริการสำหรับการใช้เพื่อส่วนตัวเท่านั้น<br><br>

5. ห้ามใช้ข้อความที่ไม่สุภาพ หรือเป็นการหมิ่นประมาทผู้อื่นในการสื่อสาร ไม่ว่ากรณีใดๆ ทั้งสิ้น ทั้งนี้เพื่อสร้างวัฒนธรรมที่ดี ในการใช้เว็บ<br><br>

6.  "www.fpsthailand.com" ไม่รับประกันความเสียหายของจดหมายที่เกิดจากการใช้บริการของ  "www.fpsthailand.com" ซึ่งอาจจะไม่สามารถให้บริการได้ตลอด 24 ชั่วโมง เพราะเครื่องเซิร์ฟเวอร์ของ  "www.fpsthailand.com" อาจขัดข้องโดยเหตุสุดวิสัยที่ไม่อาจคาดการณ์ได้ อีกทั้ง  "www.fpsthailand.com" ไม่รับรองความปลอดภัยในการใช้เว็บ เพื่อสั่งซื้อสินค้าผ่านทางอินเทอร์เน็ต อย่างไรก็ดีระบบที่  "www.fpsthailand.com" นำมาให้บริการกับท่านเป็นระบบ ที่มีความปลอดภัยได้มาตรฐาน ซึ่งในภาวะการทำงานปกติจะไม่เกิด ความเสียหายใดๆ ข้อความ ภาพนิ่ง เสียง หรือภาพวิดีโอต่างๆ ที่พ่วงท้ายมากับจดหมาย  "www.fpsthailand.com"  เป็นทรัพย์สินของบริษัท กรุงเทพโทรทัศน์ และวิทยุ จำกัด ทางเราขอสงวนลิขสิทธิ์ในข้อความ ภาพนิ่ง เสียง และภาพวิดีโอเหล่านั้น ห้ามมิให้สมาชิกนำ ไปเผยแพร่ใน รูปแบบใดๆ โดยมิได้รับอนุญาต ทั้งนี้มีผลบังคับรวมไปถึงโลโก้ เครื่องหมายการค้าชื่อสินค้า และบริการต่างๆ ของ "www.fpsthailand.com" ด้วย<br><br>

7. หากมีการเปลี่ยนแปลงข้อตกลงในการให้บริการ "www.fpsthailand.com" จะแจ้งให้ท่านทราบ โดยจะประกาศ ข้อตกลงใหม่ขึ้นหน้าจอ เพื่อให้ท่านรับทราบ และท่านจะใช้บริการของ "www.fpsthailand.com" ต่อไปได้ เมื่อท่านยอมรับข้อตกลงใหม่ของ "www.fpsthailand.com"  โดยการกดปุ่ม " ยอมรับ" หาก  "www.fpsthailand.com" พบว่าสมาชิก ของ  "www.fpsthailand.com" ละเมิดข้อตกลงที่ได้กำหนดไว้  "www.fpsthailand.com"  ขอสงวนสิทธิ์ในการระงับการให้บริการ โดยมิต้องแจ้งให้ทราบล่วงหน้า
	</p>
		</div>
		<span class="corners-bottom"><span></span></span></div>

                            </div>
                        <div class="registers_wrapper">
                        	<div class="login">
                            	<h2>Login ด้วย FPSThailand Accunt</h2>
                                <form id="register_form" name="register_form" method="post" action="" class="login-form" >
                                <fieldset class="fields1">
		
                                <dl>
                                    <dt><label for="username">ชื่อผู้ใช้:</label></dt>
                                    <dd><input type="text" class="inputbox autowidth" value="" size="25" id="login_user" name="login_user" tabindex="1"></dd>
                                </dl>
                                <dl>
                                    <dt><label for="password">รหัสผ่าน:</label></dt>
                                    <dd><input type="password" class="inputbox autowidth" size="25" name="login_pass" id="login_pass" tabindex="2"></dd>
                                </dl>
                                
                                <dl>
                                    <dt>&nbsp;</dt>
                                    <dd><input type="hidden" value="d09ac74081be9245e86d18cb2874d033" name="sid">
                                    <input type="hidden" value="index.php" name="redirect">
                                    <input type="submit" class="button1" value="เข้าสู่ระบบ" tabindex="6" name="login"></dd>
                                            </dl>
                    			</fieldset>
                                </form>
                            </div>
                        </div>    
            </td></tr> 
           
        </table></td> 
        <td background="<?=other_asset_url('Untitled-2_06.jpg', 'frontend', 'images')?>"><img src="<?=other_asset_url('Untitled-2_06.jpg', 'frontend', 'images')?>" width="2" height="" alt=""></td> 
      </tr> 
      <tr> 
        <td><img src="<?=other_asset_url('Untitled-2_07.jpg', 'frontend', 'images')?>" width="2" height="2" alt=""></td> 
        <td valign="top"><img src="<?=other_asset_url('Untitled-2_08.jpg', 'frontend', 'images')?>" width="920" height="2" alt=""></td> 
        <td><img src="<?=other_asset_url('Untitled-2_09.jpg', 'frontend', 'images')?>" width="2" height="2" alt=""></td> 
      </tr> 
    </table></td>
    </tr> 
  
  
  <tr>
    <td valign="top"></a></td>
  </tr>
  
  <!--<tr>  </tr>-->
  
  </table> 
 