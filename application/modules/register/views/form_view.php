<?=css_asset('bootstrap-fileupload.css', 'frontend')?>
 <?=js_asset('bootstrap-fileupload.min.js' , 'frontend'); ?>
 <script type="text/javascript">

$(document).ready(function()
{
// Popover 
$('#registerForm input').hover(function()
{
$(this).popover('show')
});

// Validation
$("#registerHere").validate({
rules:{
user_name:"required",
user_email:{required:true,email: true},
pwd:{required:true,minlength: 6},
cpwd:{required:true,equalTo: "#pwd"},
gender:"required"
},

messages:{
user_name:"Enter your first and last name",
user_email:{
required:"Enter your email address",
email:"Enter valid email address"},
pwd:{
required:"Enter your password",
minlength:"Password must be minimum 6 characters"},
cpwd:{
required:"Enter confirm password",
equalTo:"Password and Confirm Password must match"},
gender:"Select Gender"
},

errorClass: "help-inline",
errorElement: "span",
highlight:function(element, errorClass, validClass)
{
$(element).parents('.control-group').addClass('error');
},
unhighlight: function(element, errorClass, validClass)
{
$(element).parents('.control-group').removeClass('error');
$(element).parents('.control-group').addClass('success');
}
});
});
</script>
<div class="span8" style="margin-top:20px;">
	<form id="registerForm" name="registerForm" method="post" class="form-horizontal" enctype="multipart/form-data" action="/register/team_validation" >
        <div class="well" style="background-color: #22262A; border: 1px solid #444444;">
		<legend style="color:#FF8A00;">ลงทะเบียน เพื่อสร้างทีม</legend>
		<div class="control-group">
	        <label class="control-label">ชื่อทีม  *</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-user"></i></span>
					<input type="text" class="input-xlarge" id="team_name" name="team_name" placeholder="ชื่อทีม">
				</div>
			</div>
		</div>
		<div class="control-group ">
	        <label class="control-label">โลโก้ทีม *</label>
			<div class="controls">
			    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="<?php echo other_asset_url('no_image.gif', 'frontend', 'images');?>" /></div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                       <div class="input-prepend"><span class="add-on"><i class="icon-file"></i></div></span><span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" id="logo_img" name="logo_img" /></span>
                      <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>
			</div>
		</div>
		<div class="control-group">
	        <label class="control-label">รายละเอียดของทีม :</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-user"></i></span>
					<input type="text" class="input-xlarge" id="team_desc" name="team_desc" placeholder="รายละเอียดของทีม">
				</div>
			</div>
		</div>
		<div class="control-group">
	        <label class="control-label">สมาชิกในทีม :</label>
	        <?php  $i = 1; while ($i <= 6) { ?>
			<div class="controls" style="margin-bottom:30px;">
				<div class="fileupload fileupload-new" data-provides="fileupload"  >
                      <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="<?php echo other_asset_url('no_image.gif', 'frontend', 'images');?>" /></div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                       <div class="input-prepend"><span class="add-on"><i class="icon-file"></i></div></span><span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" id="img_member[]" name="img_member[]" /></span>
                      <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>
			    <div class="input-prepend" >
				<span class="add-on"><i class="icon-user"></i></span>
					<input type="text" class="input-xlarge" id="member_name[]" name="member_name[]" placeholder="ชื่อตัวละคร <?php echo $i;?>">
				</div>
                   
			</div>
			 <?php $i++; } ?>
		</div>
		
		<div class="control-group">
				<label class="control-label"></label>
	     		 <div class="controls">
			       <button type="submit" class="btn btn-success" >Create My Team</button>
			      </div>
		
		</div>	
		</div>
	  </form>
</div>
  
     

 
 