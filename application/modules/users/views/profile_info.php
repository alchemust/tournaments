<div class="content">
       <div class="span8 center tournaments_wrapper">
                    <h1 class="header">Team Detail</h1>
                    <?php //var_dump($match_list);?>
                    <?php if($team_detail) { ?>
                     <div class="post">
                          <?php $team_img = ($team_detail["team_logo"]) ? IMGPATH_URL."/".$team_detail["team_logo"] : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                             <img src="/timthumb.php?src=<?php echo $team_img;?>&zc=1&w=185&h=185" width="185" height="185" class="postthumbnail">
                            <div class="teamdetail-main">
                                <div class="teamdetail-list">
                                  <ul>
                                        <li><span class="head"><?php echo $team_detail["team_name"]; ?></span> <?php echo team_star($team_detail["team_point"]); ?></li>     
                                        <li style="margin-bottom: 10px;"><span class="leader">รายชื่อสมาชิก : </span></li>  
                                        <?php if($team_detail["member_list"]) {?>
                                        <li>
                                          <?php foreach ($team_detail["member_list"] as $members => $member_list) { ?>
                                          <?php $member_logo = ($member_list->member_logo) ? IMGPATH_URL."/".$member_list->member_logo : other_asset_url('no_image.jpg', 'frontend', 'images'); ?>
                                        	<div style="height: 30px;"><img src="/timthumb.php?src=<?php echo $member_logo;?>&zc=1&w=40&h=40" /> <?php echo $member_list->member_name; ?></div>
                                            <br/>
                                          <?php  } ?>  
                                        </li>    
                                        <?php  } ?>        
                                         <li style="border-top: 1px solid #666666;width:300px;"></li>     
                                        <li><span class="leader" >Score : </span> <?php echo $team_detail["team_point"]; ?></li>  
                                        <li><span class="leader">Win : </span><?php echo $team_detail["team_win"]; ?></li>          
                                        <li><span class="leader">Lose : </span><?php echo $team_detail["team_lose"]; ?></li>                               
                                  </ul>
                                  </div>
                                  <div> 
                                  <button class="btn btn-primary btn-medium" id="btn-edit-profile" name="btn-edit-profile" type="button" style="margin:10px 0 30px 10px;">แก้ไขข้อมูล</button>
                                  </div>
            				</div>
            				<?php if($team_gamepoint) { ?>
            				<div style="float:left; text-align: center;">
            					<div class="header" style="color:#FF8A00;"><h4>คะแนนในแต่ละเกมส์</h4></div>
                                  <div id="members-menu" style="margin-left:30px;">
				                        <ul>
				                            <li>
				                                <div class="team-main-head">ชื่อเกมส์</div>
				                                <div class="team-enemy-head">คะแนนรวม</div>
				                                
				                            </li>
				                            
				                            <?php foreach($team_gamepoint as $gamepoint) { ?>
				                             <li class="menu1">
				                                 <div class="team-main"><?php echo $gamepoint->game_name;?></div>
				                                <div class="team-enemy"><?php echo $gamepoint->game_point;?></div>
				                           </li>
				                           <?php } ?>
				                          
				                       		</ul>
			                    	</div>
			                    </div>
			                     <?php } ?>
                    </div>
                    <?php if($match_list) { ?>
                    <div class="post">
                    	<div class="header" style="color:#FF8A00;"><h3>สถิติการแข่งขัน</h3></div>
                        <div id="members-menu">
                        <ul>
                            <li>
                                <div class="team-main-head">ชื่อทีม</div>
                                <div class="team-enemy-head">คู่แข่ง</div>
                                <div class="team-win-head">Win</div>
                                <div class="team-lost-head">Lose</div>
                            </li>
                            
                            <?php foreach($match_list as $match) { 
                            	
                                  list($player1_point, $player2_point) = explode("-",$match->scores_csv);
                             ?>
                             <li class="menu1">
                                 <div class="team-main"><?php echo $match->player1_name;?></div>
                                <div class="team-enemy"><?php echo $match->player2_name;?></div>
                                <div class="team-win"><?php echo $player1_point;?></div>
                                <div class="team-lost"><?php echo $player2_point;?></div>
                           </li>
                           <?php } ?>
                           
                           
                           
                       </ul>
                    </div>
                </div>
                <?php } ?>
               <?php } ?>
  </div>
  </div>
           
      