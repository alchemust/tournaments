<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Users extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database('default');
	}

	public function all_record_count() {

        return $this->db->count_all("tour_users");
	}
	
	public function total_user_static($fromDt, $toDt){
		$sql ="SELECT count(user_id) total_user , date(create_date) as user_date,UNIX_TIMESTAMP(create_date) as unix_time FROM `tour_users` WHERE create_date between '".$fromDt." 00:00:00' and '".$toDt." 00:00:00' group by date(create_date)"; 
		
		$product_list = array();
		$query = $this->db->query($sql);
		
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
	}

	function load_members_detail($user_id = 0)
	{
		$member_deital = "";
		$user_id = intval($user_id);
		$sql ="select a.ID, a.user_id, a.user_type, a.group_id, a.user_name, a.user_password, a.user_email, a.team_active,a.user_ip from tour_users a WHERE a.user_active = 1 AND a.user_id = ".$user_id."  limit 1"; 
		
		$product_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			foreach ($query->result() as $row){
				$member_deital = array(
				   'ID' => $row->ID,
				   'user_id' => $row->user_id,
				   'user_type' => $row->user_type,
				   'group_id' =>  $row->group_id,
				   'user_name' =>  $row->user_name,
				   'user_email' =>$row->user_email,
				   'team_active' =>$row->team_active,
				   'user_ip' => $row->user_ip
				);
			}
		}else {
			$member_deital = "";
		}
		
		return $member_deital;
	}
	
	function reject_user()
	{
		$res_add = false;
	
		$user_id = trim($this->input->post('user_id'));

		$time_process = date("Y-m-d H:i:s", time());
		
		$users = array(
			   'active' => 0,
			   'update_date' => $time_process
		);
		
		$this->db->update('tbl_users', $users, "user_id = ".$user_id);
		//$this->db->insert('tbl_products' , $products);
		
		//log_message('info',$username." ".$password);
		$res_add = true;
		return $res_add;
		
	}
	
	function users_list($option = 0,$limit = 10)
	{
		if($option == 0) { 
			$sql ="select a.* from tbl_users a limit 0,$limit"; 
		} else {
			$sql ="select a.* from tbl_options a
WHERE a.group_id = $groupId AND a.active = 1 limit 0,$limit"; 
		}
		$pages_list = array();
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			return $query->result() ;
		}else {
			return false;
		}
		
	}

	
}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */