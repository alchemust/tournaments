<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class users extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		//$this->load->model('m_users');
		$this->load->model('m_frontend');
		//$this->load->model('backend/products');
	}
	
	function profile_info(){
		$this->load->model('team/m_team');
		$this->load->model('matches/m_matches');
		
		$data_main = array();
		$data_sidebar = array();
		
		$user_id = ($this->m_authen->getsession("user_id")) ? $this->m_authen->getsession("user_id") : 0;
		
		//$team_id = ($this->m_authen->getsession("team_id")) ? $this->m_authen->getsession("team_id") : 0;
		$data_main["team_detail"] = $this->m_team->load_team_detail($user_id);
		$team_id = intval($data_main["team_detail"]["team_id"]);
		$data_main["team_gamepoint"] = $this->m_team->team_gamepoint($team_id);
		$data_main["match_list"] = $this->m_matches->match_list_by_team($team_id);
		$data_sidebar = $this->m_frontend->sidebar_info();
		
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar',$data_sidebar);
		$this->load->view('profile_info', $data_main);
		$this->load->view('frontend/footer');
	}
	
	function edit_profile(){
		$this->load->model('team/m_team');
		$this->load->model('matches/m_matches');
		
		$data_main = array();
		$data_sidebar = array();
		
		$user_id = ($this->m_authen->getsession("user_id")) ? $this->m_authen->getsession("user_id") : 0;
		
		//$team_id = ($this->m_authen->getsession("team_id")) ? $this->m_authen->getsession("team_id") : 0;
		$data_main["team_detail"] = $this->m_team->load_team_detail($user_id);
		$team_id = intval($data_main["team_detail"]["team_id"]);
		$data_main["team_gamepoint"] = $this->m_team->team_gamepoint($team_id);
		$data_main["match_list"] = $this->m_matches->match_list_by_team($team_id);
		$data_sidebar = $this->m_frontend->sidebar_info();
		
		$this->load->view('frontend/header_main');
		$this->load->view('frontend/left_sidebar',$data_sidebar);
		$this->load->view('edit_profile_view', $data_main);
		$this->load->view('frontend/footer');
	}
	
	function profile_validation(){
		
		//var_dump($_POST);
		$res = $this->m_team->team_validation("2"); 
		 
		if($res == 100){
			redirect('/users/complete/', 'location', 301);
		} else {
			redirect('/users/complete/', 'location', 301);
		}
	}
	
	public function complete() {
		//echo  $user = $this->session->userdata('logged_in');
		
			$data_sidebar = $this->m_frontend->sidebar_info();
			
			$this->load->view('frontend/header_main');
			$this->load->view('frontend/left_sidebar',$data_sidebar);
			$this->load->view('complete');
			$this->load->view('frontend/footer');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */