<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class m_authen {
	var $process_id;
	var $key;
	function m_authen()
	{
		$this->CI =& get_instance();

		//$this->CI->load->helper(array('url','cookie'));
	}
	
	/*
	function index()
	{
		if($this->CI->session->userdata('logged_in') === TRUE)
		{
			define('ADMIN_ID', $this->CI->session->userdata('admin_id'));
			define('ADMIN_NAME', $this->CI->session->userdata('admin_name'));
			define('STATUS_LOGIN', TRUE);

			$this->CI->smarty->assign('status_login', STATUS_LOGIN);
			$this->CI->smarty->assign('admin_id', ADMIN_NAME);
			$this->CI->smarty->assign('admin_name', ADMIN_NAME);
		}
		else
		{
			if ( ! $this->getcookie('authorized', TRUE))
			{
				redirect(SITE_URL.'login', 'location', 301);
			}
			else
			{
				$authorized = $this->getcookie('authorized', TRUE);
				$obj = json_decode($authorized);
					
				if (isset($obj->{'id'}) && isset($obj->{'name'}))
				{
					define('ADMIN_ID', $obj->{'id'});
					define('ADMIN_NAME', $obj->{'name'});
					define('STATUS_LOGIN', TRUE);
						
					$this->CI->smarty->assign('status_login', STATUS_LOGIN);
					$this->CI->smarty->assign('admin_id', ADMIN_NAME);
					$this->CI->smarty->assign('admin_name', ADMIN_NAME);
				}
				else
				{
					$this->deletecookie('authorized');
					redirect(SITE_URL.'login', 'location', 301);
				}
			}
		}
	}
	*/
	function initcookie()
	{
		$this->CI->load->helper('url');

		$expire = 86500;
		$domain = '.'.DOMAIN;
		$path = '/';
		$prefix = '';

		return array($expire, $domain, $path, $prefix);
	}

	function getcookie($var, $boolean=FALSE)
	{
		list($expire, $domain, $path, $prefix) = $this->initcookie();

		if ($boolean===FALSE) {
			return get_cookie($prefix.$var);
		} else {
			return get_cookie($prefix.$var, TRUE);
		}
	}

	function setcookies($var, $value)
	{
		list($expire, $domain, $path, $prefix) = $this->initcookie();
		set_cookie($var, $value, $expire, $domain, $path, $prefix);
	}

	function deletecookie($var)
	{
		list($expire, $domain, $path, $prefix) = $this->initcookie();

		delete_cookie($var, $domain, $path, $prefix);
	}
	
	 function is_logged_in()
    {
		//$res = FALSE;
        $user = $this->CI->session->userdata('logged_in');
		
        return $user;
		
    }
	
	 function getsession($var, $boolean=FALSE)
    {
        $sess_val = $this->CI->session->userdata($var);
        return $sess_val;
    }
    
	function setsession($name, $var, $boolean=FALSE)
    {
        $sess_val = $this->CI->session->set_userdata($name, $var);
        return TRUE;
    }
	
	function set_authorize_member($row="")
	{
		$this->key = KEY;
		$this->process_id = $this->CI->session->userdata('session_id');
		$res_sess = 0;
		/*
		list($expire, $domain, $path, $prefix) = $this->m_authen->initcookie();

		$array['id'] = $row['user_id'];
		$array['user_name'] = $row['user_name'];
		$array['user_type'] = $row['user_type'];
		
		//$array['key'] = mb5('ck'.$row['']);

		$this->m_authen->setcookies('authorized', json_encode($array));
		*/
		//set session
		$admindata = array(
                   'ID'  => $row['ID'],
				   'user_id'     => $row['user_id'],
                   'user_login'     => $row['user_name'],
				   'user_email'     => $row['user_email'],
				   'user_type'     => $row['user_type'],
                   'logged_in' => TRUE,
				   'team_active' => $row['team_active']
		);

		$this->CI->session->set_userdata($admindata);
		$logs = $this->process_id."|1||m_authen.set_authorize_member|set_session|1|res:".implode(",",$admindata)."\n";
		$this->CI->utility->writeLog("login",$logs);
		$res_sess = 1;
		return $res_sess;
	}
	
	function logout(){ 
		$this->key = KEY;
		$this->process_id = $this->CI->session->userdata('session_id');
		
		$this->CI->session->sess_destroy();
		$logs = $this->process_id."|1||m_authen.logout|user_logout|1|session_id:".$this->process_id."\n";
		$this->CI->utility->writeLog("logout",$logs);
		
		$res= '{"code": "1000"}';
		return $res; 
	}
	
}

// END CI_My_author class

/* End of file My_author.php */
/* Location: ./system/libraries/My_author.php */