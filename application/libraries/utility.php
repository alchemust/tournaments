<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
Class Utility {	
	
	function excerpt_textlimit($str, $length) {
	
		mb_internal_encoding("UTF-8");
	
		$string = htmlspecialchars($str);
		//readmore cut string
		if (mb_strlen($string) > $length){
		$textmore = mb_substr(strip_tags($str), 0, $length,'utf-8').'&#8230;';
		}else{
		$textmore = mb_substr(strip_tags($str), 0, $length,'utf-8');
		}
		return $textmore;
	}
    
	function uploaded_file($files, $current_file="", $mode="add"){
		$res = array();
		
		if( !empty($files['name']) ) { 
				preg_match("/(\.(?:jpg|jpeg|png|gif))$/i", $files['name'], $matches);
				// Location of thumbnail on server.
				$source_ext = strtolower($matches[0]);	
				if (!(($source_ext == "jpg" || $source_ext == "gif" || $source_ext == "png") && ($files["type"] == "image/jpeg" || $files["type"] == "image/gif" || $files["type"] == "image/png"))) {
					
					
					$thumbFileName = time().rand(0,100);
					$datetime = date('Y-m');
				
					$dirImg = IMGPATH."/".$datetime."/";
					
					if(!file_exists($dirImg)) mkdir($dirImg,0777);
					@chmod($dirImg,0777);
					
					$loc = $dirImg . $thumbFileName.$source_ext;
					$upload_file_path = $datetime."/".$thumbFileName.$source_ext;
					
					// Attempt to move the uploaded thumbnail to the thumbnail directory.
					if ( !empty($files['tmp_name']) && move_uploaded_file($files['tmp_name'], $loc) ) 
						
						$res["file_path"] = $upload_file_path;
						$res["result"] = true;
						if($mode == "edit") {
						if(!empty($current_file)) {
							@unlink(IMGPATH ."/". $current_file);
							$res["del_current_file"] = true;
						}
					} 
				}
		} else {
			$res["file_path"] = "";
			$res["result"] = false;
		}
		
		return $res;
	}

	function datediff($interval, $datefrom, $dateto, $using_timestamps = false) { 
		/* $interval can be: yyyy - Number of full years q - Number of full quarters m - Number of full months y - Difference between day numbers (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".) d - Number of full days w - Number of full weekdays ww - Number of full weeks h - Number of full hours n - Number of full minutes s - Number of full seconds (default) */ 
		if (!$using_timestamps) { 
			$datefrom = strtotime($datefrom); 
			$dateto = strtotime($dateto); 
		} 
	
		$difference = $dateto - $datefrom; 
		//echo "<br>";
		// Difference in seconds 
		switch($interval) { 
			case 'yyyy': // Number of full years 
				$years_difference = floor($difference / 31536000); if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) { $years_difference--; } if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) { $years_difference++; } $datediff = $years_difference; break; case "q": // Number of full quarters 
				$quarters_difference = floor($difference / 8035200); while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) { $months_difference++; } $quarters_difference--; $datediff = $quarters_difference; break; case "m": // Number of full months 
				$months_difference = floor($difference / 2678400); while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) { $months_difference++; } $months_difference--; $datediff = $months_difference; break; case 'y': // Difference between day numbers 
				$datediff = date("z", $dateto) - date("z", $datefrom); break; case "d": // Number of full days 
				$datediff = floor($difference / 86400); break; case "w": // Number of full weekdays 
				$days_difference = floor($difference / 86400); $weeks_difference = floor($days_difference / 7); // Complete weeks 
				$first_day = date("w", $datefrom); $days_remainder = floor($days_difference % 7); $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
				if ($odd_days > 7) { // Sunday $days_remainder--; 
	
					} 
				if ($odd_days > 6) { // Saturday $days_remainder--; 
	
				} 
				$datediff = ($weeks_difference * 5) + $days_remainder; 
			break; 
			case "ww": // Number of full weeks 
				$datediff = floor($difference / 604800); break; 
			case "h": // Number of full hours 
				$datediff = floor($difference / 3600); break; 
			case "n": // Number of full minutes 
				$datediff = floor($difference / 60); break; 
			default: // Number of full seconds (default) 				 
				$datediff = ($difference < 0) ? 0 : $difference*1000; 
				break; 
		} 
	
		return $datediff; 
	} 
	
	function writeLog($filename , $content, $type = 0) {
		/*
		###############################################################
		Logs Template 
		date time|processid|appid|app_trans_id|action|level|message
		###############################################################
		*/
		
		//$content_log = date('Y-m-d H:i:s')."|".$content;
		$datetime = date('Y_m_d');
		$dirLog = "logs/";
		$content_log = ($type == 0) ? date('Y-m-d H:i:s')."|".$content : $content;
		$fileName = ($type == 0) ? $dirLog."/".$filename."_".$datetime.".txt" : $dirLog."/".$filename.".xml";
		
		if(!file_exists($dirLog)) chmod($dirLog,0777);
		/* End create date directory in Log */
			
		$fp = fopen($fileName , "a+" );		
		fwrite( $fp , $content_log ); 
		fclose( $fp ); 	
	}
	
	function array_put_to_position(&$array, $object, $position, $name = null)
	{
			$count = 0;
			$return = array();
			foreach ($array as $k => $v) 
			{   
					// insert new object
					if ($count == $position)
					{   
							if (!$name) $name = $count;
							$return[$name] = $object;
							$inserted = true;
					}   
					// insert old object
					$return[$k] = $v; 
					$count++;
			}   
			if (!$name) $name = $count;
			if (!$inserted) $return[$name];
			$array = $return;
			return $array;
	}
	
	function generateRoundRobinPairings($num_players) { 
		//do we have a positive number of players? otherwise default to 4 
		$num_players = ($num_players > 0) ? (int)$num_players : 4; 
		//set number of players to even number 
		$num_players = ($num_players % 2 == 0) ? $num_players : $num_players + 1; 
		//format for pretty alignment of pairings across rounds 
		$format = "%0" . ceil(log10($num_players)) . "d"; 
		$pairing = "$format-$format "; 
		//set the return value 
		$ret = $num_players . " Player Round Robin:\n-----------------------"; 
		//print the rounds 
		for ($round = 1; $round < $num_players; $round++) { 
			$ret .= sprintf("\nRound #$format : ", $round); 
			$players_done = array(); 
			//print the pairings 
			for ($player = 1; $player < $num_players; $player++) { 
				if (!in_array($player, $players_done)) { 
					//select opponent 
					$opponent = $round - $player; 
					$opponent += ($opponent < 0) ? $num_players : 1; 
					//ensure opponent is not the current player 
					if ($opponent != $player) { 
						//choose colours 
						if ($player % 2 == $opponent % 2) { 
							if ($player < $opponent) { 
								//player plays black 
								$ret .= sprintf($pairing, $opponent, $player); 
							} else { 
								//player plays white 
								$ret .= sprintf($pairing, $player, $opponent); 
							} 
						} else { 
							if ($player < $opponent) { 
								//player plays white 
								$ret .= sprintf($pairing, $player, $opponent); 
							} else { 
								//player plays black 
								$ret .= sprintf($pairing, $opponent, $player); 
							} 
						} 
						//these players are done for this round 
						$players_done[] = $player; 
						$players_done[] = $opponent; 
					} 
				} 
			} 
			//print the last pairing (i.e. for the last player) 
			if ($round % 2 == 0) { 
				$opponent = ($round + $num_players) / 2; 
				//last player plays white 
				$ret .= sprintf($pairing, $num_players, $opponent); 
			} else { 
				$opponent = ($round + 1) / 2; 
				//last player plays black 
				$ret .= sprintf($pairing, $opponent, $num_players); 
			} 
		} 
		return $ret; 
	} 
	
	function utf8_to_cp1252($string)
	{
		static $transform = array(
			"\xE2\x82\xAC" => "\x80",
			"\xE2\x80\x9A" => "\x82",
			"\xC6\x92" => "\x83",
			"\xE2\x80\x9E" => "\x84",
			"\xE2\x80\xA6" => "\x85",
			"\xE2\x80\xA0" => "\x86",
			"\xE2\x80\xA1" => "\x87",
			"\xCB\x86" => "\x88",
			"\xE2\x80\xB0" => "\x89",
			"\xC5\xA0" => "\x8A",
			"\xE2\x80\xB9" => "\x8B",
			"\xC5\x92" => "\x8C",
			"\xC5\xBD" => "\x8E",
			"\xE2\x80\x98" => "\x91",
			"\xE2\x80\x99" => "\x92",
			"\xE2\x80\x9C" => "\x93",
			"\xE2\x80\x9D" => "\x94",
			"\xE2\x80\xA2" => "\x95",
			"\xE2\x80\x93" => "\x96",
			"\xE2\x80\x94" => "\x97",
			"\xCB\x9C" => "\x98",
			"\xE2\x84\xA2" => "\x99",
			"\xC5\xA1" => "\x9A",
			"\xE2\x80\xBA" => "\x9B",
			"\xC5\x93" => "\x9C",
			"\xC5\xBE" => "\x9E",
			"\xC5\xB8" => "\x9F",
			"\xC2\xA0" => "\xA0",
			"\xC2\xA1" => "\xA1",
			"\xC2\xA2" => "\xA2",
			"\xC2\xA3" => "\xA3",
			"\xC2\xA4" => "\xA4",
			"\xC2\xA5" => "\xA5",
			"\xC2\xA6" => "\xA6",
			"\xC2\xA7" => "\xA7",
			"\xC2\xA8" => "\xA8",
			"\xC2\xA9" => "\xA9",
			"\xC2\xAA" => "\xAA",
			"\xC2\xAB" => "\xAB",
			"\xC2\xAC" => "\xAC",
			"\xC2\xAD" => "\xAD",
			"\xC2\xAE" => "\xAE",
			"\xC2\xAF" => "\xAF",
			"\xC2\xB0" => "\xB0",
			"\xC2\xB1" => "\xB1",
			"\xC2\xB2" => "\xB2",
			"\xC2\xB3" => "\xB3",
			"\xC2\xB4" => "\xB4",
			"\xC2\xB5" => "\xB5",
			"\xC2\xB6" => "\xB6",
			"\xC2\xB7" => "\xB7",
			"\xC2\xB8" => "\xB8",
			"\xC2\xB9" => "\xB9",
			"\xC2\xBA" => "\xBA",
			"\xC2\xBB" => "\xBB",
			"\xC2\xBC" => "\xBC",
			"\xC2\xBD" => "\xBD",
			"\xC2\xBE" => "\xBE",
			"\xC2\xBF" => "\xBF",
			"\xC3\x80" => "\xC0",
			"\xC3\x81" => "\xC1",
			"\xC3\x82" => "\xC2",
			"\xC3\x83" => "\xC3",
			"\xC3\x84" => "\xC4",
			"\xC3\x85" => "\xC5",
			"\xC3\x86" => "\xC6",
			"\xC3\x87" => "\xC7",
			"\xC3\x88" => "\xC8",
			"\xC3\x89" => "\xC9",
			"\xC3\x8A" => "\xCA",
			"\xC3\x8B" => "\xCB",
			"\xC3\x8C" => "\xCC",
			"\xC3\x8D" => "\xCD",
			"\xC3\x8E" => "\xCE",
			"\xC3\x8F" => "\xCF",
			"\xC3\x90" => "\xD0",
			"\xC3\x91" => "\xD1",
			"\xC3\x92" => "\xD2",
			"\xC3\x93" => "\xD3",
			"\xC3\x94" => "\xD4",
			"\xC3\x95" => "\xD5",
			"\xC3\x96" => "\xD6",
			"\xC3\x97" => "\xD7",
			"\xC3\x98" => "\xD8",
			"\xC3\x99" => "\xD9",
			"\xC3\x9A" => "\xDA",
			"\xC3\x9B" => "\xDB",
			"\xC3\x9C" => "\xDC",
			"\xC3\x9D" => "\xDD",
			"\xC3\x9E" => "\xDE",
			"\xC3\x9F" => "\xDF",
			"\xC3\xA0" => "\xE0",
			"\xC3\xA1" => "\xE1",
			"\xC3\xA2" => "\xE2",
			"\xC3\xA3" => "\xE3",
			"\xC3\xA4" => "\xE4",
			"\xC3\xA5" => "\xE5",
			"\xC3\xA6" => "\xE6",
			"\xC3\xA7" => "\xE7",
			"\xC3\xA8" => "\xE8",
			"\xC3\xA9" => "\xE9",
			"\xC3\xAA" => "\xEA",
			"\xC3\xAB" => "\xEB",
			"\xC3\xAC" => "\xEC",
			"\xC3\xAD" => "\xED",
			"\xC3\xAE" => "\xEE",
			"\xC3\xAF" => "\xEF",
			"\xC3\xB0" => "\xF0",
			"\xC3\xB1" => "\xF1",
			"\xC3\xB2" => "\xF2",
			"\xC3\xB3" => "\xF3",
			"\xC3\xB4" => "\xF4",
			"\xC3\xB5" => "\xF5",
			"\xC3\xB6" => "\xF6",
			"\xC3\xB7" => "\xF7",
			"\xC3\xB8" => "\xF8",
			"\xC3\xB9" => "\xF9",
			"\xC3\xBA" => "\xFA",
			"\xC3\xBB" => "\xFB",
			"\xC3\xBC" => "\xFC",
			"\xC3\xBD" => "\xFD",
			"\xC3\xBE" => "\xFE",
			"\xC3\xBF" => "\xFF"
		);
		return strtr($string, $transform);
	}
	function phpbb_hash($password)
	{
		$itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	
		$random_state = $this->unique_id();
		$random = '';
		$count = 6;
	
		if (($fh = @fopen('/dev/urandom', 'rb')))
		{
			$random = fread($fh, $count);
			fclose($fh);
		}
	
		if (strlen($random) < $count)
		{
			$random = '';
	
			for ($i = 0; $i < $count; $i += 16)
			{
				$random_state = md5($this->unique_id() . $random_state);
				$random .= pack('H*', md5($random_state));
			}
			$random = substr($random, 0, $count);
		}
	
		$hash = _hash_crypt_private($password, _hash_gensalt_private($random, $itoa64), $itoa64);
	
		if (strlen($hash) == 34)
		{
			return $hash;
		}
	
		return md5($password);
	}
	
	function _hash_crypt_private($password, $setting, &$itoa64)
	{
		$output = '*';
	
		// Check for correct hash
		if (substr($setting, 0, 3) != '$H$' && substr($setting, 0, 3) != '$P$')
		{
			return $output;
		}
	
		$count_log2 = strpos($itoa64, $setting[3]);
	
		if ($count_log2 < 7 || $count_log2 > 30)
		{
			return $output;
		}
	
		$count = 1 << $count_log2;
		$salt = substr($setting, 4, 8);
	
		if (strlen($salt) != 8)
		{
			return $output;
		}
	
		/**
		* We're kind of forced to use MD5 here since it's the only
		* cryptographic primitive available in all versions of PHP
		* currently in use.  To implement our own low-level crypto
		* in PHP would result in much worse performance and
		* consequently in lower iteration counts and hashes that are
		* quicker to crack (by non-PHP code).
		*/
		if (PHP_VERSION >= 5)
		{
			$hash = md5($salt . $password, true);
			do
			{
				$hash = md5($hash . $password, true);
			}
			while (--$count);
		}
		else
		{
			$hash = pack('H*', md5($salt . $password));
			do
			{
				$hash = pack('H*', md5($hash . $password));
			}
			while (--$count);
		}
	
		$output = substr($setting, 0, 12);
		$output .= $this->_hash_encode64($hash, 16, $itoa64);
	
		return $output;
	}
	
	function _hash_gensalt_private($input, &$itoa64, $iteration_count_log2 = 6)
	{
		if ($iteration_count_log2 < 4 || $iteration_count_log2 > 31)
		{
			$iteration_count_log2 = 8;
		}
	
		$output = '$H$';
		$output .= $itoa64[min($iteration_count_log2 + ((PHP_VERSION >= 5) ? 5 : 3), 30)];
		$output .= _hash_encode64($input, 6, $itoa64);
	
		return $output;
	}
	
	function unique_id($extra = 'c')
	{
		static $dss_seeded = false;
		global $config;
	
		$val = $config['rand_seed'] . microtime();
		$val = md5($val);
		$config['rand_seed'] = md5($config['rand_seed'] . $val . $extra);
	
		if ($dss_seeded !== true && ($config['rand_seed_last_update'] < time() - rand(1,10)))
		{
			set_config('rand_seed_last_update', time(), true);
			set_config('rand_seed', $config['rand_seed'], true);
			$dss_seeded = true;
		}
	
		return substr($val, 4, 16);
	}
	
	function _hash_encode64($input, $count, &$itoa64)
	{
		$output = '';
		$i = 0;
	
		do
		{
			$value = ord($input[$i++]);
			$output .= $itoa64[$value & 0x3f];
	
			if ($i < $count)
			{
				$value |= ord($input[$i]) << 8;
			}
	
			$output .= $itoa64[($value >> 6) & 0x3f];
	
			if ($i++ >= $count)
			{
				break;
			}
	
			if ($i < $count)
			{
				$value |= ord($input[$i]) << 16;
			}
	
			$output .= $itoa64[($value >> 12) & 0x3f];
	
			if ($i++ >= $count)
			{
				break;
			}
	
			$output .= $itoa64[($value >> 18) & 0x3f];
		}
		while ($i < $count);
	
		return $output;
	}
	
	function phpbb_check_hash($password, $hash)
	{
		$itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		if (strlen($hash) == 34)
		{
		
			return ($this->_hash_crypt_private($password, $hash, $itoa64) === $hash) ? true : false;
		}
		
		return (md5($password) === $hash) ? true : false;
	}
	
	function bb_parse($string) { 
        $tags = 'b|i|size|color|center|quote|url|img'; 
        while (preg_match_all('`\[('.$tags.')=?(.*?)\](.+?)\[/\1\]`', $string, $matches)) foreach ($matches[0] as $key => $match) { 
            list($tag, $param, $innertext) = array($matches[1][$key], $matches[2][$key], $matches[3][$key]); 
            switch ($tag) { 
                case 'b': $replacement = "<strong>$innertext</strong>"; break; 
                case 'i': $replacement = "<em>$innertext</em>"; break; 
                case 'size': $replacement = "<span style=\"font-size: $param;\">$innertext</span>"; break; 
                case 'color': $replacement = "<span style=\"color: $param;\">$innertext</span>"; break; 
                case 'center': $replacement = "<div class=\"centered\">$innertext</div>"; break; 
                case 'quote': $replacement = "<blockquote>$innertext</blockquote>" . $param? "<cite>$param</cite>" : ''; break; 
                case 'url': $replacement = $innertext; break; 
                case 'img': 
                	if(!empty($param)) {
                    list($width, $height) = preg_split('`[Xx]`', $param);
                	} else {
                		$width = "";
                		$height = "";
                	} 
                    $replacement = "<img src=\"$innertext\" " . (is_numeric($width)? "width=\"$width\" " : '') . (is_numeric($height)? "height=\"$height\" " : '') . '/>'; 
                break; 
                case 'video': 
                    $videourl = parse_url($innertext); 
                    parse_str($videourl['query'], $videoquery); 
                    if (strpos($videourl['host'], 'youtube.com') !== FALSE) $replacement = '<embed src="http://www.youtube.com/v/' . $videoquery['v'] . '" type="application/x-shockwave-flash" width="425" height="344"></embed>'; 
                    if (strpos($videourl['host'], 'google.com') !== FALSE) $replacement = '<embed src="http://video.google.com/googleplayer.swf?docid=' . $videoquery['docid'] . '" width="400" height="326" type="application/x-shockwave-flash"></embed>'; 
                break; 
            } 
            $string = str_replace($match, $replacement, $string); 
        } 
        return $string; 
    } 
} 

?>