<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class authen {

	function CI_M_authen()
	{
		$this->CI =& get_instance();

		$this->CI->load->helper(array('url','cookie'));
	}

	function index()
	{
		$this->CI->load->library('session');
		if($this->CI->session->userdata('logged_in') === TRUE)
		{
			define('ADMIN_ID', $this->CI->session->userdata('admin_id'));
			define('ADMIN_NAME', $this->CI->session->userdata('admin_name'));
			define('STATUS_LOGIN', TRUE);

			$this->CI->smarty->assign('status_login', STATUS_LOGIN);
			$this->CI->smarty->assign('admin_id', ADMIN_NAME);
			$this->CI->smarty->assign('admin_name', ADMIN_NAME);
		}
		else
		{
			if ( ! $this->getcookie('authorized', TRUE))
			{
				redirect(SITE_URL.'login', 'location', 301);
			}
			else
			{
				$authorized = $this->getcookie('authorized', TRUE);
				$obj = json_decode($authorized);
					
				if (isset($obj->{'id'}) && isset($obj->{'name'}))
				{
					define('ADMIN_ID', $obj->{'id'});
					define('ADMIN_NAME', $obj->{'name'});
					define('STATUS_LOGIN', TRUE);
						
					$this->CI->smarty->assign('status_login', STATUS_LOGIN);
					$this->CI->smarty->assign('admin_id', ADMIN_NAME);
					$this->CI->smarty->assign('admin_name', ADMIN_NAME);
				}
				else
				{
					$this->deletecookie('authorized');
					redirect(SITE_URL.'login', 'location', 301);
				}
			}
		}
	}

	function initcookie()
	{
		$this->CI->load->helper('url');

		$expire = 86500;
		$domain = '.'.DOMAIN;
		$path = '/';
		$prefix = '';

		return array($expire, $domain, $path, $prefix);
	}

	function getcookie($var, $boolean=FALSE)
	{
		list($expire, $domain, $path, $prefix) = $this->initcookie();

		if ($boolean===FALSE) {
			return get_cookie($prefix.$var);
		} else {
			return get_cookie($prefix.$var, TRUE);
		}
	}

	function setcookies($var, $value)
	{
		list($expire, $domain, $path, $prefix) = $this->initcookie();
		set_cookie($var, $value, $expire, $domain, $path, $prefix);
	}

	function deletecookie($var)
	{
		list($expire, $domain, $path, $prefix) = $this->initcookie();

		delete_cookie($var, $domain, $path, $prefix);
	}
}

// END CI_My_author class

/* End of file My_author.php */
/* Location: ./system/libraries/My_author.php */