<?php
if (!defined('BASEPATH'))
	exit ('No direct script access allowed');

class CI_XmlJson {

	var $CI;

	function CI_XmlJson() {
		error_reporting( 0 );
		$this->CI = & get_instance();
		$this->CI->load->library('memcache');
	}

	function readXml($domnode, & $array) {
		$array_ptr = & $array;
		$domnode = $domnode->firstChild;
		while (!is_null($domnode)) {
			switch ($domnode->nodeType) {
				case XML_TEXT_NODE :
					if (!(trim($domnode->nodeValue) == ""))
						$array_ptr['cdata'] = $domnode->nodeValue;
					break;
				case XML_CDATA_SECTION_NODE :
					if (!(trim($domnode->nodeValue) == ""))
						$array_ptr['cdata'] = $domnode->nodeValue;
					break;
				case XML_ELEMENT_NODE :
					$array_ptr = & $array[$domnode->nodeName][];
					if ($domnode->hasAttributes()) {
						$attributes = $domnode->attributes;
						foreach ($attributes as $index => $domobj) {
							$array_ptr[$domobj->name] = $domobj->value;
						}
					}
					break;
			}
			if ($domnode->hasChildNodes()) {
				$this->readXml($domnode, $array_ptr);
			}
			$domnode = $domnode->nextSibling;
		}
	}

	function responseXML($url = '', $post_data = '', $parse = false) {
		$ch = curl_init(); //initiate the curl session
		curl_setopt($ch, CURLOPT_URL, $url); //set to url to post to
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // tell curl to return data ina variable
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); // post the xml
		curl_setopt($ch, CURLOPT_TIMEOUT, SERVICE_TIMEOUT); // set timeout in seconds
		$xmlResponse = curl_exec($ch);
		if (curl_errno($ch)) {
			//			print curl_error($ch);
			return false;
		} else {
			curl_close($ch);
		}

		if (!$parse) {
			return $xmlResponse;
		} else {
			$aData = array ();
			if (trim($xmlResponse) != "") {
				$dom = new DOMDocument;
				$dom->loadXML($xmlResponse);
				$this->readXml($dom, $aData);
			}
			return $aData;
		}
	}
	function header_responseXML($url = '', $post_data = '', $parse = false) {
		$ch = curl_init(); //initiate the curl session
		curl_setopt($ch, CURLOPT_URL, $url); //set to url to post to
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // tell curl to return data ina variable
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array (
			"Content-Type: text/xml; charset=UTF-8"
		));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); // post the xml
		curl_setopt($ch, CURLOPT_TIMEOUT, SERVICE_TIMEOUT); // set timeout in seconds
		$xmlResponse = curl_exec($ch);
		if (curl_errno($ch)) {
			//			print curl_error($ch);
			return false;
		} else {
			curl_close($ch);
		}

		if (!$parse) {
			return $xmlResponse;
		} else {
			$aData = array ();
			if (trim($xmlResponse) != "") {
				$dom = new DOMDocument;
				$dom->loadXML($xmlResponse);
				$this->readXml($dom, $aData);
			}
			return $aData;
		}
	}
	/**************************************************/
	/*************2010-06-14 Editer:Prang**************/
	/***** Add function to debug register function*****/
	/**************************************************/
	function responseXML2($url = '', $post_data = '', $parse = false) {
		$ch = curl_init(); //initiate the curl session
		curl_setopt($ch, CURLOPT_URL, $url); //set to url to post to
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // tell curl to return data ina variable
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); // post the xml
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // set timeout in seconds
		$xmlResponse = curl_exec($ch);

		if (curl_errno($ch)) {
			return false;
		} else {
			curl_close($ch);
		}
		$this->writelog($url . "?" . $post_data." \r\n::::".$xmlResponse);
		if (!$parse) {
			return $xmlResponse;
		} else {
			$aData = array ();
			if (trim($xmlResponse) != "") {
				$dom = new DOMDocument;
				$dom->loadXML($xmlResponse);
				$this->readXml($dom, $aData);
			}
			return $aData;
		}
	}
	function get_responseXML2($url, $parse = false) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$xmlResponse = curl_exec($ch);

		if (curl_errno($ch)) {
			return false;
		} else {
			curl_close($ch);
		}
		$this->writelog($url ." \r\n::::".$xmlResponse);

		if (!$parse) {
			return $xmlResponse;
		} else {
			$aData = array ();
			if (trim($xmlResponse) != "") {
				$dom = new DOMDocument();
				$dom->loadXML(trim($xmlResponse));
				$this->readXml($dom, $aData);
			}
			return $aData;
		}

	}
	function writelog($msg){
		$today   = date("Y-m-d");
		$today = $today;
		$filepath = '/app/log/truelife/new/xmljson_'.$today.'.txt';
		$date_time   = date("H:i:s");
		$date_time=$date_time;
		$logFile = fopen($filepath, 'a+');
		fputs($logFile, $date_time."  ,msg:".$msg." \r\n");
		fclose($logFile);
		return true;
	}
	/**************************************************/
	/*************2010-06-14 End***********************/
	/**************************************************/

	function get_responseXML($url, $parse = false) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$xmlResponse = curl_exec($ch);

		if (curl_errno($ch)) {
			print curl_error($ch);
		} else {
			curl_close($ch);
		}

		if (!$parse) {
			return $xmlResponse;
		} else {
			$aData = array ();
			if (trim($xmlResponse) != "") {
				$dom = new DOMDocument();
				$dom->loadXML(trim($xmlResponse));
				$this->readXml($dom, $aData);
			}
			return $aData;
		}
	}

	function responseXML_mtget($url, $parse = false) {
		try {
			$xmlResponse = $this->CI->memcache->get($url);
			if (empty ($xmlResponse)) {
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				$xmlResponse = curl_exec($ch);

				if (curl_errno($ch)) {
					return false;
				} else {
					curl_close($ch);
				}
				$this->CI->memcache->set($url, $xmlResponse);
			}

			if (!$parse) {
				return $xmlResponse;
			} else {
				$aData = array ();
				if (trim($xmlResponse) != "") {
					$dom = new DOMDocument();
					$dom->loadXML(trim($xmlResponse));
					$this->readXml($dom, $aData);
				}
				return $aData;
			}
		} catch (Exception $e) {
		}
		return false;
	}

	function responseSimpleXML_mtget($url, $parse = false) {
		try {
			$xmlResponse = $this->CI->memcache->get($url);
			if (empty ($xmlResponse)) {
//				echo "empty";
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, SERVICE_TIMEOUT);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				$xmlResponse = curl_exec($ch);

				if (curl_errno($ch)) {
					return false;
				} else {
					curl_close($ch);
				}
				$this->CI->memcache->set($url, $xmlResponse);
			}

			if (!$parse) {
				return $xmlResponse;
			} else {
				if ($xmlResponse) {
					return new SimpleXMLElement($xmlResponse);
				} else {
					return false;
				}
			}

		} catch (Exception $e) {
		}
		return false;
	}
}
?>
