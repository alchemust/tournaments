<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "main";
$route['404_override'] = '';
$route['blackend/add_tournament_process'] = "tournament/add_tournament_process";
$route['teamlist/(:num)'] = "tournament/teamlist/$1";
$route['page/(:num)'] = "main/index/$1";

$route['team-detail/(:num)'] = "tournament/teamdetail/$1";
$route['match_detail/(:num)'] = "matches/match_detail/$1";
$route['match/update_score'] = "matches/update_score";
$route['match/post_comment'] = "matches/post_comment";
$route['schedule/(:num)'] = "tournament/schedule/$1";
$route['blackend/logs/(:num)'] = "app_logs/index/$1";
$route['profile_info'] = "users/profile_info";
$route['edit_profile_info'] = "users/edit_profile";
$route['team/(:num)'] = "team/team_info/$1";
$route['team_info/(:num)'] = "team/team_info_iframe/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */