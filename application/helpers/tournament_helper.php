<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function team_star($point) {
		$star_img = "";
		$point = intval($point);
		$star = 1;
		if($point <= 100) {
			$star = 1;
		} else if($point > 100 && $point <= 500) {
			$star = 2;
		} else if($point > 500 && $point <= 1000)  {
			$star = 3;
		} else if($point > 1000 && $point <= 2000)  {
			$star = 4;
		} else if($point > 2000)  {
			$star = 5;
		}
		
		$i = 0; while($i < $star) { $star_img .='<img src="'.other_asset_url('fps-icon2.png', 'frontend', 'images').'" alt="" />'; $i++; } 
		
		return $star_img;
	}

?>