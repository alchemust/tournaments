<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_frontend extends CI_Model {

	function __construct()
	{
		parent::__construct();

		//LOAD DB DEFAULT
		$this->load->model('team/m_team');
		$this->load->model('tournament/m_tournament');
		$this->load->model('news/m_news');
	}

	function sidebar_info()
	{
		$data_sidebar = array();
		
		if ($this->m_authen->is_logged_in() === TRUE) {
			$user_id = $this->m_authen->getsession("user_id");
			$user_type = $this->m_authen->getsession("user_type");
			$team_detail = $this->m_team->load_team_detail($user_id);
			if($this->m_authen->getsession("user_type") == 1) {
				$data_sidebar = array(
					"login" => 1,
					"user_id" => $this->m_authen->getsession("user_id"),
					"user_name" => $this->m_authen->getsession("user_login"),
					"team_name" => "",
					"team_id" => 0,
					"team_logo" => "/assets/modules/frontend/images/no_images.jpg",
					"team_win" => "",
					"team_lose" => "",
					"team_point" => "",
					"tour_join" => array(),
					"team_active" => $this->m_authen->getsession("team_active"),
					"user_type" => $user_type
				);
				
			} else {
				$team_logo ="/assets/modules/frontend/images/no_images.jpg";
				$data_sidebar["login"] = 1;
				$data_sidebar["user_id"] = $this->m_authen->getsession("user_id");
				$data_sidebar["user_name"] = $this->m_authen->getsession("user_login");
				$data_sidebar["team_active"] = $this->m_authen->getsession("team_active");
				$data_sidebar["team_logo"] = $team_logo;
				$data_sidebar["tour_join"] = array();
				$data_sidebar["team_id"] = 0;
				$data_sidebar["user_type"] = $user_type;
				
				if($team_detail) {
					$team_join = ($team_detail["tour_join"]) ? $team_detail["tour_join"] : array();
					$team_logo = ($team_detail["team_logo"]) ? IMGPATH_URL."/".$team_detail["team_logo"] : "/assets/modules/frontend/images/no_images.jpg";
					$data_sidebar["team_name"] = $team_detail["team_name"];
					$data_sidebar["team_id"] = $team_detail["team_id"];
					$data_sidebar["team_logo"] = $team_logo;
					$data_sidebar["team_win"] = $team_detail["team_win"];
					$data_sidebar["team_lose"] = $team_detail["team_lose"];
					$data_sidebar["team_point"] = $team_detail["team_point"];
					$data_sidebar["tour_join"] = $team_join;
				}
			}
		} else {
			$data_sidebar["login"] = 0;
			$data_sidebar["user_id"] = "";
			$data_sidebar["user_name"] = "";
			$data_sidebar["team_id"] = 0;
			$data_sidebar["team_name"] = "";
			$data_sidebar["team_logo"] = "";
			$data_sidebar["team_active"] = 0;
			$data_sidebar["tour_join"] = "";
			$data_sidebar["user_type"] = 0;
		}
		$data_sidebar["news_list"] = $this->m_news->news_list();
		$data_sidebar["team_rankings"] = $this->m_team->team_rankings_list();
		$data_sidebar["hot_tour"] = $this->m_tournament->tournament_sidebar_list();
		
		
		return $data_sidebar;
	}

	function process()
	{
		/*
		$username = trim($this->input->post('login_user'));
		$password = trim($this->input->post('login_pass'));
		log_message('info',$username." ".$password);
		
		$sql ="SELECT user_id,user_login,user_pass, display_name FROM tbl_users WHERE user_login = '".$username ."' AND user_pass = '".$password."' AND user_status='1'";
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			$this->authorize($query->row_array());
		}else {
			echo 0;
		}
		*/
	}

	function authorize($row="")
	{
		$this->load->library('session');
		list($expire, $domain, $path, $prefix) = $this->m_authen->initcookie();

		$array['id'] = $row['user_id'];
		$array['user_login'] = $row['user_login'];
		$array['display_name'] = $row['display_name'];
		//$array['key'] = mb5('ck'.$row['']);

		$this->m_authen->setcookies('authorized', json_encode($array));

		//set session
		$admindata = array(
                   'id'  => $row['user_id'],
                   'user_login'     => $row['user_login'],
                   'display_name'     => $row['display_name'],
                   'logged_in' => TRUE
		);

		$this->session->set_userdata($admindata);
		echo 1;
	}

}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */