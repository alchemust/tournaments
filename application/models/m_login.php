<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_login extends CI_Model {

	function __construct()
	{
		parent::__construct();

		//LOAD DB DEFAULT
		$this->load->library('Utility');
		$this->load->database('default');
	}

	function index()
	{
		$this->validation();
	}

	function validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login_user', '', 'trim|required|xss_clean');
		$this->form_validation->set_rules('login_pass', '', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			echo 0;
		}
		else
		{
			$this->process();
		}
	}

	function process()
	{
		/*
		$username = trim($this->input->post('login_user'));
		$password = trim($this->input->post('login_pass'));
		log_message('info',$username." ".$password);
		
		$sql ="SELECT user_id,user_login,user_pass, display_name FROM tbl_users WHERE user_login = '".$username ."' AND user_pass = '".$password."' AND user_status='1'";
		$query = $this->db->query($sql);
		$num_row = $query->num_rows();
		if($num_row>0)
		{
			$this->authorize($query->row_array());
		}else {
			echo 0;
		}
		*/
	}

	function authorize($row="")
	{
		$this->load->library('session');
		list($expire, $domain, $path, $prefix) = $this->m_authen->initcookie();

		$array['id'] = $row['user_id'];
		$array['user_login'] = $row['user_login'];
		$array['display_name'] = $row['display_name'];
		//$array['key'] = mb5('ck'.$row['']);

		$this->m_authen->setcookies('authorized', json_encode($array));

		//set session
		$admindata = array(
                   'id'  => $row['user_id'],
                   'user_login'     => $row['user_login'],
                   'display_name'     => $row['display_name'],
                   'logged_in' => TRUE
		);

		$this->session->set_userdata($admindata);
		echo 1;
	}

}

/* End of file main.php */
/* Location: ./application/modules/models/main.php */