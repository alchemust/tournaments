<div class="span3 sidebar left-sidebar">
	 <?php if(!$login){?>
    <div id="login-box">
        <span class="header-box">Torunament Login</span>
    	<form id="login_sidebar_form" name="login_sidebar_form" method="post" >
        
          <fieldset>
            
            <label>User</label>
            <input type="text" name="login_user" id="login_user" placeholder="Username" class="span2 input-medium input-login">	
            <label>Password</label>
            <input type="Password" name="login_pass" id="login_pass" placeholder="Password" class="span2 input-medium input-login">
            <!--<label class="checkbox">
              <input type="checkbox"> Remember 
            </label>-->
            <button data-loading-text="Loading..." class="btn btn-primary btn-mini button-loading" id="submit" type="submit" style="margin-bottom:10px;">Sign in</button>
            <label><img width="8" height="8" alt="fps-icon" src="<?=other_asset_url('fps-icon.png', 'frontend', 'images')?>"> ลืมรหัสผ่าน</label>
          </fieldset>
        </form>
    </div>
    <?php } ?>
    <?php if($login){?>
    <div id="user-profile-box">
        <span class="header-box">User Profile</span>
        <div class="media">
            <a class="pull-left" href="#"> 
                <img src="<?php echo $team_logo;?>" width="50" height="50" class="img-rounded">
              </a>
            <div class="media-body">
            	
                <h5 class="media-heading"><?php echo (isset($team_name)) ? $team_name : "";?></h5>
                <p><i class="icon-user icon-white"></i> <?php echo $user_name;?></p>
                <p><i class="icon-star icon-white"></i></p>
            </div>
            
        </div>
        <div class="span2" style="margin-top:10px;">
        	<?php if($team_active) { ?>
           <ul class="unstyled">
              <li>win : <?php echo $team_win;?></li>
              <li>Lose : <?php echo $team_lose;?></li>
              <li>Total Point : <?php echo $team_point;?></li>
            </ul>
            
            <button type="button" id="btn-profile" name="btn-profile" class="btn btn-primary btn-mini button-loading" style="margin-bottom:5px;">ข้อมูลส่วนตัว&nbsp;</button>

            <?php } else { ?>
             <label><a href="/register/form" target="_self" title="สร้างทีม" class="" ><img width="8" height="8" alt="fps-icon" src="<?=other_asset_url('fps-icon.png', 'frontend', 'images')?>"> สร้างทีม</a></label>

            <?php } ?>
            <button type="button" id="btn-logout" name="btn-logout" class="btn btn-danger btn-mini button-loading">ออกจากระบบ</button>
            
        </div>
        
    </div>
    <?php } ?>
   
   	<?php //if($news_list) { ?>
    <!--<div id="tour-news-box">
        <span class="header-box">Tournament News</span>
        <div class="news-list">
            <ul class="unstyled">
              <?php foreach($news_list as $news) {?>
              <li><img width="8" height="8" alt="fps-icon" src="<?=other_asset_url('fps-icon.png', 'frontend', 'images')?>"><a href="<?php echo $news->news_link;?>" title="" target="_blank"><?php echo $news->news_title;?></a></li>
              <?php } ?>
            </ul>
        </div>
        
    </div>-->
    <?php //} ?>
    
    <?php if($team_rankings) {?>
    <div id="team-rankings-box">
        <span class="header-box">Team Rankings</span>
        <div class="span2 ranking-list">
            <ol>
             <?php foreach($team_rankings as $rankings) { ?>
              <li><a href="/team/<?php echo $rankings->team_id;?>" title="<?php echo $rankings->team_name;?>"><?php echo $rankings->team_name;?></a></li>
              <?php } ?>
            </ol>
        </div>
    </div>
    <?php } ?>
    
     <?php if($hot_tour) {?>
    <div id="hot-tour-box">
        <span class="header-box">Hot Tournaments</span>
        <div class="span2 tour-list">
            <ol>
            	<?php foreach($hot_tour as $hot){ ?>
              <li><a href="/tournament/detail/<?php echo $hot->tour_id;?>" title="<?php echo $hot->tour_title; ?>"><?php echo $hot->tour_title; ?></a> <span class="label label-warning"><?php echo $hot->tour_team_num; ?></span></li>
              <?php } ?>
            </ol>
        </div>
    </div>
    <?php } ?>
 </div>