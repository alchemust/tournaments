<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Fps Thailand, เกาะติดสถานการณ์และวงการเกม FPS ของเมืองไทย</title>
<meta name="robots" content="nofollow,noindex" /> 
<meta name="description" content="เวบไซต์ที่รวบรวม ข่าวสารวงการเกมfps อุปกรณ์เล่นเกม คลิบการแข่งขัน ผู้เล่นระดับแนวหน้า แคลนดัง เทคนิคการเล่นเกม"> 
<meta name="keywords" content="fps, fps thailand, first person shooting, esport, e-sports, esports, counter strike, sudden attack, special force, point blank, sa, sf, pb, cso, cs, game online, ava , battery , เกมส์, เกมส์ต่อสู้, เกมส์ยิงปืน"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://fpsthailand.com/favicon.ico" > 
    <?=css_asset('bootstrap.css', 'frontend')?>
    <?=css_asset('main.css', 'frontend')?>
    <? //js_asset('jquery.min.js', 'frontend'); ?>
    <?=js_asset('jquery-1.8.2.min.js', 'frontend'); ?>
     <?=js_asset('bootstrap.js', 'frontend'); ?>
      <?=js_asset('jquery.validate.min.js', 'frontend'); ?>
    <?=js_asset('scripts.js', 'frontend'); ?>   
	<?=js_asset('jquery.fancybox.js','frontend'); ?>
	<?=css_asset('jquery.fancybox.css','frontend')?>
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>
<body>
<div id="overlay" style="display: none;"></div>

<div id="wrap-pop" style="display:none;">
    <div id="login" style="display: none;">
        <div class="loading-box">
        <p>Please wait ...</p>
        <p><img src="<?=other_asset_url('loader-bar.gif', 'backend', 'images')?>"></p>
        </div>
    </div>
</div>  
<div class="WrapBoardFPS">
    
    <div id="wrapBoardInside">
    	<div class="outside">
        	<div class="top-left"></div>
            <div class="top-center"></div>
            <div class="top-right"></div>
        	<div class="inside">
            	<div class="notopgap">
                    <div id="wrap">
                        <div id="header" >
                            <div id="page-header">
                                <div class="headerbar2">
                                    <div class="inner"></div>
                        
                                    <div id="site-description" style="padding-right:20px;">
                                    <a href="http://www.fpsthailand.com/index.htm" title="fpsthailand.com">
                                    <img src="<?=other_asset_url('logo.jpg', 'frontend', 'images')?>">
                                    </a></div>
                                
                                </div>
                            </div>
                       </div>
                       <div class="navibar">
                        <div class="inner"><span class="corners-top"><span></span></span>
                            <ul class="linklist navlinks">
                            
                                <li class="home-header-icon"><label><a href="/main" title="หน้าแรกทัวร์นาเมนต์" >หน้าแรกทัวร์นาเมนต์</a></label> </li>
                
                            </ul>
                             
                                <ul class="linklist rightside">
                                <li class="faq-header-icon"><label> FAQ </label></li>
                                
                                </ul>
                               
                            <span class="corners-bottom"><span></span></span></div>
                        </div>
                        
                        
                       <div class="row">
                   