2013-07-28 13:18:56|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.start_tournament|create_challonge_tournament|2|tour_id:5,challonge_tour_id:567658,chalonge_name:Gview Special Force by FPSThailand #5,chalonge_url:fps_tour_5,full_challonge_url:http://challonge.com/fps_tour_5,live_image_url:http://images.challonge.com/fps_tour_5.png,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<tournament>
  <accept-attachments type="boolean">false</accept-attachments>
  <allow-participant-match-reporting type="boolean">true</allow-participant-match-reporting>
  <anonymous-voting type="boolean">false</anonymous-voting>
  <category nil="true"/>
  <check-in-at type="datetime" nil="true"/>
  <completed-at type="datetime" nil="true"/>
  <created-at type="datetime">2013-07-28T14:16:00-04:00</created-at>
  <created-by-api type="boolean">true</created-by-api>
  <credit-capped type="boolean">false</credit-capped>
  <description>Gview Special Force by FPSThailand #5</description>
  <enable-group-stage type="boolean">false</enable-group-stage>
  <game-id type="integer" nil="true"/>
  <hide-forum type="boolean">false</hide-forum>
  <hide-seeds type="boolean">false</hide-seeds>
  <hold-third-place-match type="boolean">false</hold-third-place-match>
  <id type="integer">567658</id>
  <max-predictions-per-user type="integer">1</max-predictions-per-user>
  <name>Gview Special Force by FPSThailand #5</name>
  <notify-users-when-matches-open type="boolean">true</notify-users-when-matches-open>
  <notify-users-when-the-tournament-ends type="boolean">true</notify-users-when-the-tournament-ends>
  <open-signup type="boolean">false</open-signup>
  <participant-count-to-advance-per-group type="integer" nil="true"/>
  <participants-count type="integer">0</participants-count>
  <prediction-method type="integer">0</prediction-method>
  <predictions-opened-at type="datetime" nil="true"/>
  <private type="boolean">false</private>
  <progress-meter type="integer">0</progress-meter>
  <pts-for-bye type="decimal">1.0</pts-for-bye>
  <pts-for-game-tie type="decimal">0.0</pts-for-game-tie>
  <pts-for-game-win type="decimal">0.0</pts-for-game-win>
  <pts-for-match-tie type="decimal">0.5</pts-for-match-tie>
  <pts-for-match-win type="decimal">1.0</pts-for-match-win>
  <ranked-by nil="true"/>
  <require-score-agreement type="boolean">false</require-score-agreement>
  <round-labels nil="true"/>
  <rr-pts-for-game-tie type="decimal">0.0</rr-pts-for-game-tie>
  <rr-pts-for-game-win type="decimal">0.0</rr-pts-for-game-win>
  <rr-pts-for-match-tie type="decimal">0.5</rr-pts-for-match-tie>
  <rr-pts-for-match-win type="decimal">1.0</rr-pts-for-match-win>
  <second-place-id type="integer" nil="true"/>
  <sequential-pairings type="boolean">false</sequential-pairings>
  <show-rounds type="boolean">false</show-rounds>
  <signup-cap type="integer" nil="true"/>
  <signup-requires-account type="boolean">false</signup-requires-account>
  <started-at type="datetime" nil="true"/>
  <state>pending</state>
  <swiss-rounds type="integer">0</swiss-rounds>
  <third-place-id type="integer" nil="true"/>
  <tournament-type>single elimination</tournament-type>
  <updated-at type="datetime">2013-07-28T14:16:00-04:00</updated-at>
  <url>fps_tour_5</url>
  <winner-id type="integer" nil="true"/>
  <description-source>Gview Special Force by FPSThailand #5</description-source>
  <subdomain nil="true"/>
  <full-challonge-url>http://challonge.com/fps_tour_5</full-challonge-url>
  <live-image-url>http://images.challonge.com/fps_tour_5.png</live-image-url>
  <sign-up-url nil="true"/>
  <review-before-finalizing type="boolean">true</review-before-finalizing>
</tournament>

2013-07-28 13:18:56|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.start_tournament|update_tournament_table|2|tour_id:5,challonge_tour_id:567658,chalonge_url:fps_tour_5
2013-07-28 13:18:58|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:5,participant_id:8864013,participant_name:James Sarawut Team,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:02-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864013</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>James Sarawut Team</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">1</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:02-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:00|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:6,participant_id:8864015,participant_name:OKC Team,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:04-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864015</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>OKC Team</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">2</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:04-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:02|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:7,participant_id:8864016,participant_name:100-INSPIRATION,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:06-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864016</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>100-INSPIRATION</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">3</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:06-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:05|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:8,participant_id:8864017,participant_name:2BTAP,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:09-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864017</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>2BTAP</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">4</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:09-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:07|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:9,participant_id:8864019,participant_name:AccomPLISH,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:11-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864019</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>AccomPLISH</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">5</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:11-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:09|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:10,participant_id:8864020,participant_name:aMiTyeSport,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T11:16:13-07:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864020</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>aMiTyeSport</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">6</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T11:16:13-07:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:11|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:11,participant_id:8864021,participant_name:Axis eSport,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:14-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864021</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>Axis eSport</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">7</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:14-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:12|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:12,participant_id:8864023,participant_name:Axis eSport.Academy,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:15-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864023</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>Axis eSport.Academy</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">8</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:15-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:13|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:13,participant_id:8864025,participant_name:COSMIS-,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:17-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864025</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>COSMIS-</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">9</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:17-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:17|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:14,participant_id:8864026,participant_name:Crazy E-sport,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:21-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864026</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>Crazy E-sport</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">10</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:21-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:19|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:15,participant_id:8864027,participant_name:don'tSTOP,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:22-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864027</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>don'tSTOP</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">11</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:22-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:21|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:5,challonge_tour_id:567658,team_id:16,participant_id:8864028,participant_name:Dynamite-,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-28T14:16:25-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8864028</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>Dynamite-</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">12</seed>
  <tournament-id type="integer">567658</tournament-id>
  <updated-at type="datetime">2013-07-28T14:16:25-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-28 13:19:23|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.start_challonge_tour|Start_Challonge_Tournament|2|tour_id:5,challonge_tour_id:567658,state:underway,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<tournament>
  <accept-attachments type="boolean">false</accept-attachments>
  <allow-participant-match-reporting type="boolean">true</allow-participant-match-reporting>
  <anonymous-voting type="boolean">false</anonymous-voting>
  <category nil="true"/>
  <check-in-at type="datetime" nil="true"/>
  <completed-at type="datetime" nil="true"/>
  <created-at type="datetime">2013-07-28T14:16:00-04:00</created-at>
  <created-by-api type="boolean">true</created-by-api>
  <credit-capped type="boolean">false</credit-capped>
  <description>Gview Special Force by FPSThailand #5</description>
  <enable-group-stage type="boolean">false</enable-group-stage>
  <game-id type="integer" nil="true"/>
  <hide-forum type="boolean">false</hide-forum>
  <hide-seeds type="boolean">false</hide-seeds>
  <hold-third-place-match type="boolean">false</hold-third-place-match>
  <id type="integer">567658</id>
  <max-predictions-per-user type="integer">1</max-predictions-per-user>
  <name>Gview Special Force by FPSThailand #5</name>
  <notify-users-when-matches-open type="boolean">true</notify-users-when-matches-open>
  <notify-users-when-the-tournament-ends type="boolean">true</notify-users-when-the-tournament-ends>
  <open-signup type="boolean">false</open-signup>
  <participant-count-to-advance-per-group type="integer" nil="true"/>
  <participants-count type="integer">12</participants-count>
  <prediction-method type="integer">0</prediction-method>
  <predictions-opened-at type="datetime" nil="true"/>
  <private type="boolean">false</private>
  <progress-meter type="integer">0</progress-meter>
  <pts-for-bye type="decimal">1.0</pts-for-bye>
  <pts-for-game-tie type="decimal">0.0</pts-for-game-tie>
  <pts-for-game-win type="decimal">0.0</pts-for-game-win>
  <pts-for-match-tie type="decimal">0.5</pts-for-match-tie>
  <pts-for-match-win type="decimal">1.0</pts-for-match-win>
  <ranked-by nil="true"/>
  <require-score-agreement type="boolean">false</require-score-agreement>
  <round-labels nil="true"/>
  <rr-pts-for-game-tie type="decimal">0.0</rr-pts-for-game-tie>
  <rr-pts-for-game-win type="decimal">0.0</rr-pts-for-game-win>
  <rr-pts-for-match-tie type="decimal">0.5</rr-pts-for-match-tie>
  <rr-pts-for-match-win type="decimal">1.0</rr-pts-for-match-win>
  <second-place-id type="integer" nil="true"/>
  <sequential-pairings type="boolean">false</sequential-pairings>
  <show-rounds type="boolean">false</show-rounds>
  <signup-cap type="integer" nil="true"/>
  <signup-requires-account type="boolean">false</signup-requires-account>
  <started-at type="datetime">2013-07-28T14:16:26-04:00</started-at>
  <state>underway</state>
  <swiss-rounds type="integer">0</swiss-rounds>
  <third-place-id type="integer" nil="true"/>
  <tournament-type>single elimination</tournament-type>
  <updated-at type="datetime">2013-07-28T14:16:27-04:00</updated-at>
  <url>fps_tour_5</url>
  <winner-id type="integer" nil="true"/>
  <description-source>Gview Special Force by FPSThailand #5</description-source>
  <subdomain nil="true"/>
  <full-challonge-url>http://challonge.com/fps_tour_5</full-challonge-url>
  <live-image-url>http://images.challonge.com/fps_tour_5.png</live-image-url>
  <sign-up-url nil="true"/>
  <review-before-finalizing type="boolean">true</review-before-finalizing>
</tournament>

2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|get_challonge_matches|2|tour_id:5,challonge_tour_id:567658,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<matches type="array">
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845847</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864019</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer">8864028</player2-id>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer" nil="true"/>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">1</round>
    <started-at type="datetime">2013-07-28T14:16:26-04:00</started-at>
    <state>open</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv/>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845848</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864020</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer">8864027</player2-id>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer" nil="true"/>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">1</round>
    <started-at type="datetime">2013-07-28T14:16:27-04:00</started-at>
    <state>open</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:27-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv/>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845849</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864021</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer">8864026</player2-id>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer" nil="true"/>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">1</round>
    <started-at type="datetime">2013-07-28T14:16:27-04:00</started-at>
    <state>open</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:27-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv/>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845850</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864023</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer">8864025</player2-id>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer" nil="true"/>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">1</round>
    <started-at type="datetime">2013-07-28T14:16:27-04:00</started-at>
    <state>open</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:27-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv/>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845851</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864013</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer" nil="true"/>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer">12845850</player2-prereq-match-id>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">2</round>
    <started-at type="datetime" nil="true"/>
    <state>pending</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv>12845850</prerequisite-match-ids-csv>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845852</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864015</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer" nil="true"/>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer">12845849</player2-prereq-match-id>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">2</round>
    <started-at type="datetime" nil="true"/>
    <state>pending</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv>12845849</prerequisite-match-ids-csv>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845853</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864016</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer" nil="true"/>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer">12845848</player2-prereq-match-id>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">2</round>
    <started-at type="datetime" nil="true"/>
    <state>pending</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv>12845848</prerequisite-match-ids-csv>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845854</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8864017</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer" nil="true"/>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer">12845847</player2-prereq-match-id>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">2</round>
    <started-at type="datetime" nil="true"/>
    <state>pending</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv>12845847</prerequisite-match-ids-csv>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845855</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer" nil="true"/>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer">12845851</player1-prereq-match-id>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer" nil="true"/>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer">12845854</player2-prereq-match-id>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">3</round>
    <started-at type="datetime" nil="true"/>
    <state>pending</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv>12845851,12845854</prerequisite-match-ids-csv>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845856</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer" nil="true"/>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer">12845852</player1-prereq-match-id>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer" nil="true"/>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer">12845853</player2-prereq-match-id>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">3</round>
    <started-at type="datetime" nil="true"/>
    <state>pending</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv>12845852,12845853</prerequisite-match-ids-csv>
    <scores-csv/>
  </match>
  <match>
    <attachment-count type="integer" nil="true"/>
    <created-at type="datetime">2013-07-28T14:16:26-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12845857</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer" nil="true"/>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer">12845855</player1-prereq-match-id>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer" nil="true"/>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer">12845856</player2-prereq-match-id>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">4</round>
    <started-at type="datetime" nil="true"/>
    <state>pending</state>
    <tournament-id type="integer">567658</tournament-id>
    <updated-at type="datetime">2013-07-28T14:16:26-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv>12845855,12845856</prerequisite-match-ids-csv>
    <scores-csv/>
  </match>
</matches>

2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:8,detail:12845847567658588640198864028001open2013-07-28T14:16:26-04:002013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:9,detail:12845848567658588640208864027001open2013-07-28T14:16:27-04:002013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:10,detail:12845849567658588640218864026001open2013-07-28T14:16:27-04:002013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:11,detail:12845850567658588640238864025001open2013-07-28T14:16:27-04:002013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:12,detail:12845851567658588640130002pending2013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:13,detail:12845852567658588640150002pending2013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:14,detail:12845853567658588640160002pending2013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:15,detail:12845854567658588640170002pending2013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:16,detail:12845855567658500003pending2013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:17,detail:12845856567658500003pending2013-07-28 13:19:23
2013-07-28 13:19:25|f8f11718eb05190e7492b088dd3751c1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:5,challonge_tour_id:567658,challonge_match_id:18,detail:12845857567658500004pending2013-07-28 13:19:23
