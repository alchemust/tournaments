2013-07-23 11:07:14|87e879eb03cdf45304e7e0023d92dca1|1||m_tournament.start_tournament|create_challonge_tournament|2|tour_id:4,challonge_tour_id:561036,chalonge_name:test tournament #1,chalonge_url:fps_tour_4,full_challonge_url:http://challonge.com/fps_tour_4,live_image_url:http://images.challonge.com/fps_tour_4.png,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<tournament>
  <accept-attachments type="boolean">false</accept-attachments>
  <allow-participant-match-reporting type="boolean">true</allow-participant-match-reporting>
  <anonymous-voting type="boolean">false</anonymous-voting>
  <category nil="true"/>
  <check-in-at type="datetime" nil="true"/>
  <completed-at type="datetime" nil="true"/>
  <created-at type="datetime">2013-07-23T19:04:29+03:00</created-at>
  <created-by-api type="boolean">true</created-by-api>
  <credit-capped type="boolean">false</credit-capped>
  <description>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description>
  <enable-group-stage type="boolean">false</enable-group-stage>
  <game-id type="integer" nil="true"/>
  <hide-forum type="boolean">false</hide-forum>
  <hide-seeds type="boolean">false</hide-seeds>
  <hold-third-place-match type="boolean">false</hold-third-place-match>
  <id type="integer">561036</id>
  <max-predictions-per-user type="integer">1</max-predictions-per-user>
  <name>test tournament #1</name>
  <notify-users-when-matches-open type="boolean">true</notify-users-when-matches-open>
  <notify-users-when-the-tournament-ends type="boolean">true</notify-users-when-the-tournament-ends>
  <open-signup type="boolean">false</open-signup>
  <participant-count-to-advance-per-group type="integer" nil="true"/>
  <participants-count type="integer">0</participants-count>
  <prediction-method type="integer">0</prediction-method>
  <predictions-opened-at type="datetime" nil="true"/>
  <private type="boolean">false</private>
  <progress-meter type="integer">0</progress-meter>
  <pts-for-bye type="decimal">1.0</pts-for-bye>
  <pts-for-game-tie type="decimal">0.0</pts-for-game-tie>
  <pts-for-game-win type="decimal">0.0</pts-for-game-win>
  <pts-for-match-tie type="decimal">0.5</pts-for-match-tie>
  <pts-for-match-win type="decimal">1.0</pts-for-match-win>
  <ranked-by nil="true"/>
  <require-score-agreement type="boolean">false</require-score-agreement>
  <round-labels nil="true"/>
  <rr-pts-for-game-tie type="decimal">0.0</rr-pts-for-game-tie>
  <rr-pts-for-game-win type="decimal">0.0</rr-pts-for-game-win>
  <rr-pts-for-match-tie type="decimal">0.5</rr-pts-for-match-tie>
  <rr-pts-for-match-win type="decimal">1.0</rr-pts-for-match-win>
  <second-place-id type="integer" nil="true"/>
  <sequential-pairings type="boolean">false</sequential-pairings>
  <show-rounds type="boolean">false</show-rounds>
  <signup-cap type="integer" nil="true"/>
  <signup-requires-account type="boolean">false</signup-requires-account>
  <started-at type="datetime" nil="true"/>
  <state>pending</state>
  <swiss-rounds type="integer">0</swiss-rounds>
  <third-place-id type="integer" nil="true"/>
  <tournament-type>single elimination</tournament-type>
  <updated-at type="datetime">2013-07-23T19:04:29+03:00</updated-at>
  <url>fps_tour_4</url>
  <winner-id type="integer" nil="true"/>
  <description-source>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description-source>
  <subdomain nil="true"/>
  <full-challonge-url>http://challonge.com/fps_tour_4</full-challonge-url>
  <live-image-url>http://images.challonge.com/fps_tour_4.png</live-image-url>
  <sign-up-url nil="true"/>
  <review-before-finalizing type="boolean">true</review-before-finalizing>
</tournament>

2013-07-23 11:07:14|87e879eb03cdf45304e7e0023d92dca1|1||m_tournament.start_tournament|update_tournament_table|2|tour_id:4,challonge_tour_id:561036,chalonge_url:fps_tour_4
2013-07-23 11:07:17|87e879eb03cdf45304e7e0023d92dca1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:4,challonge_tour_id:561036,team_id:5,participant_id:8771884,participant_name:James Sarawut Team,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-23T19:04:31+03:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8771884</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>James Sarawut Team</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">1</seed>
  <tournament-id type="integer">561036</tournament-id>
  <updated-at type="datetime">2013-07-23T19:04:31+03:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-23 11:07:20|87e879eb03cdf45304e7e0023d92dca1|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:4,challonge_tour_id:561036,team_id:6,participant_id:8771885,participant_name:OKC Team,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-23T12:04:35-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8771885</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>OKC Team</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">2</seed>
  <tournament-id type="integer">561036</tournament-id>
  <updated-at type="datetime">2013-07-23T12:04:35-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-23 11:07:26|87e879eb03cdf45304e7e0023d92dca1|1||m_tournament.start_challonge_tour|Start_Challonge_Tournament|2|tour_id:4,challonge_tour_id:561036,state:underway,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<tournament>
  <accept-attachments type="boolean">false</accept-attachments>
  <allow-participant-match-reporting type="boolean">true</allow-participant-match-reporting>
  <anonymous-voting type="boolean">false</anonymous-voting>
  <category nil="true"/>
  <check-in-at type="datetime" nil="true"/>
  <completed-at type="datetime" nil="true"/>
  <created-at type="datetime">2013-07-23T11:04:29-05:00</created-at>
  <created-by-api type="boolean">true</created-by-api>
  <credit-capped type="boolean">false</credit-capped>
  <description>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description>
  <enable-group-stage type="boolean">false</enable-group-stage>
  <game-id type="integer" nil="true"/>
  <hide-forum type="boolean">false</hide-forum>
  <hide-seeds type="boolean">false</hide-seeds>
  <hold-third-place-match type="boolean">false</hold-third-place-match>
  <id type="integer">561036</id>
  <max-predictions-per-user type="integer">1</max-predictions-per-user>
  <name>test tournament #1</name>
  <notify-users-when-matches-open type="boolean">true</notify-users-when-matches-open>
  <notify-users-when-the-tournament-ends type="boolean">true</notify-users-when-the-tournament-ends>
  <open-signup type="boolean">false</open-signup>
  <participant-count-to-advance-per-group type="integer" nil="true"/>
  <participants-count type="integer">2</participants-count>
  <prediction-method type="integer">0</prediction-method>
  <predictions-opened-at type="datetime" nil="true"/>
  <private type="boolean">false</private>
  <progress-meter type="integer">0</progress-meter>
  <pts-for-bye type="decimal">1.0</pts-for-bye>
  <pts-for-game-tie type="decimal">0.0</pts-for-game-tie>
  <pts-for-game-win type="decimal">0.0</pts-for-game-win>
  <pts-for-match-tie type="decimal">0.5</pts-for-match-tie>
  <pts-for-match-win type="decimal">1.0</pts-for-match-win>
  <ranked-by nil="true"/>
  <require-score-agreement type="boolean">false</require-score-agreement>
  <round-labels nil="true"/>
  <rr-pts-for-game-tie type="decimal">0.0</rr-pts-for-game-tie>
  <rr-pts-for-game-win type="decimal">0.0</rr-pts-for-game-win>
  <rr-pts-for-match-tie type="decimal">0.5</rr-pts-for-match-tie>
  <rr-pts-for-match-win type="decimal">1.0</rr-pts-for-match-win>
  <second-place-id type="integer" nil="true"/>
  <sequential-pairings type="boolean">false</sequential-pairings>
  <show-rounds type="boolean">false</show-rounds>
  <signup-cap type="integer" nil="true"/>
  <signup-requires-account type="boolean">false</signup-requires-account>
  <started-at type="datetime">2013-07-23T11:04:41-05:00</started-at>
  <state>underway</state>
  <swiss-rounds type="integer">0</swiss-rounds>
  <third-place-id type="integer" nil="true"/>
  <tournament-type>single elimination</tournament-type>
  <updated-at type="datetime">2013-07-23T11:04:41-05:00</updated-at>
  <url>fps_tour_4</url>
  <winner-id type="integer" nil="true"/>
  <description-source>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description-source>
  <subdomain nil="true"/>
  <full-challonge-url>http://challonge.com/fps_tour_4</full-challonge-url>
  <live-image-url>http://images.challonge.com/fps_tour_4.png</live-image-url>
  <sign-up-url nil="true"/>
  <review-before-finalizing type="boolean">true</review-before-finalizing>
</tournament>

2013-07-23 11:07:27|87e879eb03cdf45304e7e0023d92dca1|1||m_tournament.get_challonge_matches|get_challonge_matches|2|tour_id:4,challonge_tour_id:561036,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<matches type="array">
  <match>
    <created-at type="datetime">2013-07-23T12:04:41-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12708410</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8771884</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer">8771885</player2-id>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer" nil="true"/>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">1</round>
    <started-at type="datetime">2013-07-23T12:04:41-04:00</started-at>
    <state>open</state>
    <tournament-id type="integer">561036</tournament-id>
    <updated-at type="datetime">2013-07-23T12:04:41-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv/>
    <scores-csv/>
  </match>
</matches>

2013-07-23 11:07:27|87e879eb03cdf45304e7e0023d92dca1|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:4,challonge_tour_id:561036,challonge_match_id:2,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<matches type="array">
  <match>
    <created-at type="datetime">2013-07-23T12:04:41-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12708410</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8771884</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer">8771885</player2-id>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer" nil="true"/>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">1</round>
    <started-at type="datetime">2013-07-23T12:04:41-04:00</started-at>
    <state>open</state>
    <tournament-id type="integer">561036</tournament-id>
    <updated-at type="datetime">2013-07-23T12:04:41-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv/>
    <scores-csv/>
  </match>
</matches>

2013-07-23 11:24:53|121eee39e8c2af65ab2b91418b309f34|1||m_tournament.start_tournament|create_challonge_tournament|2|tour_id:4,challonge_tour_id:561047,chalonge_name:test tournament #1,chalonge_url:fps_tour_4,full_challonge_url:http://challonge.com/fps_tour_4,live_image_url:http://images.challonge.com/fps_tour_4.png,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<tournament>
  <accept-attachments type="boolean">false</accept-attachments>
  <allow-participant-match-reporting type="boolean">true</allow-participant-match-reporting>
  <anonymous-voting type="boolean">false</anonymous-voting>
  <category nil="true"/>
  <check-in-at type="datetime" nil="true"/>
  <completed-at type="datetime" nil="true"/>
  <created-at type="datetime">2013-07-23T12:22:08-04:00</created-at>
  <created-by-api type="boolean">true</created-by-api>
  <credit-capped type="boolean">false</credit-capped>
  <description>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description>
  <enable-group-stage type="boolean">false</enable-group-stage>
  <game-id type="integer" nil="true"/>
  <hide-forum type="boolean">false</hide-forum>
  <hide-seeds type="boolean">false</hide-seeds>
  <hold-third-place-match type="boolean">false</hold-third-place-match>
  <id type="integer">561047</id>
  <max-predictions-per-user type="integer">1</max-predictions-per-user>
  <name>test tournament #1</name>
  <notify-users-when-matches-open type="boolean">true</notify-users-when-matches-open>
  <notify-users-when-the-tournament-ends type="boolean">true</notify-users-when-the-tournament-ends>
  <open-signup type="boolean">false</open-signup>
  <participant-count-to-advance-per-group type="integer" nil="true"/>
  <participants-count type="integer">0</participants-count>
  <prediction-method type="integer">0</prediction-method>
  <predictions-opened-at type="datetime" nil="true"/>
  <private type="boolean">false</private>
  <progress-meter type="integer">0</progress-meter>
  <pts-for-bye type="decimal">1.0</pts-for-bye>
  <pts-for-game-tie type="decimal">0.0</pts-for-game-tie>
  <pts-for-game-win type="decimal">0.0</pts-for-game-win>
  <pts-for-match-tie type="decimal">0.5</pts-for-match-tie>
  <pts-for-match-win type="decimal">1.0</pts-for-match-win>
  <ranked-by nil="true"/>
  <require-score-agreement type="boolean">false</require-score-agreement>
  <round-labels nil="true"/>
  <rr-pts-for-game-tie type="decimal">0.0</rr-pts-for-game-tie>
  <rr-pts-for-game-win type="decimal">0.0</rr-pts-for-game-win>
  <rr-pts-for-match-tie type="decimal">0.5</rr-pts-for-match-tie>
  <rr-pts-for-match-win type="decimal">1.0</rr-pts-for-match-win>
  <second-place-id type="integer" nil="true"/>
  <sequential-pairings type="boolean">false</sequential-pairings>
  <show-rounds type="boolean">false</show-rounds>
  <signup-cap type="integer" nil="true"/>
  <signup-requires-account type="boolean">false</signup-requires-account>
  <started-at type="datetime" nil="true"/>
  <state>pending</state>
  <swiss-rounds type="integer">0</swiss-rounds>
  <third-place-id type="integer" nil="true"/>
  <tournament-type>single elimination</tournament-type>
  <updated-at type="datetime">2013-07-23T12:22:08-04:00</updated-at>
  <url>fps_tour_4</url>
  <winner-id type="integer" nil="true"/>
  <description-source>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description-source>
  <subdomain nil="true"/>
  <full-challonge-url>http://challonge.com/fps_tour_4</full-challonge-url>
  <live-image-url>http://images.challonge.com/fps_tour_4.png</live-image-url>
  <sign-up-url nil="true"/>
  <review-before-finalizing type="boolean">true</review-before-finalizing>
</tournament>

2013-07-23 11:24:53|121eee39e8c2af65ab2b91418b309f34|1||m_tournament.start_tournament|update_tournament_table|2|tour_id:4,challonge_tour_id:561047,chalonge_url:fps_tour_4
2013-07-23 11:24:55|121eee39e8c2af65ab2b91418b309f34|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:4,challonge_tour_id:561047,team_id:5,participant_id:8772015,participant_name:James Sarawut Team,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-23T12:22:10-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8772015</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>James Sarawut Team</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">1</seed>
  <tournament-id type="integer">561047</tournament-id>
  <updated-at type="datetime">2013-07-23T12:22:10-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-23 11:25:02|121eee39e8c2af65ab2b91418b309f34|1||m_tournament.create_participants|create_challonge_participants|2|tour_id:4,challonge_tour_id:561047,team_id:6,participant_id:8772017,participant_name:OKC Team,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<participant>
  <active type="boolean">true</active>
  <checked-in type="boolean">false</checked-in>
  <created-at type="datetime">2013-07-23T12:22:13-04:00</created-at>
  <final-rank type="integer" nil="true"/>
  <group-id type="integer" nil="true"/>
  <icon nil="true"/>
  <id type="integer">8772017</id>
  <invitation-id type="integer" nil="true"/>
  <invite-email nil="true"/>
  <misc nil="true"/>
  <name>OKC Team</name>
  <on-waiting-list type="boolean">false</on-waiting-list>
  <seed type="integer">2</seed>
  <tournament-id type="integer">561047</tournament-id>
  <updated-at type="datetime">2013-07-23T12:22:13-04:00</updated-at>
  <challonge-username nil="true"/>
  <challonge-email-address-verified nil="true"/>
</participant>

2013-07-23 11:25:04|121eee39e8c2af65ab2b91418b309f34|1||m_tournament.start_challonge_tour|Start_Challonge_Tournament|2|tour_id:4,challonge_tour_id:561047,state:underway,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<tournament>
  <accept-attachments type="boolean">false</accept-attachments>
  <allow-participant-match-reporting type="boolean">true</allow-participant-match-reporting>
  <anonymous-voting type="boolean">false</anonymous-voting>
  <category nil="true"/>
  <check-in-at type="datetime" nil="true"/>
  <completed-at type="datetime" nil="true"/>
  <created-at type="datetime">2013-07-23T12:22:08-04:00</created-at>
  <created-by-api type="boolean">true</created-by-api>
  <credit-capped type="boolean">false</credit-capped>
  <description>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description>
  <enable-group-stage type="boolean">false</enable-group-stage>
  <game-id type="integer" nil="true"/>
  <hide-forum type="boolean">false</hide-forum>
  <hide-seeds type="boolean">false</hide-seeds>
  <hold-third-place-match type="boolean">false</hold-third-place-match>
  <id type="integer">561047</id>
  <max-predictions-per-user type="integer">1</max-predictions-per-user>
  <name>test tournament #1</name>
  <notify-users-when-matches-open type="boolean">true</notify-users-when-matches-open>
  <notify-users-when-the-tournament-ends type="boolean">true</notify-users-when-the-tournament-ends>
  <open-signup type="boolean">false</open-signup>
  <participant-count-to-advance-per-group type="integer" nil="true"/>
  <participants-count type="integer">2</participants-count>
  <prediction-method type="integer">0</prediction-method>
  <predictions-opened-at type="datetime" nil="true"/>
  <private type="boolean">false</private>
  <progress-meter type="integer">0</progress-meter>
  <pts-for-bye type="decimal">1.0</pts-for-bye>
  <pts-for-game-tie type="decimal">0.0</pts-for-game-tie>
  <pts-for-game-win type="decimal">0.0</pts-for-game-win>
  <pts-for-match-tie type="decimal">0.5</pts-for-match-tie>
  <pts-for-match-win type="decimal">1.0</pts-for-match-win>
  <ranked-by nil="true"/>
  <require-score-agreement type="boolean">false</require-score-agreement>
  <round-labels nil="true"/>
  <rr-pts-for-game-tie type="decimal">0.0</rr-pts-for-game-tie>
  <rr-pts-for-game-win type="decimal">0.0</rr-pts-for-game-win>
  <rr-pts-for-match-tie type="decimal">0.5</rr-pts-for-match-tie>
  <rr-pts-for-match-win type="decimal">1.0</rr-pts-for-match-win>
  <second-place-id type="integer" nil="true"/>
  <sequential-pairings type="boolean">false</sequential-pairings>
  <show-rounds type="boolean">false</show-rounds>
  <signup-cap type="integer" nil="true"/>
  <signup-requires-account type="boolean">false</signup-requires-account>
  <started-at type="datetime">2013-07-23T12:22:19-04:00</started-at>
  <state>underway</state>
  <swiss-rounds type="integer">0</swiss-rounds>
  <third-place-id type="integer" nil="true"/>
  <tournament-type>single elimination</tournament-type>
  <updated-at type="datetime">2013-07-23T12:22:19-04:00</updated-at>
  <url>fps_tour_4</url>
  <winner-id type="integer" nil="true"/>
  <description-source>SteelSeries ผู้ผลิตอุปกรณ์เกมมิ่งเกียร์สำหรับมืออาชีพ คุณภาพชั้นนำระดับโลก เพราะชัยชนะคือทุกสิ่ง! ตบเท้าเข้าร่วมระเบิดศึก PlayFPS Elites2013 by GIGABYTE ดวลสะท้าน 7 ประเทศ มันส์สะเทือน...ระดับโลก!! งานแข่ง</description-source>
  <subdomain nil="true"/>
  <full-challonge-url>http://challonge.com/fps_tour_4</full-challonge-url>
  <live-image-url>http://images.challonge.com/fps_tour_4.png</live-image-url>
  <sign-up-url nil="true"/>
  <review-before-finalizing type="boolean">true</review-before-finalizing>
</tournament>

2013-07-23 11:25:06|121eee39e8c2af65ab2b91418b309f34|1||m_tournament.get_challonge_matches|get_challonge_matches|2|tour_id:4,challonge_tour_id:561047,xml_res:<?xml version="1.0" encoding="UTF-8"?>
<matches type="array">
  <match>
    <created-at type="datetime">2013-07-23T12:22:19-04:00</created-at>
    <group-id type="integer" nil="true"/>
    <has-attachment type="boolean">false</has-attachment>
    <id type="integer">12708609</id>
    <identifier nil="true"/>
    <loser-id type="integer" nil="true"/>
    <player1-id type="integer">8772015</player1-id>
    <player1-is-prereq-match-loser type="boolean">false</player1-is-prereq-match-loser>
    <player1-prereq-match-id type="integer" nil="true"/>
    <player1-votes type="integer" nil="true"/>
    <player2-id type="integer">8772017</player2-id>
    <player2-is-prereq-match-loser type="boolean">false</player2-is-prereq-match-loser>
    <player2-prereq-match-id type="integer" nil="true"/>
    <player2-votes type="integer" nil="true"/>
    <round type="integer">1</round>
    <started-at type="datetime">2013-07-23T12:22:19-04:00</started-at>
    <state>open</state>
    <tournament-id type="integer">561047</tournament-id>
    <updated-at type="datetime">2013-07-23T12:22:19-04:00</updated-at>
    <winner-id type="integer" nil="true"/>
    <prerequisite-match-ids-csv/>
    <scores-csv/>
  </match>
</matches>

2013-07-23 11:25:06|121eee39e8c2af65ab2b91418b309f34|1||m_tournament.get_challonge_matches|create_challonge_matches|2|tour_id:4,challonge_tour_id:561047,challonge_match_id:3,detail:12708609561047487720158772017001open2013-07-23T12:22:19-04:002013-07-23 11:25:04
